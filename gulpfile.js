const gulp = require("gulp");
const { parallel, series } = require("gulp");
const package = require('./package.json');
const fs = require('fs');

require('dotenv').config({ path: './ftp.env.local' })

const imagemin = require("gulp-imagemin");
const htmlmin = require("gulp-htmlmin");
const uglify = require("gulp-uglify");
const sass = require("gulp-sass");
const sourcemaps = require('gulp-sourcemaps');
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create(); //https://browsersync.io/docs/gulp#page-top
const nunjucksRender = require("gulp-nunjucks-render");
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const inject = require('gulp-inject');
const es = require('event-stream');
const sSeries = require('stream-series');
const header = require('gulp-header');
const clean = require('gulp-clean');
const data = require('gulp-data');

const cheerio = require('gulp-cheerio');
const gitCommit = require('git-rev-sync');
const zip = require('gulp-zip');

var FtpDeploy = require("ftp-deploy");
var ftpDeploy = new FtpDeploy();
var creds = {
        remotePath: "/public_html/tlab2-test",
        host: process.env.FTP_SERVER,
        user: process.env.FTP_USER,
        pass: process.env.FTP_PASS
    };


/**
 * Paths to project folders
 */

var paths = {
    input: 'src/',
    output: 'dist/',
    scripts: {
        input: 'src/js/*',
        polyfills: '.polyfill.js',
        output: 'dist/js/'
    },
    styles: {
        vendors: "src/css/vendors/*.{css,scss,sass,map}",
        input: 'src/css/**/*.{scss,sass}',
        fonts: 'src/css/fonts/*.*',
        output: 'dist/css/'
    },
    svgs: {
        input: 'src/svg/*.svg',
        output: 'dist/svg/'
    },
    copy: {
        input: 'src/copy/**/*',
        output: 'dist/'
    },
    reload: './dist/'
};


/**
 * Template for banner to add to file headers
 */

var banner = {
    main:
        '/*!' +
        ' <%= package.name %> v<%= package.version %>' +
        ' | (c) ' + new Date().getFullYear() + ' <%= package.author.name %>' +
        ' | <%= package.license %> License' +
        ' | <%= package.repository.url %>' +
        ' | ' + gitCommit.branch() + '/' + gitCommit.short() +
        ' */\n',
    html:
        '<!--' +
        ' <%= package.name %> v<%= package.version %>' +
        ' | (c) ' + new Date().getFullYear() + ' <%= package.author.name %>' +
        ' | <%= package.license %> License' +
        ' | <%= package.repository.url %>' +
        ' | ' + gitCommit.branch() + '/' + gitCommit.short() +
        ' -->\n'
};

// /*
// TOP LEVEL FUNCTIONS
//     gulp.task = Define tasks
//     gulp.src = Point to files to use
//     gulp.dest = Points to the folder to output
//     gulp.watch = Watch files and folders for changes
// */

//clean build
function cleanDist(cb) {
    if (fs.existsSync('./dist')) {
        return gulp.src('dist', {read: false})
              .pipe(clean());
    } else {
        cb();
    }
}

// Optimise Images
function imageMin(cb) {
    return gulp.src("src/img/**/*")
        .pipe(imagemin())
        .pipe(gulp.dest("dist/img"));
    cb();
}

// Copy all HTML files to Dist
function copyHTML(cb) {
    gulp.src("src/*.html").pipe(gulp.dest("dist"));
    cb();
}

// Copy all HTML files to Dist
function copyFonts(cb) {
    return gulp.src(paths.styles.fonts).pipe(gulp.dest(paths.styles.output + 'fonts/'));
}

// Minify HTML
function minifyHTML(cb) {
    return gulp.src("src/*.html")
        .pipe(gulp.dest("dist"))
        .pipe(
            htmlmin({
                collapseWhitespace: true
            })
        )
        .pipe(gulp.dest("dist"));
    cb();
}

// Scripts
function js(cb) {
    return gulp.src("src/js/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat("main.js"))
        .pipe(uglify())
        .pipe(header(banner.main, {package: package}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("dist/js"));
    
    // return cb();
}

function vendorsJS(cb) {
    gulp.src(["src/js/vendors/*.js"])
        .pipe(concat("vendors.bundle.js"))
        .pipe(gulp.dest("dist/js/vendors"));

    gulp.src("src/js/vendors/*.map")
        .pipe(gulp.dest("dist/js/vendors"));
    
    return cb();
}

// Compile Sass
function css(cb) {
    return gulp.src(paths.styles.input, !paths.styles.vendors)
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(autoprefixer({
            browserlist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(header(banner.main, {package: package}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.styles.output))
        // Stream changes to all browsers
        .pipe(browserSync.stream());
    cb();
}

function vendorsCSS(cb) {
    return gulp.src(paths.styles.vendors)
        .pipe(gulp.dest(paths.styles.output + 'vendors/'))
        // Stream changes to all browsers
        .pipe(browserSync.stream());
    cb();
}

function injectAssets(cb) {
    const vendorStream = gulp.src(['./dist/js/vendors/*.js', './dist/css/vendors/*.css'], {read: false});
    const appStream = gulp.src(['dist/**/*.js', 'dist/**/*.css','!dist/**/vendors/*.*'], {read: false});
    return gulp.src("src/templates/**/*.html")
        .pipe(
            inject(sSeries(vendorStream, appStream), {ignorePath: "/dist", addRootSlash: false})
        )
        .pipe(gulp.dest(
            function (file) {
                return file.base;
            }
        ))
        .pipe(browserSync.stream());
    cb();
}


function getDataForFile(file) {
    
  return {
    file: file.relative.split('.')[0],
    lang: 'default',
    tlabInfo: `${package.name} ${package.version}`
  };
}

// Process Nunjucks
function nunjucks(cb) {
    return gulp.src("src/pages/*.html")
        .pipe(data(getDataForFile))
        .pipe(
            nunjucksRender({
                path: ["src/templates/"] // String or Array
            })
        )
        .pipe(header(banner.html, {package: package}))
        .pipe(gulp.dest("dist"));
    cb();
}

function nunjucksMinify(cb) {
    gulp.src("src/pages/*.html")
        .pipe(
            nunjucksRender({
                path: ["src/templates/"] // String or Array
            })
        )
        .pipe(
            htmlmin({
                collapseWhitespace: true
            })
        )
        .pipe(gulp.dest("dist"));
    cb();
}

function getTranslationStrings(cb) {
    var trans = {};
    return gulp.src("dist/*.html")
        .pipe(
            cheerio({run: function($, file) {
                // Each file will be run through cheerio and each corresponding `$` will be passed here.
                // `file` is the gulp file object
                // Make all h1 tags uppercase
                $('[data-lang]').each(function() {
                    var el = $(this),
                        key = el.data('lang'),
                        txt = el.html().replace(/<!--[\s\S]*?-->/g, '').trim();

                        if (key == "") {
                            trans[key]  = txt;
                        }
                        else {
                            trans[txt]  = txt;
                        }
                    
                    //el.attr('data-lang', txtKey);
                    console.log("added key: " + key );
                    // console.log("proposal: " + txtKey );
                    
                    //console.log(el.data('lang'), el.text());
                });
            }, parserOptions: {decodeEntities: false}})
        )
        .on("data", function(file) { 
            // console.log(trans);
            fs.writeFileSync('./translations.json', JSON.stringify(trans, null, 4) );
        })
        .pipe(gulp.dest("dist"));
    
    cb();
}

function genTranslationStrings(cb) {
    var trans = {};
    return gulp.src("src/**/*.html")
        .pipe(
            cheerio({run: function($, file) {
                // Each file will be run through cheerio and each corresponding `$` will be passed here.
                // `file` is the gulp file object
                // Make all h1 tags uppercase
                $('[data-lang]').each(function() {
                    var el = $(this),
                        key = el.data('lang'),
                        txtKey;

                        if (key == "") {
                            //generate strings
                            txtKey = el.text();
                            txtKey = txtKey.replace(/(\t|\n|\r)+(\s{2,})+/g, '');
                            txtKey = txtKey.replace(/\s/g, '_');
                            txtKey = txtKey.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                            txtKey = txtKey.replace(/\W/g, '').toLowerCase();
                            
                            el.attr('data-lang', txtKey);
                            console.log("added key to attribute: " + txtKey );
                        }                    
                });
            }, parserOptions: {decodeEntities: false}})
        )
        .pipe(gulp.dest(function (file) {
                return file.base;
            }));
    
    cb();
}

function zipDist(cb) {
    
    return gulp.src('dist/**/*')
        .pipe(zip(`t-lab@${package.version}.zip`))
        .pipe(gulp.dest('distzips/'))
    
}

function ftpDeployDist(cb) {

    var config = {
        user: creds.user,
        // Password optional, prompted if none given
        password: creds.pass,
        host: creds.host,
        port: 21,
        localRoot: __dirname + "/dist",
        remoteRoot: "/public_html/tlab2-test",
        // include: ["*", "**/*"],      // this would upload everything except dot files
        include: ["*", "**/*"],
        // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
        // exclude: ["dist/**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**"],
        // delete ALL existing files at destination before uploading, if true
        deleteRemote: false,
        // Passive mode is forced (EPSV command is not sent)
        forcePasv: true,
        // use sftp or ftp
        sftp: false
    };

    const progress = require('./docs/progress.js');

    ftpDeploy.on("uploading", progress.uploading)
    ftpDeploy.on("uploaded", progress.uploaded);

    ftpDeploy
        .deploy(config)
        .then(res => {
            let arrItemsLength = res.map(w => w.length);
            let nFiles = arrItemsLength.reduce((a, b) => a + b, 0);
            console.log(
              `\nUpload Finished: ${nFiles} files uploaded to ${arrItemsLength.length} directories.`
            );
            return cb();
        })
        .catch(err => {
            console.log(err);
            return cb();            
        });



}

function ftpDeployZips(cb) {

    // changelogjson will be created or overwritten by default.
    fs.copyFile('./docs/changelog.json', './distzips/changelog.json', (err) => {
      if (err) throw err;
      console.log('copied changelog.json');
    });

    var config = {
        user: creds.user,
        // Password optional, prompted if none given
        password: creds.pass,
        host: creds.host,
        port: 21,
        localRoot: __dirname + "/distzips",
        remoteRoot: "/public_html/tlab2-test/distzips",
        include: ["*"],
        deleteRemote: false,
        // Passive mode is forced (EPSV command is not sent)
        forcePasv: true,
        // use sftp or ftp
        sftp: false
    };

    const progress = require('./docs/progress.js');
    ftpDeploy.on("uploading", progress.uploading)
    ftpDeploy.on("uploaded", progress.uploaded);

    ftpDeploy
        .deploy(config)
        .then(res => {
            let arrItemsLength = res.map(w => w.length);
            let nFiles = arrItemsLength.reduce((a, b) => a + b, 0);
            console.log(
              `\nUpload Finished: ${nFiles} files uploaded to ${arrItemsLength.length} directories.`
            );
            return cb();
        })
        .catch(err => {
            console.log(err);
            return cb();
        });

}

function isClean(cb) {
    if ( gitCommit.isDirty() ) {
        // console.log(gitCommit.isTagDirty());
        cb('[ERR] You must have a clean commit...');
    } else {
        const changelog = require('./docs/changelog_gen.js');
        changelog.generateCL('./docs');
        cb();
    }
}

// browsersync reload to be used in series with js as it was firing the change event too soon
function reloadBS(cb) {
    browserSync.reload();
    cb();
}


// Watch Files
function watch_files() {
    browserSync.init({
        server: {
            baseDir: "dist/"
        }
    });
    gulp.watch("src/css/vendors/*.*", series(vendorsCSS, injectAssets) );
    gulp.watch(["src/css/**/*.{scss,css}", "!src/css/vendors"], series(css) );
    gulp.watch("src/img/**/*", series(imageMin) );
    gulp.watch("src/css/fonts/**/*", series(copyFonts) );
    gulp.watch("src/js/*.js", series(js, reloadBS) );
    gulp.watch("src/js/vendors/*.js", series(vendorsJS, js, injectAssets) ).on("change", browserSync.reload);
    gulp.watch("src/pages/*.html", nunjucks).on("change", browserSync.reload);
    gulp.watch("src/templates/**/*.html", nunjucks).on(
        "change",
        browserSync.reload
    );
}

// Default 'gulp' command with start local server and watch files for changes.
exports.default = series(parallel(vendorsCSS, css, vendorsJS, js), injectAssets, nunjucks, imageMin, copyFonts, watch_files);

// 'gulp build' will build all assets but not run on a local server.
// exports.build = parallel(nunjucksMinify, css, js, imageMin);
exports.build = series(cleanDist, parallel(vendorsCSS, css, vendorsJS, js), injectAssets, nunjucks, imageMin, copyFonts);
exports.translate = series(exports.build, getTranslationStrings);
// exports.translate_gen = series(genTranslationStrings);
exports.zip = series(zipDist);
exports.ftp = series(isClean, exports.build, exports.zip, ftpDeployDist, ftpDeployZips);
