# README #

HTML files for T-LAB 2.0

## New URL Structure

generic structure: https://domain.tld/content/country-code

e.g.:
https://t-lab.app/content/es-MX/ - index 
https://t-lab.app/content/es-MX/diagnostic - diagnostic
https://t-lab.app/content/es-MX/dosage - dosage
https://t-lab.app/content/es-MX/prescription - prescription
https://t-lab.app/content/es-MX/pack-calculator - pack-calculator


## What is this repository for?

This repository is intended to provide the simple html to be integrated in the app/cms.


## How do I get set up?

Install node and gulp globally.


### Install node modules

first install all the dependencies:
`npm install`

run the following commands:

* `gulp` - dev with live reloading
* `gulp build` - exports dist directory
* `gulp translate` - creates a translations.json file in the root with all the keys and corresponding texts that need translation - NEEDS TESTING
* ~~`gulp translate_gen` - generates keys missing in elements with the `data-lang` attribute~~ DEPRECATED


### Contribution guidelines

* Writing tests
* Code review
* Other guidelines


### On Push Changes

To bump the version of the package run the followng commands

`npm version patch -m "patch version message"`

`npm version minor -m "minor version message"`

`npm version major -m "major version message"`

## Project Structure

With nunjucks it is possible to have HTML partials and templates. 

There are two folders, one with templates and partials in `src/templates` and actual pages in `src/pages`.

Each page extends from a subfolder in the templates folder and all files extend from the `src/templates/base.html` 

### Tree

* src/templates/base.html
	* auth/
		* base.html
			* foooter.html
			* header.html
				* index.html/login.html/password_recovery.html/registo.html

	* menu/
		* base.html
			* foooter.html
			* ../header.html (root)
				* 01_main-menu.html

	* diagnostic/
		* base.html
			* ../foooter.html (root)
			* ../header.html (root)
				* 02_diagnostic.html

### References modal macro

To add the references modal to a page and automatically adding it to the "i" menu, set the variable `refs` like in the example bellow:

```html
{% set refs %}
	<!-- you can use any HTML, this is just the standard references list -->
	<ol class="refs" id="populate-refs">
		<li>Reference #1</li>
		<li>Reference #2</li>
	</ol>
{% endset %}
```

This variable when defined, triggers the macro defined in the `templates/refs-macro.html` on the `templates/base.html` file.


## Guide to create a new language for Prescription and Pack Calculator

Unfortunately is a bit of a fragmented process. This might change in the future.


### Interface Translation / Localized Link replacement

On the `transInterface.js` create a new key for the `appTranslations` and `refsTranslation` variables with the code for the new country in ISO format (e.g. pt-BR).

The `appTranslations` object should take a key with the original string to be translated (which in this case is in english) and the value should be te corresponding translation. All this is case sensitive, the Translator class ignores html comments and whitespace when substituting the strings.

The `refsTranslation` var should take first a key of the context and then an array containing text values with each reference to be shown. Example bellow.

```js
	"prescription": [
		"Reference #1",
		"Reference #2"
	]
```

**Both these vars must have the country key or the script will fail!!!**

Three functions are invoked on the prescription page using as the country parameter the value passed as as the URL parameter country. If this value doesn't exist in the transInterface object, the value will reset to "default".


* `translation.populateTemplateFromString( appTranslations[country] )` - translates the interface.
* `translation.updateLocalizedLinks( country )` - changes the href of the links that depend on the URL Param to present the correct language.
* `translation.populateLocalizedRefs( refsTranslation[country].prescription )` - Populates the references modal.


### Prescription & Pack Calculator Localization

After creating the key in the transInterface.js variables, a key with the same country **MUST** exist on the following places:


#### presCalc.js file
* `presCalc.country_code()` - responsible for the calculations of the final dosage.


#### packCalc.js file
* `packCalc.country_code()` - sets the information about the packages for each coutry. Info about each var o the top of the file.


#### prescription.js file
These **have a check to be optional**, so create them only if necessary.

* `presCalc.addLocalizedFields.country_code()` - Adds extra fields to the standard form 
* `presCalc.hideLocalizedConditionalFields.country_code()` - Adds conditions for the display of these extra fields.

## FTP deploy to php enabled server
create a `.ftp.env.local` file with the credentials of the server as in the example bellow

```sh
FTP_SERVER=domain_or_ip
FTP_USER=username
FTP_PASS=password
```

~~On the `docs/changelog.json`, create a key with the version you are about to deploy. The key should follow the same structure as the previous versions declared on the file, with a date and a changelog.~~ 

Changelog is created automatically from commit history.

The current version is present in the `package.json` file and it is tagged automatically in git with [npm version](#markdown-header-on-push-changes) command.

*NOTE* - If you run this command without bumping the version the latest commit will overwrite the previously deployed version.

Run `gulp ftp` to deploy, this should take a couple of minutes.


## Who do I talk to? ##

* Miguel Santos (jose.miguel@float.pt)
* Pedro Gabriel ()