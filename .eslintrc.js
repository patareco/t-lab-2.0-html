module.exports = {
	root: true,
	env: {
		node: true
	},
	extends: ["standard"],
	parserOptions: {
		parser: "babel-eslint"
	},
	rules: {
		"no-multi-spaces": ["warn", {
			exceptions: { VariableDeclarator: true }
		}],
		// "key-spacing": ["warn", { align: "value" }],
		quotes: ["off", "double"],
		semi: "off",
		indent: ["warn", "tab", { SwitchCase: 1 }],
		// because of form comparinson
		eqeqeq: "off",
		"no-unused-vars": "warn",
		"space-in-parens": ["warn", "always"],
		"no-undef": "off",
		"no-tabs": "off",
		"no-multiple-empty-lines": ["warn", { max: 2 }],
		"no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off"
	}
};
