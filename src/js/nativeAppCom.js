/* eslint-disable no-unused-vars */
/**
* Functions for communicating with the native app
* or google analytics444
*
*/

/**
* @function openProfile
*
* Open native dialog for editing profile
* @returns null
*/
function openProfile () {
	try {
		messageHandler.postMessage( 'EditProfile' );
		console.log( 'Profile opened' );
	} catch {
		console.log( 'not in native app' );
	}
}

/**
* @function logAnalyticsPage
*
* Sends info to the native app about the page being viewed
* also hides unnecessary items
* @returns null
*/

( function logAnalyticsPage () {
	const path = window.location.pathname;
	const sanitizedPath = path.replace( /\/([\w|_|-]+)(\.\w+)/, ( val, name ) => name );
	// console.log(sanitizedPath);

	try {
		messageHandler.postMessage( 'logAnalytics', sanitizedPath );
		console.log( 'Analytics Sent' );
	} catch {
		console.log( "not in native app" );
	}
} )();

/**
* @function isNativeApp
*
* Hides elements not needed in native app
*
* @returns null
*/

function isNativeApp () {
	if ( navigator.userAgent.indexOf( "TLAB_APP" ) == -1 ) {
		return true;
	} else {
		return false;
	}
};

/**
* @function adaptNativeApp
*
* (IEFE) Hides elements not needed in native app
*
* @returns null
*/

( function adaptNativeApp () {
	if ( navigator.userAgent.indexOf( "TLAB_APP" ) == -1 ) {
		console.log( "not in native app" );
		// hide unnecessary items
		if ( document.getElementById( 'open-profile' ) ) {
			document.getElementById( 'open-profile' ).style.display = 'none';
		}
	} else {
		// native app modifications
		const bottomMenu = document.getElementById( 'menu-bottom' );
		// const homeButton = document.querySelector( '.start-button .btn-icn' );
		// const headerLogo = document.querySelector( '.brand div:nth-child(3)' );
		const headerBar = document.querySelector( 'header' );
		const headerRefs = document.querySelector( '.brand div:nth-child(2)' );
		const mainTitle = document.querySelector( '.main-section-title' );

		const isDiagnosticSubpage = document.querySelector( '.diagnostic main:has(.swiper-container)' );

		console.log( isDiagnosticSubpage );
		if ( bottomMenu ) {
			bottomMenu.style.display = 'none';
		}
		if ( headerBar ) {
			headerBar.style.display = 'none';
			// headerBar.style.visibility = 'hidden';
			// headerBar.style.height = '0';
		}
		if ( mainTitle && headerRefs ) {
			headerRefs.style.float = 'right';
			headerRefs.classList.remove( 'pr-3' );
			mainTitle.appendChild( headerRefs );
		}

		if ( isDiagnosticSubpage ) {
			const backEl = document.createElement( 'h2' );
			backEl.classList.add( 'main-section-title' );
			backEl.innerHTML = `
				<a href="#" onClick="window.history.back()" class="animsition-trigger">
					<small>←</small>
				</a>
			`;
			document.querySelector( 'main' ).prepend( backEl );
			console.log( appTranslations );
		}

		// give padding to bottom of modal for native menu to not overlay
		const modals = document.querySelectorAll( '.modal' );
		modals.forEach( modal => modal.style.height = "calc(100% - 90px)");

	}
} )();

/**
* @function logAnalyticsPresCalc
*
* Sends info to the native app about user made calculations in
* prescription calc
* @returns null
*/

function logAnalyticsPresCalc ( presCalc ) {
	const eventName = "prescriptionCalc";
	const params = ["initialDose", "maintenanceDose", "tshDose", "followupDose"];

	params.forEach( param => {
		try {
			if ( presCalc[param] !== null && presCalc[param] !== " - " ) {
				messageHandler.postMessage( `${eventName}|${param}|${presCalc[param]}` );
				console.log( `Analytics Sent: ${param}: ${presCalc[param]}` );
			}
		} catch {
			console.log( "not in native app, prescriptionCalc analytics not sent" );
		}
	} );
}

/**
* @function logAnalyticsPackCalc
*
* Sends info to the native app about user made calculations in
* Pack calc or Google Analytics in case of iFrame
* @returns null
*/

function logAnalyticsPackCalc ( dosage, days, packsJson, closest ) {
	const eventName = "packCalc";
	packsJson = JSON.stringify( packsJson );
	try {
		messageHandler.postMessage( `${eventName}|dosage|${dosage}` );
		messageHandler.postMessage( `${eventName}|days|${days}` );
		messageHandler.postMessage( `${eventName}|packsJson|${packsJson}` );
		messageHandler.postMessage( `${eventName}|closest|${closest}` );
		console.log( `Analytics Sent: ${dosage}: ${days} | ${closest}: ${packsJson}` );
	} catch {
		console.log( "not in native app, packCalc analytics not sent" );
	}
	// if is iframe and has GA
	// ga('send', {
	// 	hitType: 'event',
	// 	eventCategory: 'Videos',
	// 	eventAction: 'play',
	// 	eventLabel: 'Fall Campaign',
	// 	value: 60
	// });
}

/**
* @function openPDF
*
* this is in the main menu page to be processed by the bundle generator :(
* Opens PDF in native app
* todo: open tab with pdf when not in native app
* @returns null
*/
/* function openPDF () {
	try {
		messageHandler.postMessage( 'openPDF' );
		console.log( 'openPDF opened' );
	} catch {
		console.log( 'not in native app. Opening PDF in browser' );
		console.log( './img/dosage/TMDI00000010' );
		window.open( './img/dosage/TMDI00000010' );
	}
} */
