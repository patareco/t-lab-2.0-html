/**
 * JS Utilities for the prescription form
 */

const presc = {

	isAppleDevice () {
		return [
			'iPad Simulator',
			'iPhone Simulator',
			'iPod Simulator',
			'iPad',
			'iPhone',
			'iPod'
		].includes( navigator.platform ) ||
		// iPad on iOS 13 detection
		( navigator.appVersion.includes( "Mac" ) && "ontouchend" in document )
	},

	// add class to non selected elements in a radio group (per slide)
	optionClasses () {
		const els = document.querySelectorAll( 'input[type="radio"]' );

		// add class to non selected elements (per slide)
		els.forEach( el => {
			el.addEventListener( 'change', function ( e ) {
				// console.log('I changed but you don\'t');
				document.querySelectorAll( '[name=' + e.target.name + ']' ).forEach( el => {
					if ( el.checked === true ) { el.parentElement.classList.remove( 'inactive' ); } else { el.parentElement.classList.add( 'inactive' ); }
				} );
			} );
		} );
		// console.log(els);
	},

	// Initialize KioskBoard (default/all options)
	initializeKioskboard () {
		KioskBoard.Init( {
			keysArrayOfObjects: [
				{ 0: "6", 1: "8", 2: "9" },
				{ 0: "4", 1: "5", 2: "6" },
				{ 0: "1", 1: "2", 2: "3" },
				{ 0: "0", 1: "0", 2: "." }
			],
			// specialCharactersObject: {'0': ','},
			keysAllowSpacebar: false,
			capsLockActive: false,
			keysFontFamily: 'Verdana',
			autoScroll: true,
			cssAnimations: true,
			cssAnimationsStyle: 'slide',
			cssAnimationsDuration: 350
		} );
		console.log( this.isAppleDevice() );
		if ( !this.isAppleDevice() ) {
			KioskBoard.Run( '.js-kioskboard-input' );
		}
	},

	// height unit
	// replaces the cms inputbox by ft + in inputboxes
	replaceHeightInputs () {
		const heightUnitEl = document.getElementById( 'unit-heigth' );

		heightUnitEl.addEventListener( 'change', () => {
			const heightUnit = heightUnitEl.value;

			document.querySelectorAll( '[class*=i-height-]' ).forEach( el => ( el.style.display = 'none' ) );
			document.querySelectorAll( '.i-height-' + heightUnit ).forEach( el => ( el.style.display = 'block' ) );
		} );

		heightUnitEl.dispatchEvent( new Event( "change" ) );
	},

	// prevSlide btn in slidesPerView
	nextSlideButtons () {
		document.querySelectorAll( '.prev-question' ).forEach( el => {
			el.addEventListener( 'click', ( ev ) => {
				swiper.slidePrev()
			} );
		} );
	},

	// nextSLide btn in slidesPerView
	prevSlideButtons () {
		document.querySelectorAll( '.next-question' ).forEach( el => {
			el.addEventListener( 'click', ( ev ) => {
				swiper.slideNext()
			} );
		} );
	},

	// auto advance to next slide when an option is selected by user
	autoAdvance () {
		let enabledAutoAdvance = true;
		document.querySelectorAll( 'input[type=radio]' ).forEach( el => {
			el.addEventListener( 'click', ( ev ) => {
				const actualSlideIndex = swiper.activeIndex;
				// add 'done' class to slides already answered
				const listOfSlides = Array.prototype.slice.call( document.querySelectorAll( '.swiper-slide' ) );
				const slide = listOfSlides[actualSlideIndex];
				slide.classList.add( 'done' );

				// swiper.params.allowSlideNext = true;
				// swiper.update();
				if ( enabledAutoAdvance ) {
					setTimeout( () => { swiper.slideNext(); enabledAutoAdvance = true; }, 300 );
				}

				// prevent double click double advance
				enabledAutoAdvance = false;
			} );
		} );
	},

	// Prevent pagination nav to undone tabs
	preventNavToUndoneTabs () {
		document.querySelectorAll( '.swiper-pagination-bullet' ).forEach( ( el, index ) => {
			el.index = index;
			el.addEventListener( 'click', ( ev ) => {
				ev.preventDefault();
				// console.log( el.index )

				const indexBullet = el.index;
				const actualSlideIndex = swiper.activeIndex;

				const listOfSlides = Array.prototype.slice.call( document.querySelectorAll( '.swiper-slide' ) );
				const actualSlide = listOfSlides[actualSlideIndex];
				const lastDoneSlide = ( ( indexBullet - 1 ) >= 0 ) ? listOfSlides[indexBullet - 1] : listOfSlides[0];

				// class 'done' inserted in slide when an option is selected
				const navNext = actualSlide.classList.contains( 'done' );
				const navTo = lastDoneSlide.classList.contains( 'done' );

				// always nav back. If actual slide is 'done' nav one step forward
				if ( ( indexBullet - actualSlideIndex ) < 0 ) {
					swiper.slideTo( indexBullet );
				} else if ( ( indexBullet - actualSlideIndex ) === 1 && navNext ) {
					swiper.slideTo( indexBullet );
				} else if ( ( indexBullet - actualSlideIndex ) > 1 && navTo ) {
					swiper.slideTo( indexBullet );
				}
			} );
		} );
	},

	/**
	* @function presc.resetSlidesNav()
	*
	* remove 'done' class from all slides except made to slide IDs in 'exceptionId' array
	* this is a function so it can be reused for multiple countries
	*
	* @param exceptionId - an array with slide IDs to ignore
	*
	*/
	resetSlidesNav ( exceptionId = [] ) {
		const arrException = ['slide-start', ...exceptionId];
		const allSlides = Array.prototype.slice.call( document.querySelectorAll( '.swiper-slide' ) );
		const listOfSlides = allSlides.filter( slide => !arrException.includes( slide.id ) );
		listOfSlides.forEach( slide => slide.classList.remove( 'done' ) );
	},

	// range slider
	// deals with the interactivity of the age slider element
	// applies to all inputs of type range
	rangeSliderInteractions () {
		const rangeLabels = document.querySelectorAll( '[data-range-id]' );

		rangeLabels.forEach( ( el ) => {
			el.addEventListener( 'click', e => {
				document.getElementById( el.dataset.rangeId ).value = e.target.dataset.rangeVal;
				document.getElementById( el.dataset.rangeId ).dispatchEvent( new CustomEvent( 'input', { detail: { byClick: true } } ) );
			} )
		} );

		const rangeEls = document.querySelectorAll( '[type="range"]' );

		rangeEls.forEach( ( el ) => {
			el.oninput = e => {
				document.querySelector( 'label[for=' + el.id + ']' ).innerHTML = document.querySelector( '[data-range-id="' + el.id + '"][ data-range-val="' + e.target.value + '"]' ).innerHTML;
				document.querySelectorAll( '[data-range-id="' + el.id + '"]' ).forEach( el => el.classList.remove( 'active' ) );
				document.querySelector( '[data-range-id="' + el.id + '"][ data-range-val="' + e.target.value + '"]' ).classList.add( 'active' );
			}
			// trigger an event to put the initial value in the label
			el.dispatchEvent( new CustomEvent( 'input', { detail: { byClick: false } } ) );
		} );
	},

	// Only allows to advance if all inputboxes are filled correctly
	validateTextInputs ( ev ) {
		const inputs = ev.target.closest( '.swiper-slide' ).querySelectorAll( 'input, select:not([name^=unit])' );
		let allowNext = true;

		inputs.forEach( el => {
			// TSH is not required when calculationg by patient weight
			// if input is required, check value and validity
			if ( el.required ) {
				// if ( el.value == '' && el.parentElement.style.display !== 'none' ) {
				if ( el.value == '' && el.offsetParent !== null ) {
					console.log( `please set a positive value: ${el.value}` );
					allowNext = false;
					el.parentElement.classList.add( 'was-validated' );
					return;
				}
				if ( el.value && el.pattern ) {
					console.log( 'not empty pattern' );
					// const re = new RegExp( el.pattern );
					if ( !el.checkValidity() ) {
						console.log( 'pattern' );
						allowNext = false;
						el.parentElement.classList.add( 'was-validated' );
					}
				}
			}
		} )

		if ( allowNext ) {
			ev.target.closest( '.swiper-slide' ).classList.add( 'done' );
			swiper.slideNext();
		}
		//	console.log(inputs);
	},

	// Populate the list items for choices review
	// @param form  - precessed form data
	// @param el - the wrapper ul element id to populate
	populateReviewList ( form, el ) {
		const list = document.getElementById( el );
		list.innerHTML = "";

		for ( const selection in form.labels ) {
			const newLi = document.createElement( 'li' );
			newLi.innerHTML = form.labels[selection];

			list.appendChild( newLi );
		}
	},

	// Function to process all of the form data
	// outputs an object with two subobjects, data for calculations and labels for presentation
	getFormData () {
		const els = document.querySelectorAll( 'input, select:not([name^=unit])' );
		console.log( els );
		const formData = {};
		let formLabels = {};
		els.forEach( el => {
			if ( el.checked ) {
				formData[el.name] = el.value;
				formLabels[el.name] = el.parentElement.querySelector( 'label p.option-main' ).innerHTML;
				if ( el.parentElement.querySelector( 'label p.option-desc' ) ) { formLabels[el.name] += ' ' + el.parentElement.querySelector( 'label p.option-desc' ).innerHTML; }
			}
			// console.log(el);

			// country indicator
			if ( el.name === "country" ) {
				formData[el.name] = el.value;
				// formLabels[el.name] = `Country: ${el.value}`;
			}

			if ( el.name === "age" ) {
				formData[el.name] = el.value;
				formLabels[el.name] = document.querySelector( `[data-range-id="${el.id}"][data-range-val="${el.value}"]` ).innerHTML;
			}

			if ( el.name === "tsh" && el.value != '' ) {
				formData[el.name] = el.value;
				formLabels[el.name] = `TSH: ${el.value} mIU/L`;
			}

			// For now, the T4 & anticorpo inputs are required only for Brazil(pt_BR) calculator
			// If they are hidden, they'll not be displayed
			if ( el.name === "t4" && el.value != '' && el.offsetParent !== null ) {
				formData[el.name] = el.value;
				formLabels[el.name] = `T4: ${el.value} ng/dL`;
			}
			if ( el.name === "anticorpo" && el.value != '' && el.offsetParent !== null ) {
				formData[el.name] = el.value;
				formLabels[el.name] = `Anticorpo: ${el.value}`;
			}
		} );

		const unitEls = document.querySelectorAll( 'select' );

		unitEls.forEach( el => {
			// console.log(el.value);

			if ( el.value === 'kg' ) {
				formData.weightUnit = 'kg';
				const kgs = el.parentElement.parentElement.querySelector( 'input' ).value;
				// console.log('kgms:', kgs);
				if ( kgs ) {
					formData.kg = kgs;
					formLabels.weight = `${kgs}kg`;
				}
			}
			if ( el.value === 'lb' ) {
				formData.weightUnit = 'lb';
				const lbs = el.parentElement.parentElement.querySelector( 'input' ).value;
				const kgs = ( lbs * 0.45359237 ).toFixed( 2 );
				// console.log('lbs2kg:', kgs, lbs   );
				if ( kgs ) {
					formData.lb = lbs;
					formData.kg = kgs;
					formLabels.weight = `${kgs}kg (${lbs}lbs)`;
				}
			}

			if ( el.value === 'cm' ) {
				formData.heightUnit = 'cm';
				const cms = el.parentElement.parentElement.querySelector( 'input' ).value;
				// console.log('cms:', cms);
				if ( cms ) {
					formData.cm = cms;
					formLabels.height = `${cms}cm`;
				}
			}

			if ( el.value === 'ft' ) {
				formData.heightUnit = 'ft';
				const ftinEls = el.parentElement.parentElement.querySelectorAll( 'input' );
				let ft, inc;
				ftinEls.forEach( el => {
					if ( el.name === 'height-ft' ) {
						ft = el.value;
					}
					if ( el.name === 'height-in' ) {
						inc = el.value;
					}
				} )
				const cms = ft * 30.48 + inc * 2.54
				// console.log('fts2cms', cms+'cms', ft+'\''+inc+'"');
				if ( cms ) {
					formData.cm = cms;
					formData.ft = `${ft}'${inc}"`;
					formLabels.height = `${cms}cm (${formData.ft})`;
				}
			}

			// special TSH/Weight choice for selected countries (ref#calc-type)
			if ( el.name === "calcType" ) {
				const name = el.name;
				const chosen = el.querySelector( `option[value="${el.value}"]` ).innerHTML;
				// we use the spread operator to insert values at the top of the object
				formData[el.name] = el.value;
				formLabels = { [name]: `${el.dataset.label}: ${chosen}`, ...formLabels };
			}
		} );

		console.log( formData, formLabels );
		const form = {
			data: formData,
			labels: formLabels
		}
		return form;
	},

	/**
	* @function presc.addCalcTypeComboBox()
	*
	* create a CalcType combo box in first slide
	* this is a function so it can be reused for multiple countries
	* this has to be dealt in a special way by the get form data (search "ref#calc-type")
	*
	* @param label - the label for the field
	* @param dataLabel - the label for the review choices screen
	* @param oprtions - object containing the options for combo box (the key will be the html value and hb)
	*
	*/
	addCalcTypeComboBox ( label, dataLabel, options ) {
		// append dropdown weight/tsh to slide (1)
		const ddEl = document.createElement( 'div' );
		let htmlToAppend = "";
		ddEl.classList.add( 'form-row', 'align-items-center' );

		htmlToAppend += `
			<div class="form-group col-12 col-md-8 offset-md-2 mt-2">
				<label for="calcType">${label}</label>
				<select class="form-control" name="calcType" id="calcType" data-label="${dataLabel}" required>`;

		for ( const value in options ) {
			htmlToAppend += `<option value="${value}">${options[value]}</option>`
		}

		htmlToAppend += `
				</select>
			</div>
		`;
		ddEl.innerHTML = htmlToAppend;
		const targetEL = document.querySelector( '#slide-start .form-group' );

		targetEL.insertAdjacentElement( "beforebegin", ddEl );

		const calcTypeSelect = document.getElementById( 'calcType' );
		calcTypeSelect.onchange = ( e ) => {
			presc.resetSlidesNav();
		}
	},
	/**
	* @function presc.addHtmlElm()
	*
	* create a html element
	* this is a function so it can be reused for multiple countries
	* this function is flexible. is possible to add any type of html element
	*
	* @param target - the target or parent element selector string or node witch will contain the new element
	* @param position - use insertAdjacentElement options
	* @param tag - the type of element to be added
	* @param attributes - an object with the element attributes
	* @param innerHtml - the inner html to be appended
	*
	*/
	addHtmlElm ( target, position, tag, attributes = {}, innerHtml = null ) {
		const htmlEl = document.createElement( tag );

		if ( typeof attributes === 'object' ) {
			for ( const attribute in attributes ) {
				if ( attribute === 'src' ) {
					// src attribute full url change to match the country
					attributes[attribute] = `./img/i18n/${country}/added/${attributes[attribute]}`;
				}

				htmlEl.setAttribute( attribute, attributes[attribute] );
			}
		}
		// if exists, add the html into the element
		if ( innerHtml ) {
			htmlEl.innerHTML = innerHtml;
		}

		const targetEL = typeof target === 'string' ? document.querySelector( target ) : target;

		targetEL.insertAdjacentElement( position, htmlEl );
	},
	/**
	* @function presc.setFormElm()
	*
	* create a form element
	* this is a function so it can be reused for multiple countries
	* this function allows the insertion of two types of elements: input[type=text] and select
	*
	* @param type - 'text' or 'select'
	* @param name - the element's name and id
	* @param labelText - the label text
	* @param unitText - the unit to use
	* @param options - the select option (inserted only if type = 'select')
	*
	* @returns {object} { block: the hole DOM block, input: the input or select element }
	*
	*/
	setFormElm ( type, name, labelText, unitText = null, options = [] ) {
		const formEl = document.getElementById( 'form-elm' ).cloneNode( false );
		formEl.removeAttribute( 'id' );

		const inputContainer = document.getElementById( 'form-elm-input-container' ).cloneNode( false );
		inputContainer.removeAttribute( 'id' );

		let inputEl = null;
		// let inputLabel = null;

		if ( type === 'input' ) {
			inputEl = document.getElementById( 'tsh' ).cloneNode( false );
			inputEl.id = name;
			inputEl.name = name;
		}

		if ( type === 'select' && options.length > 0 ) {
			// create the select element
			inputEl = document.createElement( 'select' );
			inputEl.id = name;
			inputEl.setAttribute( 'name', name );
			inputEl.setAttribute( 'required', '' );
			inputEl.classList.add( 'form-control' );
			// inputEl.classList.add( 'js-kioskboard-input' );
			// populate the select element with its options
			options.forEach( ( option ) => {
				presc.addHtmlElm( inputEl, 'beforeend', 'option', option, option.name );
			} );
		}

		const inputLabel = document.getElementById( 'form-elm-label' ).cloneNode( false );
		inputLabel.removeAttribute( 'id' );
		inputLabel.setAttribute( 'for', name );
		inputLabel.innerHTML = labelText;

		let inputUnit = null;
		if ( unitText ) {
			inputUnit = document.getElementById( 'form-elm-unit' ).cloneNode( false );
			inputUnit.removeAttribute( 'id' );
			inputUnit.innerHTML = unitText;
		} else {
			// if there is no unit to show, the select grow to "full-posible-width"
			inputContainer.classList.remove( 'col-8', 'col-md-5' );
			inputContainer.classList.add( 'col-12', 'col-md-7' );
		}

		const inputIcon = document.getElementById( 'form-elm-icon' ).cloneNode( true );
		inputIcon.removeAttribute( 'id' );

		inputContainer.appendChild( inputEl );
		inputLabel && inputContainer.appendChild( inputLabel );

		formEl.appendChild( inputContainer );
		inputUnit && formEl.appendChild( inputUnit );
		formEl.appendChild( inputIcon );

		return { block: formEl, input: inputEl };
	},

	/**
	* @function presc.addMonthsToAge()
	*
	* create a selection that replaces the gender selector
	* to select ages of less than one year
	*
	* @param monthsLabel (string) - the translated name for months
	*
	* @returns {element} the element to added to dom
	*
	*/
	addMonthsToAge ( monthsLabel = 'months' ) {
		const extraAge = `
			<div class="form-check">
				<input class="form-check-input" type="radio" name="ageMonths" id="ageMonths-0_3" value="ageMonths-0_3" required>
				<label class="form-check-label" for="ageMonths-0_3">
					<div class="option-icons">
						<span class="icon-baby-m"></span>
					</div>
					<p class="option-main">0-3 ${monthsLabel}</p>
				</label>
			</div>

			<div class="form-check">
				<input class="form-check-input" type="radio" name="ageMonths" id="ageMonths-3_6" value="ageMonths-3_6">
				<label class="form-check-label" for="ageMonths-3_6">
					<div class="option-icons">
						<span class="icon-baby-m"></span>
					</div>
					<p class="option-main">3-6 ${monthsLabel}</p>
				</label>
			</div>

			<div class="form-check">
				<input class="form-check-input" type="radio" name="ageMonths" id="ageMonths-6_12" value="ageMonths-6_12">
				<label class="form-check-label" for="ageMonths-6_12">
					<div class="option-icons">
						<span class="icon-baby-m"></span>
					</div>
					<p class="option-main">6-12 ${monthsLabel}</p>
				</label>
			</div>
		`;

		const extraAgeEl = document.createElement( "div" );
		extraAgeEl.classList.add( 'form-group', 'cols-3' );
		extraAgeEl.id = "age-m-wrapper";
		extraAgeEl.innerHTML = extraAge;

		document.querySelector( '#slideAge .range-container' ).insertAdjacentElement( 'afterend', extraAgeEl );

		return extraAge;
	},

	/**
	* @function presc.removeClassByPrefix()
	*
	* create a selection that replaces the gender selector
	* to select ages of less than one year
	* @link https://stackoverflow.com/questions/28608587/how-to-remove-a-class-that-starts-with
	*
	* @param el (HTML element) - the element to remove classes
	* @param prefix (string) - the class with given prefix to elmimiante
	* @returns {element} the element passed
	*
	*/
	removeClassByPrefix ( el, prefix ) {
		const regx = new RegExp( '\\b' + prefix + '.*?\\b', 'g' );
		el.className = el.className.replace( regx, '' );
		return el;
	},

	/**
 	* @object addLocalizedFields
 	*
 	* An object containing country specific functions that will modify the DOM
 	* to add (or remove) fields from the prescription calculator.
 	* Invoked after intial load.
 	*/
	addLocalizedFields: {
		default () {
			// nothing to change
		},

		pt_BR () {
			// append choice to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Supressão de TSH (câncer de tireoide) / nódulos / bócios eutireoidianos em adultos</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-1" value="preagnant-1" required>
					<label class="form-check-label" for="preagnant-1">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Gravidez 1º Trimestre</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-2" value="preagnant-2" required>
					<label class="form-check-label" for="preagnant-2">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Gravidez 2º Trimestre</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-3" value="preagnant-3" required>
					<label class="form-check-label" for="preagnant-3">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Gravidez 3º Trimestre</p>
					</label>
				</div>
			`;

			// change age slider options

			/* BR Value
			(initial)
			1 -> <12 years
			2 -> 12-18 years
			3 -> 19-40 years
			4 -> 41-64 years
			5 -> 45-80 years
			6 -> >80 years
			(followup)
			1 -> <1 year
			2 -> 1-12 years
			3 -> 13-18 years
			4 -> 18-40 years
			5 -> 41-64 years
			6 -> >64 years
			*/

			/*	# ATTENTION
				# For Brazil, initial and followup ages are different.
				# So the age-range setting changed to hideLocalizedConditionalFields section
			*/

			// const ageValues = ['&lt;1 ano', '1-12 anos', '13-18 anos', '18-40 anos', '41-64 anos', '&gt;64 anos'];

			/* document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} ) */

			// add the combobox for type of caculation
			// presc.addCalcTypeComboBox( "Status do paciente:", "Status do paciente", { initial: "Inicio do tratamento", followup: "Acompanhamento" } );

			console.warn( `(pt_BR) adding T4 input` );
			const t4AnchorElm = document.querySelector( '#slide-weight h4' );
			const objT4 = presc.setFormElm( 'input', 't4', 'Nível T4', 'ng/dL' );
			const t4block = objT4.block;
			t4AnchorElm.insertAdjacentElement( 'afterend', t4block );

			console.warn( `(pt_BR) adding ANICORPO input` );
			const anticorpoAnchorElm = document.querySelector( '#tsh' ).parentElement.parentElement;
			const objAnticorpo = presc.setFormElm( 'input', 'anticorpo', 'Anticorpo' );
			const anticorpoblock = objAnticorpo.block;
			const anticorpoInput = objAnticorpo.input;
			anticorpoInput.classList.add( 'js-kioskboard-select' );
			anticorpoInput.setAttribute( 'data-kioskboard-type', 'anticorpo' );
			anticorpoInput.setAttribute( 'data-kioskboard-select-behavior', true );
			anticorpoAnchorElm.insertAdjacentElement( 'afterend', anticorpoblock );

			KioskBoard.Init( {
				keysArrayOfObjects: [
					{
						0: "Negativo",
						1: "Positivo"
					}
				],
				keysAllowSpacebar: false,
				capsLockActive: false,
				keysFontFamily: 'Verdana',
				autoScroll: true
			} );
			KioskBoard.Run( '.js-kioskboard-select' );

			console.warn( `(pt_BR) changing TSH, Weight and Height input pattern` );
			/* eslint-disable no-useless-escape */

			// TSH levels must be higher than 0.4
			document.getElementById( 'tsh' ).pattern = "^0+([\.,][4-9])\\d*|^[^0]\\d*([\.,][0-9]+)?";

			// Anticorpo have a "like-select" behavior and doesn't need a pattern
			document.getElementById( 'anticorpo' ).pattern = "";

			// Height & weight must be higher than 0
			document.getElementById( 'height-cm' ).pattern = "^[^0]\\d*([\.,][0-9]+)?";
			document.getElementById( 'weight' ).pattern = "^[^0]\\d*([\.,][0-9]+)?";
			/* eslint-enable no-useless-escape */

			console.warn( `(pt_BR) add form feedbacks` );
			/* eslint-disable quote-props */
			// insert form feedbacks
			const t4InputContainer = document.getElementById( 't4' ).parentElement;
			presc.addHtmlElm( t4InputContainer, 'beforeend', 'div', { 'class': 'invalid-feedback', 'data-lang': '' }, "Os dados introduzidos não são válidos" );

			const tshInputContainer = document.getElementById( 'tsh' ).parentElement;
			presc.addHtmlElm( tshInputContainer, 'beforeend', 'div', { 'class': 'invalid-feedback', 'data-lang': '' }, "Os níveis de TSH devem ser superiores a 0.4mIU/L" );

			const anticorpoInputContainer = document.getElementById( 'anticorpo' ).parentElement;
			presc.addHtmlElm( anticorpoInputContainer, 'beforeend', 'div', { 'class': 'invalid-feedback', 'data-lang': '' }, "Os dados introduzidos não são válidos" );

			const weightInputContainer = document.getElementById( 'weight' ).parentElement;
			presc.addHtmlElm( weightInputContainer, 'beforeend', 'div', { 'class': 'invalid-feedback', 'data-lang': '' }, "Os dados introduzidos não são válidos" );

			const heightInputContainer = document.getElementById( 'height-cm' ).parentElement;
			presc.addHtmlElm( heightInputContainer, 'beforeend', 'div', { 'class': 'invalid-feedback', 'data-lang': '' }, "Os dados introduzidos não são válidos" );
			/* eslint-enable quote-props */
		},

		es_CL () {
			// append choice to conditions slide (3)
			// 1. Cancer de tiroides
			// 2. Alteraciones cardiovasculares
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Cáncer de tiroides (post-tiroidectomia)</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cardiac" value="alteraciones-cardiovasculares" required>
					<label class="form-check-label" for="cardiac">
						<div class="option-icons">
							<span class="icon-cardiac"></span>
						</div>
						<p class="option-main">Sospecha de alteraciones cardiovasculares</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_eutirox.png', width: '120', class: 'mb-3 mx-auto d-block', alt: 'Eutirox' } );

			// add the combobox for type of caculation
			presc.addCalcTypeComboBox( "Seleccione el tipo de cálculo que desea usar:", "Tipo de cálculo", { weight: "Guía ATA (Peso del Paciente)", tsh: "Guía GES (TSH)" } );

			// change age slider options
			// in chile the anges differ by type of calculation, this is for weight based
			/* CL Value
			1 -> <1 year
			2 -> 1-12 years
			3 -> 13-18 years
			4 -> 18-40 years
			5 -> 41-64 years
			6 -> >64 years
			*/

			const ageValues = ['&lt;1 año', '1-12 años', '13-18 años', '18-40 años', '41-64 años', '&gt;64 años'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )
		},

		pt_PT () {
			// change age slider options
			/* PT Value
			1 -> 0-6 months
			2 -> 7-11 months
			3 -> 1-5 years
			4 -> 6-10 years
			5 -> 11-18 years
			6 -> >18 years
			*/

			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			const ageValues = ['&le;6 meses', '7-11 meses', '1-5 anos', '6-10 anos', '11-18 anos', '&ge;18 anos'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )
		},

		es_CT () {
			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			// change age slider options
			/* CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 año', '1-5 años', '6-10 años', '11-18 años', '19-64 años', '&ge;65 años'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Terapia de supresión en cáncer tiroideo</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="post-operation" value="post-operation" required>
					<label class="form-check-label" for="post-operation">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Tratamiento de bocio eutiroideo benigno o Prevención de una nueva formación de bocio eutiroideo después de una operación</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_eutirox.png', width: '120', class: 'mb-3 mx-auto d-block', alt: 'Eutirox' } );
		},

		es_GT () {
			this.es_CT();
		},

		en_CT () {
			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			// change age slider options
			/* CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 year', '1-5 years', '6-10 years', '11-18 years', '19-64 years', '&ge;65 years'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Suppression therapy in thyroid cancer</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="post-operation" value="post-operation" required>
					<label class="form-check-label" for="post-operation">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Treatment of benign euthyroid goitre or Prophylaxis of relapse after surgery for euthyroid goitre</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_eutirox.png', width: '120', class: 'mb-3 mx-auto d-block', alt: 'Eutirox' } );
		},

		en_GT () {
			this.en_CT();
		},

		es_CO () {
			// change age slider options
			/* CO Value
			1 -> <1 year
			2 -> 1-12 years
			3 -> 13-18 years
			4 -> 18-40 years
			5 -> 41-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 año', '1-12 años', '13-18 años', '18-40 años', '41-64 años', '&ge;65 años'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				// console.log( el, i );
			} );

			console.log( "Replaced age range slider" );


			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Cáncer de tiroides (post-tiroidectomia)</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="bocio" value="bocio" required>
					<label class="form-check-label" for="bocio">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Bocio</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="infertility" value="infertility" required>
					<label class="form-check-label" for="infertility">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Infertilidad</p>
					</label>
				</div>
			`;

			console.log( "Added options to conditions slide" );
		},

		es_EC () {
			// change age slider options
			/* EC Value
			1 -> <1 year
			2 -> 1-12 years
			3 -> 13-18 years
			4 -> 18-40 years
			5 -> 41-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 año', '1-12 años', '13-18 años', '18-40 años', '41-64 años', '&ge;65 años'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				// console.log( el, i );
			} );

			console.log( "Replaced age range slider" );


			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Cáncer de tiroides (post-tiroidectomia)</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="bocio" value="bocio" required>
					<label class="form-check-label" for="bocio">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Bocio</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="infertility" value="infertility" required>
					<label class="form-check-label" for="infertility">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Infertilidad</p>
					</label>
				</div>
			`;

			console.log( "Added options to conditions slide" );
		},

		// peru
		es_PE () {
			// append choice to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Terapia de supresión en el cáncer tiroideo</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="bocio" value="bocio" required>
					<label class="form-check-label" for="bocio">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Tratamiento del bocio eutiroideo benigno</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-1" value="preagnant-1" required>
					<label class="form-check-label" for="preagnant-1">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Embarazo 1º Trimestre</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-2" value="preagnant-2" required>
					<label class="form-check-label" for="preagnant-2">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Embarazo 2º Trimestre</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-3" value="preagnant-3" required>
					<label class="form-check-label" for="preagnant-3">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Embarazo 3º Trimestre</p>
					</label>
				</div>
			`;

			// change age slider options

			/* BR Value
			1 -> <1 year
			2 -> 1-12 years
			3 -> 13-18 years
			4 -> 18-40 years
			5 -> 41-64 years
			6 -> >64 years
			*/

			const ageValues = ['&lt;1 año', '1-12 años', '13-18 años', '18-40 años', '41-64 años', '&gt;64 años'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// add the combobox for type of caculation
			presc.addCalcTypeComboBox( "Status del paciente:", "Status del paciente", { initial: "Inicio de tratamiento", followup: "Control del tratamiento" } );
		},

		es_MX () {
			// change age slider options
			/* MX Value
			1 -> 0-3 months
			2 -> 3-6 months
			3 -> 6-12 months
			4 -> 1-5 years
			5 -> 6-12 years
			6 -> 13-64 years
			7 -> >= 65 years
			*/

			// add the combobox for type of caculation
			presc.addCalcTypeComboBox( "Status del paciente:", "Status del paciente", { initial: "Inicio de tratamiento", followup: "Control del tratamiento" } );

			const ageValues = ['0-3 meses', '3-6 meses', '6-12 meses', '1-5 años', '6-12 años', '13-64 años', '&ge;65 años'];
			const ageIcons = ['baby-m', 'baby-m', 'boy', 'teen', 'man', 'woman', 'elder'];

			const AGE_ITEMS = 6;
			if ( ageValues.length > AGE_ITEMS ) {
				const ageContainer = document.querySelector( '.range-options' );
				const elmSlider = document.querySelector( '.range-slider' );

				const numItemsToAdd = ageValues.length - AGE_ITEMS;
				for ( let i = 0; i < numItemsToAdd; i++ ) {
					const newAgeItem = document.querySelector( '.range-option' ).cloneNode( false );
					newAgeItem.classList.remove( 'active' );
					ageContainer.insertAdjacentElement( 'beforeEnd', newAgeItem );
				}

				document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
					const iconClass = `icon-${ageIcons[i]}`;
					const classNames = el.className.split( ' ' );
					const newClassNames = classNames.map( ( item, index ) => { if ( item.includes( 'icon' ) ) { return iconClass } else { return item } } );
					el.classList = newClassNames.join( ' ' );
					el.dataset.rangeVal = i + 1;
				} );

				elmSlider.max = ageValues.length;
			}

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				// console.log( el, i );
			} );

			console.log( "Replaced age range slider" );


			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Cáncer de tiroides (post-tiroidectomia)</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="bocio" value="bocio" required>
					<label class="form-check-label" for="bocio">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Bocio</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="infertility" value="infertility" required>
					<label class="form-check-label" for="infertility">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Infertilidad</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-1" value="preagnant-1" required>
					<label class="form-check-label" for="preagnant-1">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Embarazo 1º Trimestre</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-2" value="preagnant-2" required>
					<label class="form-check-label" for="preagnant-2">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Embarazo 2º Trimestre</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preagnant-3" value="preagnant-3" required>
					<label class="form-check-label" for="preagnant-3">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main" data-lang="">Embarazo 3º Trimestre</p>
					</label>
				</div>
			`;

			console.log( "Added options to conditions slide" );
		},

		de_AT () {
			// change age slider options
			/* AT Value
			1 -> 0-3 months
			2 -> 4-11 months
			3 -> 1-3 years
			4 -> 4-10 years
			5 -> 11-16 years
			6 -> >16 years
			*/

			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			const ageValues = ['&le;3 monate', '4-11 monate', '1-3 jahre', '4-10 jahre', '11-16 jahre', '&ge;16 jahre'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )
		},

		pl_PL () {
			// change age slider options
			/* PT Value
			1 -> 0-6 months
			2 -> 7-11 months
			3 -> 1-5 years
			4 -> 6-10 years
			5 -> 11-18 years
			6 -> 18-64 years
			7 -> >64 years
			*/

			// remove tsh
			// document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			const ageValues = ['&le;6 miesięcy', '7-11 miesięcy', '1-5 lat', '6-10 lat', '11-18 lat', '18-64 lat', '&ge;64 lat'];
			const ageIcons = ['baby-m', 'baby-m', 'boy', 'teen', 'man', 'woman', 'elder'];

			const AGE_ITEMS = 6;
			if ( ageValues.length > AGE_ITEMS ) {
				const ageContainer = document.querySelector( '.range-options' );
				const elmSlider = document.querySelector( '.range-slider' );

				const numItemsToAdd = ageValues.length - AGE_ITEMS;
				for ( let i = 0; i < numItemsToAdd; i++ ) {
					const newAgeItem = document.querySelector( '.range-option' ).cloneNode( false );
					newAgeItem.classList.remove( 'active' );
					ageContainer.insertAdjacentElement( 'beforeEnd', newAgeItem );
				}

				document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
					const iconClass = `icon-${ageIcons[i]}`;
					const classNames = el.className.split( ' ' );
					const newClassNames = classNames.map( ( item, index ) => { if ( item.includes( 'icon' ) ) { return iconClass } else { return item } } );
					el.classList = newClassNames.join( ' ' );
					el.dataset.rangeVal = i + 1;
				} );

				elmSlider.max = ageValues.length;
			}

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				// console.log( el, i );
			} );

			console.log( "Replaced age range slider" );

			// add legal ref to final slide
			presc.addHtmlElm( '#slide-result .form-row', 'afterend', 'p', { class: 'info' }, "To narzędzie ma charakter edukacyjny w zakresie doboru dawki do terapii substytucyjnej w niedoczynności tarczycy (w celu zastąpienia naturalnych hormonów tarczycy, kiedy tarczyca nie produkuje ich w wystarczającej ilości). To nie jest wyrób medyczny. Przed dobraniem dawki należy wziąć pod uwagę indywidualną sytuację kliniczną danego pacjenta" );
			presc.addHtmlElm( '#slide-result .form-row', 'afterend', 'hr', { class: '' } );

			/* document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} ) */
		},

		en_SA () {
			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			// change age slider options
			/* CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 year', '1-5 years', '6-10 years', '11-18 years', '19-64 years', '&ge;65 years'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Suppression therapy in thyroid cancer</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="post-operation" value="post-operation" required>
					<label class="form-check-label" for="post-operation">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Treatment of benign euthyroid goitre or Prophylaxis of relapse after surgery for euthyroid goitre</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			// presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_eutirox.png', width: '120', class: 'mb-3 mx-auto d-block', alt: 'Eutirox' } );
		},

		en_ID () {
			this.en_CT();
		},

		en_PH () {
			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			// change age slider options
			/* CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 year', '1-5 years', '6-10 years', '11-18 years', '19-64 years', '&ge;65 years'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Suppression therapy in thyroid cancer</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="post-operation" value="post-operation" required>
					<label class="form-check-label" for="post-operation">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Treatment of benign euthyroid goitre or Prophylaxis of relapse after surgery for euthyroid goitre</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_archer.png', width: '120', class: 'mb-3 mx-auto d-block', alt: 'Eutirox' } );
		},

		en_ZA () {
			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			// change age slider options
			/* CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 year', '1-5 years', '6-10 years', '11-18 years', '19-64 years', '&ge;65 years'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Suppression therapy in thyroid cancer</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="post-operation" value="post-operation" required>
					<label class="form-check-label" for="post-operation">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Treatment of benign euthyroid goitre or Prophylaxis of relapse after surgery for euthyroid goitre</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_euthyrox.png', width: '125', class: 'mb-3 mx-auto d-block', alt: 'Euthyrox' } );
		},

		en_NG () {
			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			// change age slider options
			/* CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 year', '1-5 years', '6-10 years', '11-18 years', '19-64 years', '&ge;65 years'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Suppression therapy in thyroid cancer</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="post-operation" value="post-operation" required>
					<label class="form-check-label" for="post-operation">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Treatment of benign euthyroid goitre or Prophylaxis of relapse after surgery for euthyroid goitre</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_euthyrox.png', width: '125', class: 'mb-3 mx-auto d-block', alt: 'Euthyrox' } );
		},

		fr_MA () {
			// this.en_CT();

			// change age slider options
			/* MA Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-12 years
			4 -> 13-17 years
			5 -> 18-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 ans', '1-5 ans', '6-12 ans', '13-17 ans', '18-64 ans', '&ge;65 ans'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// add the months selection to 2nd slide
			presc.addMonthsToAge( 'mois' );

			// extra conditions
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="no-puberty" value="no-puberty" required>
					<label class="form-check-label" for="no-puberty">
						<div class="option-icons">
							<span class="icon-teen"></span>
						</div>
						<p class="option-main">age supérieur à 12 ans mais croissance et puberté non terminés</p>
					</label>
				</div>
				
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="acquired" value="acquired" required>
					<label class="form-check-label" for="acquired">
						<div class="option-icons">
							<span class="icon-thyroid-i"></span>
						</div>
						<p class="option-main">Hypothyroïdie Acquise</p>
					</label>
				</div>

				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="hypo-1" value="hypo-1" required>
					<label class="form-check-label" for="hypo-1">
						<div class="option-icons">
							<span class="icon-thyroid-i"></span>
						</div>
						<p class="option-main">Le patient adulte présentant une hypothyroïdie avérée</p>
					</label>
				</div>

				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="hypo-2" value="hypo-2" required>
					<label class="form-check-label" for="hypo-2">
						<div class="option-icons">
							<span class="icon-thyroid-i"></span>
						</div>
						<p class="option-main">Le patient présentant une hypothyroïdie fruste </p>
					</label>
				</div>

				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preg-4" value="preg-4" required>
					<label class="form-check-label" for="preg-4">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main">La patiente présentant une hypothyroïdie connue et planifiant une grossesse</p>
					</label>
				</div>

				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="preg-5" value="preg-5" required>
					<label class="form-check-label" for="preg-5">
						<div class="option-icons">
							<span class="icon-preagnant"></span>
						</div>
						<p class="option-main">Une primodécouverte de l'hypothyroidie durant la grossesse</p>
					</label>
				</div>

				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cancer-substitution" value="cancer-substitution" required>
					<label class="form-check-label" for="cancer-substitution">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Cancer de la thyroïde</p>
						<p class="option-desc">en substitution simple</p>
					</label>
				</div>

				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cancer-braking" value="cancer-braking" required>
					<label class="form-check-label" for="cancer-braking">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Cancer de la thyroïde</p>
						<p class="option-desc">à visée freinatrice</p>
					</label>
				</div>
			`;

			console.log( "Added options to conditions slide" );

			// extra first slide con
			document.querySelector( '#slide-start .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="pathology" id="cancer" value="cancer" required>
					<label class="form-check-label" for="cancer">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Cancer de la thyroïde</p>
						<!-- <p class="option-desc"></p> -->
					</label>
				</div>
			`;
		},

		en_KE () {
			// remove tsh
			document.getElementById( 'tsh' ).parentElement.parentElement.remove();

			// change age slider options
			/* CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			*/
			const ageValues = ['&lt;1 year', '1-5 years', '6-10 years', '11-18 years', '19-64 years', '&ge;65 years'];

			document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
				el.innerHTML = ageValues[i];
				console.log( el, i );
			} )

			// append choices to conditions slide (3)
			document.querySelector( '#slide-conditions .form-group' ).innerHTML += `
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="cns" value="cancer-nodules-supression" required>
					<label class="form-check-label" for="cns">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Suppression therapy in thyroid cancer</p>
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="condition" id="post-operation" value="post-operation" required>
					<label class="form-check-label" for="post-operation">
						<div class="option-icons">
							<span class="icon-molecule-i"></span>
						</div>
						<p class="option-main">Treatment of benign euthyroid goitre or Prophylaxis of relapse after surgery for euthyroid goitre</p>
					</label>
				</div>
			`;

			// add Eutirox logo to #slide-start
			// target element, position. tag type, {attribute: attribute value}, innerHtml ( = null by default)
			presc.addHtmlElm( '#slide-start > h4', 'afterend', 'img', { src: 'logo_euthyrox.png', width: '125', class: 'mb-3 mx-auto d-block', alt: 'Euthyrox' } );
		}
	},
	/**
	 * @object hideLocalizedConditionalFields
	 *
	 * An object containing country specific functions that will hide or show fields
	 * based on a certain criterion
	 * Invoked on slide change
	 */
	hideLocalizedConditionalFields: {
		default ( actualSlide, prevSlide, ageGroup = 3 ) {
			// [WARNING] SOME COUNTRIES INVOKE THIS FUNCTION :)
			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 18, hide #preagnant and #elder
					if ( el.name === 'age' && el.value <= ageGroup ) {
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
						document.querySelector( '#elder' ).parentElement.style.display = 'none';
					}
					// for men, hide #preagnant
					if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
					}
				} );
			}

			if ( actualSlide.id === 'slideAge' ) {
				// show #preagnant and #elder and #CNS when returning to age/sex selection
				setTimeout( () => {
					document.querySelectorAll( '#slide-conditions .form-check' ).forEach( el => {
						el.style.display = 'block';
						// console.log( el.firstElementChild );

						// TODO - reset next slide if prev slide values change
						// el.classList.remove('inactive')
						// el.firstElementChild.checked = false;
						// el.firstElementChild.dispatchEvent( new Event('change', { bubbles: true }) );
					} )
				}, 300 );
			}
		},

		pt_BR ( actualSlide, prevSlide ) {
			/* const isFollowUp = document.getElementById( 'calcType' ).value === 'followup';
			const isInitial = document.getElementById( 'calcType' ).value === 'initial'; */
			const isInitial = true;
			const ageGroup = isInitial ? 2 : 3;
			// also execute default local actions
			// set ageGroup to 2 (if initial) || 3 (if followup)
			this.default( actualSlide, prevSlide, ageGroup );

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// hide cancer/supression condition (too complex)
					document.querySelector( '#cns' ).parentElement.style.display = 'none';

					// for ages under 18 & over 65, hide #preagnant and #elder
					document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
					if ( el.name === 'age' && ( el.value <= 2 || el.value >= 5 ) ) {
						document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
					}
					// for men, hide #preagnant
					if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
						document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
					}

					// for ages under 12, hide cancer/supression condition
					/* if ( el.name === 'age' && el.value <= 2 ) {
						// console.log( 'hidden', el.name, el.value );

						document.querySelector( '#cns' ).parentElement.style.display = 'none';
					} */
				} );

				/* if ( isFollowUp ) {
					prevSlide.querySelectorAll( 'input' ).forEach( el => {
						// for ages under 18, hide #preagnant and #elder
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
						if ( el.name === 'age' && el.value <= 2 ) {
							document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
						}
						// for men, hide #preagnant
						if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
							document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
						}
					} );
				 } else {
					document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
					document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
					document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
				} */

				const condition = document.querySelector( 'input[name=condition]:checked' );
				if ( condition && condition.parentElement.style.display === 'none' ) {
					console.warn( 'Oh no! the condition selected is hidden!' );
					presc.resetSlidesNav( ['slideAge'] );
				}
			}

			const followupAgeValues = ['&lt;1 ano', '1-12 anos', '13-18 anos', '18-40 anos', '41-64 anos', '&gt;64 anos'];
			const initialAgeValues = ['&lt;12 anos', '12-18 anos', '19-40 anos', '41-64 anos', '65-80 anos', '&gt;80 anos'];
			const setAges = ( arrAges ) => {
				console.warn( `(pt_BR) setting age range` );
				document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
					el.innerHTML = arrAges[i];
					console.log( el, i );
				} )
			}

			// Gets the original text translation for slideAge help paragraph
			/* eslint-disable dot-notation */
			const orignalInfoText = translation.lang["Gender"];
			/* eslint-enable dot-notation */

			const setCustomInput = ( isInitial ) => {
				const el = document.querySelector( '[type="range"]' );
				// prevent double event for labels
				const isExtended = el.hasAttribute( 'data-extended' );

				el.isInitial = isInitial;

				if ( !isExtended ) {
					el.setAttribute( 'data-extended', '' );

					// if input already has an input function
					const hasPreviousInput = typeof el.oninput === 'function';
					// put into a variable to use call it later
					const initialInput = hasPreviousInput ? el.oninput : null;

					// extend element input function
					el.oninput = ( e ) => {
						if ( el.isInitial ) {
							// prevent calculations on ages under 12 years if "initial"
							if ( el.value < 2 ) {
								// If age 1 selected, remove 'done' class from all slides so one can't go further in calculation
								presc.resetSlidesNav();

								document.getElementById( 'female' ).parentElement.style.display = 'none';
								document.getElementById( 'male' ).parentElement.style.display = 'none';
								document.getElementById( 'female' ).parentElement.parentElement.previousElementSibling.innerHTML = `Por favor, consulte a bula`;
							} else {
								document.getElementById( 'female' ).parentElement.style = null;
								document.getElementById( 'male' ).parentElement.style = null;
								document.getElementById( 'female' ).parentElement.parentElement.previousElementSibling.style = null;
								document.getElementById( 'female' ).parentElement.parentElement.previousElementSibling.innerHTML = orignalInfoText;
							}

							// if there was an initial function, and the caller was an user click, call it too
							if ( hasPreviousInput ) {
								initialInput( e );
							}
						} else {
							document.getElementById( 'female' ).parentElement.style = null;
							document.getElementById( 'male' ).parentElement.style = null;
							document.getElementById( 'female' ).parentElement.parentElement.previousElementSibling.style = null;
							document.getElementById( 'female' ).parentElement.parentElement.previousElementSibling.innerHTML = orignalInfoText;

							if ( hasPreviousInput ) {
								initialInput( e );
							}
						}
					}
				}

				// dispatch a sintetic event to configure range input
				el.dispatchEvent( new CustomEvent( 'input', { detail: { byClick: false } } ) );
			}

			if ( isInitial ) {
				if ( prevSlide.id === 'slide-start' ) {
					setAges( initialAgeValues );

					// Show T4 & ANTICORPO inputs
					document.getElementById( 't4' ).parentElement.parentElement.style.display = null;
					document.getElementById( 'anticorpo' ).parentElement.parentElement.style.display = null;

					console.warn( `(pt_BR) custom behavior for age select` );
					setCustomInput( isInitial );
				}
			} else {
				if ( prevSlide.id === 'slide-start' ) {
					setAges( followupAgeValues );

					// Hide T4 & ANTICORPO inputs
					document.getElementById( 't4' ).parentElement.parentElement.style.display = 'none';
					document.getElementById( 'anticorpo' ).parentElement.parentElement.style.display = 'none';

					console.warn( `(pt_BR) RESET behavior for age select` );
					setCustomInput( isInitial );
				}
			}

			// check condition to add or remove ANTICORPOS input select
			if ( prevSlide.id === 'slideAge' ) {
				const sexSelected = document.querySelector( 'input[name="sex"]:checked' ).value;
				if ( sexSelected === 'male' ) {
					document.getElementById( 'anticorpo' ).parentElement.parentElement.style.display = 'none';
					document.getElementById( 'anticorpo' ).setAttribute( 'tabindex', '-1' );
				}
			}
			// add IF initial, female & preagnant
			if ( prevSlide.id === 'slide-conditions' && actualSlide.id === 'slide-weight' ) {
				const sexSelected = document.querySelector( 'input[name="sex"]:checked' ).value;
				const conditionSelected = document.querySelector( 'input[name="condition"]:checked' ).value;
				const isPreagnant = sexSelected === 'female' && conditionSelected.includes( 'preagnant' );

				if ( isPreagnant && isInitial ) {
					document.getElementById( 'anticorpo' ).parentElement.parentElement.style.display = null;
					document.getElementById( 'anticorpo' ).removeAttribute( 'tabindex' );
				} else {
					document.getElementById( 'anticorpo' ).parentElement.parentElement.style.display = 'none';
					document.getElementById( 'anticorpo' ).setAttribute( 'tabindex', '-1' );
				}
			}
		},

		es_CL ( actualSlide, prevSlide ) {
			// also execute default local actions
			// set ageGroup to 2
			this.default( actualSlide, prevSlide, 2 );

			// hide cancer option for tsh calculation
			/* if ( document.getElementById( 'calcType' ).value === 'tsh' ) {
				document.getElementById( 'cns' ).parentElement.style.display = 'none';
			} else {
				document.getElementById( 'cns' ).parentElement.style.display = 'block';
			} */

			function configTshWeight ( isWeight ) {
				if ( isWeight ) {
					// show cancer option for tsh calculation
					document.getElementById( 'cns' ).parentElement.style.display = 'block';
				} else {
					// hide cancer option for tsh calculation
					document.getElementById( 'cns' ).parentElement.style.display = 'none';
				}

				// Controls if the optional text will be added or removed
				function setOptionalText ( text, optional, action ) {
					switch ( action ) {
						case "add":
							return ( text.includes( optional ) ) ? text : `${text} ${optional}`;
						case "remove":
							return ( text.includes( optional ) ) ? text.replace( optional, "" ) : text;
						default:
							// Do nothing
					}
				}

				// TSH input is required only for TSH calculation
				const input = document.getElementById( 'tsh' );
				// Set required=false to tsh input for weight calculation
				input.required = !isWeight;

				// Set the "optional text". OBS: maybe it will be better set it in the translation options
				const txtOpcional = '(opcional)';
				const txtLabel = input.labels.item( 0 ).textContent;

				// add the (optional) text in the label for weight calculation && removes it for TSH
				const textAction = ( isWeight ) ? "add" : "remove";
				input.labels.item( 0 ).textContent = setOptionalText( txtLabel, txtOpcional, textAction );
			}

			// Set the calculator's diferent behaviors for Weight and TSH calculation
			const isWeight = document.getElementById( 'calcType' ).value === 'weight';
			configTshWeight( isWeight )

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 12, hide cancer/supression condition
					if ( el.name === 'age' && el.value <= 2 ) {
						console.log( 'hidden', el.name, el.value );
						document.querySelector( '#cns' ).parentElement.style.display = 'none';
					}
					// TSH calculation
					// for ages < 74
					// - hide cardiac condition (woman & man)
					if ( el.name === 'age' && el.value < 6 ) {
						if ( !isWeight ) {
							document.querySelector( '#cardiac' ).parentElement.style.display = 'none';
						}
					}
					// Weight calculation
					// for ages < 13
					// - hide cardiac condition
					if ( el.name === 'age' && el.value < 3 ) {
						if ( isWeight ) {
							document.querySelector( '#cardiac' ).parentElement.style.display = 'none';
						}
					}
					// TSH calculation
					// for ages > 74
					// - show elder condition (woman & man)
					// - hide healthy condition (woman & man)
					// - hide pregnancy condition (woman & man)
					// WEIGHT calculation
					// for ages > 64
					// - hide healthy condition (woman & man)
					if ( el.name === 'age' && el.value >= 6 ) {
						console.log( 'hidden', el.name, el.value );
						if ( !isWeight ) {
							document.querySelector( '#healthy' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
						} else {
							document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
						}
					}
				} );
			}

			// change the ages slider based on type of calculation
			if ( prevSlide.id === 'slide-start' ) {
				prevSlide.querySelectorAll( 'select' ).forEach( el => {
					if ( el.name === 'calcType' && el.value == 'weight' ) {
						const ageValues = ['&lt;1 año', '1-12 años', '13-18 años', '18-40 años', '41-64 años', '&gt;64 años'];
						document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
							el.innerHTML = ageValues[i];
						} )
					}
					if ( el.name === 'calcType' && el.value == 'tsh' ) {
						const ageValues = ['&lt;1 año', '1-15 años', '15-18 años', '18-40 años', '41-74 años', '&ge;75 años'];
						document.querySelectorAll( '.range-options > div' ).forEach( ( el, i ) => {
							el.innerHTML = ageValues[i];
						} )
					}
				} );
			}

			// Allways hide Elder condiction
			document.querySelector( '#elder' ).parentElement.style.display = 'none';
		},

		pt_PT ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 4 );
		},

		es_CT ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 4 );

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 12, hide cancer/supression condition
					if ( el.name === 'age' && el.value <= 4 ) {
						console.log( 'hidden', el.name, el.value );
						document.querySelector( '#elder' ).parentElement.style.display = 'none';
						document.querySelector( '#cns' ).parentElement.style.display = 'none';
						document.querySelector( '#post-operation' ).parentElement.style.display = 'none';
					}
				} );
			}
		},

		es_GT ( actualSlide, prevSlide ) {
			this.es_CT( actualSlide, prevSlide );
		},

		en_CT ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 4 );

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 12, hide cancer/supression condition
					if ( el.name === 'age' && el.value <= 4 ) {
						console.log( 'hidden', el.name, el.value );
						document.querySelector( '#elder' ).parentElement.style.display = 'none';
						document.querySelector( '#cns' ).parentElement.style.display = 'none';
						document.querySelector( '#post-operation' ).parentElement.style.display = 'none';
					}
				} );
			}
		},

		en_GT ( actualSlide, prevSlide ) {
			this.en_CT( actualSlide, prevSlide );
		},

		es_CO ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 2 );

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 12, hide cancer/supression condition
					if ( el.name === 'age' && el.value <= 2 ) {
						// console.log( 'hidden', el.name, el.value );

						document.querySelector( '#cns' ).parentElement.style.display = 'none';
						document.querySelector( '#bocio' ).parentElement.style.display = 'none';
						document.querySelector( '#infertility' ).parentElement.style.display = 'none';
					}

					if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
						document.querySelector( '#infertility' ).parentElement.style.display = 'none';
					}
				} );
			}
		},

		es_EC ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 2 );

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 12, hide cancer/supression condition
					if ( el.name === 'age' && el.value <= 3 ) {
						// console.log( 'hidden', el.name, el.value );

						document.querySelector( '#cns' ).parentElement.style.display = 'none';
						document.querySelector( '#bocio' ).parentElement.style.display = 'none';
						document.querySelector( '#infertility' ).parentElement.style.display = 'none';
					}

					if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
						document.querySelector( '#infertility' ).parentElement.style.display = 'none';
					}
				} );
			}
		},

		es_PE ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide );
			const isFollowUp = document.getElementById( 'calcType' ).value === 'followup';

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 12, hide cancer/supression condition
					if ( el.name === 'age' && el.value <= 2 ) {
						// console.log( 'hidden', el.name, el.value );

						document.querySelector( '#cns' ).parentElement.style.display = 'none';
						document.querySelector( '#bocio' ).parentElement.style.display = 'none';
					}
				} );

				if ( isFollowUp ) {
					// Show TSH if is follow up
					document.getElementById( 'tsh' ).parentElement.parentElement.style.display = 'flex';
					document.getElementById( 'tsh' ).required = true;
					// change slide title
					document.querySelector( '#slide-weight h4' )
						.innerHTML = "¿Cuáles son los niveles de TSH, el peso y la altura de su paciente?";
					document.querySelector( '#slide-weight p.help' )
						.innerHTML = "Inserte los niveles de TSH, el peso y la altura del paciente";

					prevSlide.querySelectorAll( 'input' ).forEach( el => {
						// for ages under 18, hide #preagnant and #elder
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
						if ( el.name === 'age' && el.value <= 2 ) {
							document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
						}
						// for men, hide #preagnant
						if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
							document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
							document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
						}
					} );
				} else {
					document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
					document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
					document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';

					// Hide TSH if is not follow up
					document.getElementById( 'tsh' ).parentElement.parentElement.style.display = 'none';
					document.getElementById( 'tsh' ).required = false;

					// change slide title
					document.querySelector( '#slide-weight h4' ).innerHTML = "¿Cuál es el peso y la altura de su paciente?";
					document.querySelector( '#slide-weight p.help' ).innerHTML = "Inserte el peso y la altura del paciente";
				}
			}
		},

		es_MX ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 5 );

			const isFollowUp = document.getElementById( 'calcType' ).value === 'followup';
			const isInitial = document.getElementById( 'calcType' ).value === 'initial';

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// hide 'pregnancy by trimester' when in initial
					if ( isInitial ) {
						document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
					}
					// hide 'pregnancy' when in followup
					if ( isFollowUp ) {
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
					}

					// for ages under 12, hide cancer/supression condition & bocio & infertility & pregnancy
					if ( el.name === 'age' && el.value <= 5 ) {
						// console.log( 'hidden', el.name, el.value );

						document.querySelector( '#cns' ).parentElement.style.display = 'none';
						document.querySelector( '#bocio' ).parentElement.style.display = 'none';
						/* document.querySelector( '#infertility' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none'; */
					}

					// for ages under 12 and over 64, hide pregnancy
					if ( el.name === 'age' && ( el.value <= 5 || el.value > 6 ) ) {
						document.querySelector( '#infertility' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
					}

					// for ages over 64, hide all but 'healthy' & 'elder'
					if ( el.name === 'age' && el.value > 6 ) {
						document.querySelector( '#bocio' ).parentElement.style.display = 'none';
						document.querySelector( '#cns' ).parentElement.style.display = 'none';
					}

					// for men, hide #preagnant
					if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
						document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
					}

					/* if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
						document.querySelector( '#infertility' ).parentElement.style.display = 'none';
					} */
				} );
			}
		},

		de_AT ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 4 );
		},

		pl_PL ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 5 );

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					if ( el.name === 'age' && el.value > 6 ) {
						document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
					}
				} );
			}
		},

		en_SA ( actualSlide, prevSlide ) {
			// also execute default local actions
			this.default( actualSlide, prevSlide, 4 );

			if ( prevSlide.id === 'slideAge' ) {
				prevSlide.querySelectorAll( 'input' ).forEach( el => {
					// for ages under 12, hide cancer/supression condition
					if ( el.name === 'age' && el.value <= 4 ) {
						console.log( 'hidden', el.name, el.value );
						document.querySelector( '#elder' ).parentElement.style.display = 'none';
						document.querySelector( '#cns' ).parentElement.style.display = 'none';
						document.querySelector( '#post-operation' ).parentElement.style.display = 'none';
					}
				} );
			}
		},

		en_ID ( actualSlide, prevSlide ) {
			this.en_CT( actualSlide, prevSlide )
		},

		en_PH ( actualSlide, prevSlide ) {
			this.en_CT( actualSlide, prevSlide )
		},

		en_ZA ( actualSlide, prevSlide ) {
			this.en_CT( actualSlide, prevSlide )
		},

		en_NG ( actualSlide, prevSlide ) {
			this.en_CT( actualSlide, prevSlide )
		},

		fr_MA ( actualSlide, prevSlide ) {
			// this.en_CT( actualSlide, prevSlide )

			// default conditions
			const healthySelect = document.getElementById( 'healthy' ).parentElement;
			const preagnantSelect = document.getElementById( 'preagnant' ).parentElement;
			const elderSelect = document.getElementById( 'elder' ).parentElement;

			// conditions added via JS for country
			const noPubertySelect = document.getElementById( 'no-puberty' ).parentElement;
			const acquiredSelect = document.getElementById( 'acquired' ).parentElement;
			const hypo1Select = document.getElementById( 'hypo-1' ).parentElement;
			const hypo2Select = document.getElementById( 'hypo-2' ).parentElement;
			const preg4Select = document.getElementById( 'preg-4' ).parentElement;
			const preg5Select = document.getElementById( 'preg-5' ).parentElement;
			const cancerSubstitutionSelect = document.getElementById( 'cancer-substitution' ).parentElement;
			const cancerBrakingSelect = document.getElementById( 'cancer-braking' ).parentElement;

			// hide preagancy
			preagnantSelect.style.display = 'none';

			const ageSelect = document.getElementById( 'age' );
			const cancerConditionSelect = document.getElementById( 'cancer' );
			const ageMonthWrapper = document.getElementById( 'age-m-wrapper' );
			const femaleSelect = document.getElementById( 'female' );
			const sexWrapper = femaleSelect.parentElement.parentElement;
			const sexTitle = sexWrapper.previousElementSibling;

			// hide month selection for first choice
			if ( ageSelect.value == 1 ) {
				sexWrapper.style.display = 'none';
				sexTitle.style.display = 'none';
			}
			// display age group or gender according to choice
			ageSelect.addEventListener( 'input', ( e ) => {
				if ( e.target.value == 1 ) {
					ageMonthWrapper.style.display = 'flex';
					sexWrapper.style.display = 'none';
					sexTitle.style.display = 'none';
				} else {
					ageMonthWrapper.style.display = 'none';
					sexWrapper.style.display = 'flex';
					sexTitle.style.display = 'block';

					// remove ageMonths selection
					ageMonthWrapper.querySelectorAll( '.form-check' ).forEach( el => {
						el.classList.remove( 'inactive' );
						el.querySelector( 'input' ).checked = false;
					} );
				}
			} );

			// restore defaults when going back
			if ( actualSlide.id === 'slideAge' ) {
				healthySelect.style.display = 'block';
				elderSelect.style.display = 'block';
				noPubertySelect.style.display = 'none';
				acquiredSelect.style.display = 'none';
				hypo1Select.style.display = 'none';
				hypo2Select.style.display = 'none';
				preg4Select.style.display = 'none';
				preg5Select.style.display = 'none';
				cancerSubstitutionSelect.style.display = 'none';
				cancerBrakingSelect.style.display = 'none';
			}

			if ( prevSlide.id === 'slideAge' ) {
				if ( cancerConditionSelect.checked == true ) {
					healthySelect.style.display = 'none';
					elderSelect.style.display = 'none';
					cancerSubstitutionSelect.style.display = 'block';
					cancerBrakingSelect.style.display = 'block';
				} else {
					if ( ageSelect.value <= 3 ) {
						elderSelect.style.display = 'none';
						acquiredSelect.style.display = 'block';
					}
					if ( ageSelect.value == 4 ) {
						elderSelect.style.display = 'none';
						healthySelect.style.display = 'none';
						acquiredSelect.style.display = 'block';
						noPubertySelect.style.display = 'block';
					}
					if ( ageSelect.value == 5 ) {
						elderSelect.style.display = 'block';
						healthySelect.style.display = 'none';
						hypo1Select.style.display = 'block';
						hypo2Select.style.display = 'block';
						if ( femaleSelect.checked ) {
							preg4Select.style.display = 'block';
							preg5Select.style.display = 'block';
						}
					}
				}
				// prevSlide.querySelectorAll( 'input' ).forEach( el => {
				// 	// hide cancer/supression condition (too complex)
				// 	document.querySelector( '#cns' ).parentElement.style.display = 'none';

				// 	// for ages under 18 & over 65, hide #preagnant and #elder
				// 	document.querySelector( '#preagnant' ).parentElement.style.display = 'none';
				// 	if ( el.name === 'age' && ( el.value <= 2 || el.value >= 5 ) ) {
				// 		document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
				// 		document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
				// 		document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
				// 	}
				// 	// for men, hide #preagnant
				// 	if ( el.name === 'sex' && el.value === 'male' && el.checked === true ) {
				// 		document.querySelector( '#preagnant-1' ).parentElement.style.display = 'none';
				// 		document.querySelector( '#preagnant-2' ).parentElement.style.display = 'none';
				// 		document.querySelector( '#preagnant-3' ).parentElement.style.display = 'none';
				// 	}

				// 	// for ages under 12, hide cancer/supression condition
				// 	/* if ( el.name === 'age' && el.value <= 2 ) {
				// 		// console.log( 'hidden', el.name, el.value );

				// 		document.querySelector( '#cns' ).parentElement.style.display = 'none';
				// 	} */
				// } );
			}
		},

		en_KE ( actualSlide, prevSlide ) {
			this.en_CT( actualSlide, prevSlide )
		}

	}

}
