// Functions to replace text on data-lang elements

function Translator ( langs = null ) {
	this.langs = langs;
	this.lang = null;
	this.defaultLang = {};
	this.els = document.querySelectorAll( '[data-lang]' );

	this.els.forEach( el => {
		const key = el.getAttribute( 'data-lang' );
		if ( key ) this.defaultLang[key] = el.innerHTML
		else this.defaultLang[el.innerHTML.replace( /<!--[\s\S]*?-->/g, '' ).trim()] = el.innerHTML;
	} );
}

// deprecated use -> populateTemplateFromString
Translator.prototype.populateTemplate = function ( lang ) {
	this.lang = lang;

	if ( typeof lang !== 'object' ) lang = this.defaultLang;

	this.els.forEach( el => {
		const key = el.getAttribute( 'data-lang' );

		if ( undefined !== lang[key] ) el.innerHTML = lang[key];
		else el.innerHTML = this.defaultLang[key];

		// console.log( 'replaced', key );
	} );
}

// uses innerHTML as reference when data-lang attrbute is empty
Translator.prototype.populateTemplateFromString = function ( lang ) {
	const status = { count: 0, terms: {} };
	this.lang = lang;

	if ( typeof lang !== 'object' ) lang = this.defaultLang;

	this.els.forEach( el => {
		let key;
		if ( !el.getAttribute( 'data-lang' ) ) {
			key = el.innerHTML.replace( /<!--[\s\S]*?-->/g, '' ).trim();
		} else {
			key = el.getAttribute( 'data-lang' );
		}

		if ( undefined !== lang[key] ) el.innerHTML = lang[key];
		else el.innerHTML = this.defaultLang[key];

		// just for logging for debugging purposes
		status.count++;
		status.terms[key] = lang[key];

		// console.log( 'replaced', key, this.defaultLang[key] );
	} );

	console.log( `Translated ${status.count} strings.`, status.terms );
}

// change the localized links to current language
Translator.prototype.updateLocalizedLinks = function ( country ) {
	// find all a tags containing country in href
	document.querySelectorAll( 'a[href*="country"]' ).forEach( el => {
		const url = new URL( el.href );
		const searchParams = url.searchParams;

		// new value of "id" is set to "101"
		searchParams.set( 'country', country );

		// change the search property of the main url
		url.search = searchParams.toString();

		// the new url string
		const newUrl = url.toString();

		// change the element href
		el.href = newUrl;
	} );
}

// takes an array of references and replaces in the specific part
Translator.prototype.populateLocalizedRefs = function ( refs ) {
	const el = document.getElementById( 'populate-refs' );

	// check if there is a refs element in the page
	if ( el ) {
		// clean the element first
		el.innerHTML = '';

		for ( const selection in refs ) {
			const newLi = document.createElement( 'li' );
			newLi.innerHTML = refs[selection];

			el.appendChild( newLi );
		}
	}
}

Translator.prototype.importLangsJson = function ( langObject ) {
	this.langs = langObject;
}
