/**
 * Localized calculation of dosages functions and content
 */

// eslint-disable-next-line no-unused-vars
const presCalc = {
	form: null,
	pathology: null,
	age: null,
	ageMonths: null,
	bmi: null,
	bsa: null,
	initialDose: null,
	maintenanceDose: null,
	tshDose: null,
	followupDose: null,
	resultTxt: null,
	generalResultTxt: null,
	refNum: {
		initialDose: '',
		maintenanceDose: '',
		tshDose: '',
		followupDose: ''
	},

	sanitizeInput ( value ) {
		if ( typeof value === 'string' ) {
			return parseFloat( value.replace( ',', '.' ) );
		}
		return value;
	},

	// pass form data and calculate BMI/BSA
	setupData ( form ) {
		this.form = form;
		/* pathologies
			- hypothyroidism
			- hemithyroidectomy
			- thyroidectomy
			- hyperthyroidism
			- cancer
		*/
		this.pathology = form.data.pathology;

		/*  Age correspondance ___
			INT
			1 -> <1 year
			2 -> 1-4 years
			3 -> 5-18 years
			4 -> 18-40 years
			5 -> 41-55 years
			6-> >55 years
			BR+CL(weight)
			1 -> <1 year
			2 -> 1-12 years
			3 -> 13-18 years
			4 -> 18-40 years
			5 -> 41-64 years
			6-> >64 years
			CL(tsh)
			1 -> <1 year
			2 -> 1-15 years
			3 -> 16-18 years
			4 -> 18-40 years
			5 -> 41-74 years
			6-> >=75 years
			PT
			1 -> 0-6 months
			2 -> 7-11 months
			3 -> 1-5 years
			4 -> 6-10 years
			5 -> 11-18 years
			6-> > >18 years
			CT Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			CO + EC Value
			1 -> <1 year
			2 -> 1-12 years
			3 -> 13-18 years
			4 -> 18-40 years
			5 -> 41-64 years
			6 -> >= 65 years
			MX
			1 -> 0-6 months
			2 -> 7-11 months
			3 -> 1-5 years
			4 -> 6-10 years
			5 -> 11-18 years
			6-> > >18 years
			AT
			1 -> 0-3 months
			2 -> 4-11 months
			3 -> 1-3 years
			4 -> 4-10 years
			5 -> 11-16 years
			6 -> >16 years
			PL
			1 -> 0-6 months
			2 -> 7-11 months
			3 -> 1-5 years
			4 -> 6-10 years
			5 -> 11-18 years
			6-> > >18 years
			PH Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-10 years
			4 -> 11-18 years
			5 -> 19-64 years
			6 -> >= 65 years
			MA Value
			1 -> <1 year
			2 -> 1-5 years
			3 -> 6-12 years
			4 -> 13-17 years
			5 -> 18-64 years
			6 -> >= 65 years
		*/
		this.age = form.data.age;

		// check if it has months field (initial development MA)
		this.ageMonths = ( Object.prototype.hasOwnProperty.call( form.data, 'ageMonths' ) ) ? form.data.ageMonths : null;

		// Sanitização das entradas de peso e altura
		const weight = this.sanitizeInput( form.data.kg );
		const height = this.sanitizeInput( form.data.cm );

		if ( weight && height ) {
			// body mass index
			this.bmi = ( weight / Math.pow( ( height / 100 ), 2 ) ).toFixed( 1 );
			// Mosteller Formula for body surface area
			this.bsa = ( Math.sqrt( ( height * weight ) / 3600 ) ).toFixed( 3 );
		} else {
			this.bmi = null;
			this.bsa = null;
		}

		// reset the refs
		this.refNum.initialDose = '';
		this.refNum.maintenanceDose = '';
		this.refNum.tshDose = '';
	},

	// default calculations - No country specified
	default () {
		switch ( this.pathology ) {
			case 'hypothyroidism':
				if ( this.age <= 2 ) {
					this.initialDose = `${Math.ceil( 10 * this.form.data.kg )}-${Math.ceil( 15 * this.form.data.kg )}`;
					this.maintenanceDose = `-`;
				}
				if ( this.age == 3 ) {
					this.initialDose = `12.5-50`;
					this.maintenanceDose = `${Math.ceil( 100 * this.bsa )}-${Math.ceil( 150 * this.bsa )}`;
				}
				if ( this.age == 4 ) {
					this.initialDose = `25-50`;
					if ( bmi < 24 ) {
						this.maintenanceDose = `${Math.ceil( 1.8 * this.form.data.kg )}`;
					}
					if ( bmi >= 24 && bmi < 29 ) {
						this.maintenanceDose = `${Math.ceil( 1.7 * this.form.data.kg )}`;
					}
					if ( bmi >= 29 ) {
						console.log( 'hey there!!!!' );
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					}
				}

				if ( this.age == 5 ) {
					console.log( bmi );
					this.initialDose = `25-50`;
					if ( bmi < 24 ) {
						this.maintenanceDose = `${Math.ceil( 1.7 * this.form.data.kg )}`;
					}
					if ( bmi >= 24 && bmi < 29 ) {
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					}
					if ( bmi >= 29 ) {
						this.maintenanceDose = `${Math.ceil( 1.5 * this.form.data.kg )}`;
					}
				}

				if ( this.age == 6 ) {
					this.initialDose = `${( 12.5 * this.form.data.kg ).toFixed( 1 )}`;
					if ( bmi < 24 ) {
						this.maintenanceDose = `${( 1.6 * this.form.data.kg ).toFixed( 1 )}`;
					}
					if ( bmi >= 24 && bmi < 29 ) {
						this.maintenanceDose = `${( 1.5 * this.form.data.kg ).toFixed( 1 )}`;
					}
					if ( bmi >= 29 ) {
						this.maintenanceDose = `${( 1.4 * this.form.data.kg ).toFixed( 1 )}`;
					}
				}

				break;

			// NOT USED AT THE MOMENT
			case 'hemithyroidectomy' :
			case 'thyroidectomy' :
				this.initialDose = ` - `;
				this.maintenanceDose = `75-200`;
				break;

			case 'hyperthyroidism' :
				this.initialDose = ` - `;
				this.maintenanceDose = `50-100`;
				break;

			case 'cancer' :
				this.initialDose = ` - `;
				this.maintenanceDose = `150-300`;
				break;
		}

		// There is no text for results right now
		this.resultTxt = '<p>This text needs to be reviewed according to the calculations</p>'
	},

	// formulas for brazil
	pt_BR () {
		console.log( this.form.data );
		// if ( this.form.data.calcType == 'initial' ) {
		/* if ( this.age == 1 ) {
			this.initialDose = `${Math.ceil( 5 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.initialDose = '1';
			// this.maintenanceDose = `${Math.ceil( 2.5 * this.form.data.kg )}`;

			this.resultTxt = '<p>No recém-nascido, a posologia inicial deverá ser de 5 a 6 mcg/kg/dia em função da dosagem dos hormônios circulantes.</p><p>Para a dose inicial sugerida, por favor, consulte a bula do medicamento.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.initialDose = '1';
			// this.maintenanceDose = `${Math.ceil( 2.5 * this.form.data.kg )}`;
			this.resultTxt = '<p>Na criança a posologia deve ser estabelecida em função dos resultados das dosagens hormonais e em geral é de 3mcg/kg/dia.</p><p>Para a dose inicial sugerida, por favor, consulte a bula do medicamento.</p>';
		} */

		if ( this.age <= 1 ) {
			console.log( 'There are no calculations to apply here.' );
		}

		const tsh = this.sanitizeInput( this.form.data.tsh );
		const t4 = this.sanitizeInput( this.form.data.t4 );

		if ( this.age == 2 ) {
			// console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				if ( tsh >= 0.4 && tsh <= 4.5 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `<p>Sugestão de não tratamento por apresentar parâmetros normais. Veja a referência nº 5</p>`;
				} else if ( tsh > 4.5 && tsh < 7 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p><strong>Se sintomático:</strong> 50 mcg/dia<sup>1</sup></p>
						<p><strong>Dose inicial:</strong><br/>
						50 mcg/dia, aumentando-se 25 mcg a cada 6 - 8 semanas até que o efeito desejado seja alcançado. Em pacientes com hipotireoidismo de longa data, particularmente com suspeita de alterações cardiovasculares, a dose inicial deverá ser ainda mais baixa (25mcg/dia).</p>
					`;
				} else if ( tsh >= 7 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = `${Math.ceil( 1.1 * this.form.data.kg )}`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = ``;
				} else if ( tsh > 4.5 && t4 <= 0.7 ) {
					this.initialDose = `50`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p><strong>Dose inicial:</strong><br/>
						50 mcg/dia, aumentando-se 25 mcg a cada 6 - 8 semanas até que o efeito desejado seja alcançado. Em pacientes com hipotireoidismo de longa data, particularmente com suspeita de alterações cardiovasculares, a dose inicial deverá ser ainda mais baixa (25mcg/dia).</p>
					`;
				}
			}
			/* if ( this.form.data.condition == 'preagnant-1' ) {
				this.initialDose = `50`;
				this.refNum.initialDose = '1';
			}
			if ( this.form.data.condition == 'preagnant-2' ) {
				this.initialDose = `50`;
				this.refNum.initialDose = '1';
			}
			if ( this.form.data.condition == 'preagnant-3' ) {
				this.initialDose = `50`;
				this.refNum.initialDose = '1';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				// this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				// this.refNum.maintenanceDose = '2';
			}

			// text applies to the 3 conditions above
			this.resultTxt = `
			<p><b>Hipotireoidismo</b>: Euthyrox<sup>®</sup> deve ser administrado em doses baixas (50 mcg/dia) que serão aumentadas de acordo com as condições cardiovasculares do paciente.</p>

			<p><b>Dose inicial</b>: 50 mcg/dia, aumentando-se 25 mcg a cada 2 ou 3 semanas até que o efeito desejado seja alcançado. Em pacientes com hipotireoidismo de longa data, particularmente com suspeita de alterações cardiovasculares, a dose inicial deverá ser ainda mais baixa (25mcg/dia).</p>

			<p><b>Manutenção</b>: recomenda-se 75 a 125 mcg diários sendo que alguns pacientes, com má absorção, podem necessitar de até 200 mcg/dia. A maioria dos pacientes não exige doses superiores a 150 mcg/dia. A falta de resposta ás doses de 200 mcg/dia sugere má absorção, não obediência ao tratamento ou erro diagnóstico.</p>

			<p><b>Pacientes Idosos</b>: No idoso, a integridade do sistema cardiovascular pode estar comprometida. Por isso, neste paciente a terapia com Euthyrox® deve ser iniciada com doses baixas, como por exemplo 25-50 mcg/dia.</p>
			`;

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `${Math.ceil( 2.6 * this.form.data.kg )}`;
				this.refNum.initialDose = '1';
				// this.maintenanceDose = `${Math.ceil( 2.2 * this.form.data.kg )}`;
				this.resultTxt = `<p>Dose supressiva média de levotiroxina (T4): 2,6 mcg/kg/dia, durante 7 a 10 dias. Essa dose geralmente é suficiente para obter normalização dos níveis séricos de T3 e T4 e falta de resposta à ação de TSH. A Levotiroxina sódica deve ser empregada com cautela em pacientes com suspeita de glândula tireoide autônoma, considerando que a ação dos hormônios exógenos pode sumar-se aos hormônios de fonte endógena.</p>`;
			} */
		}

		if ( this.age == 3 || this.age == 4 ) {
			if ( this.form.data.condition == 'healthy' ) {
				if ( tsh > 0.4 && tsh <= 4.5 && t4 >= 0.7 && t4 <= 1.8 ) {
					// this.initialDose = ` - `;
					// this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `<p>Sugestão de não tratamento segundo referência 5.</p><hr>`;
				} else if ( tsh > 4.5 && tsh < 7 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p><strong>Se sintomático:</strong> 50 mcg/dia<sup>1</sup></p>
						<p><strong>Dose inicial:</strong><br/>
						50 mcg/dia, aumentando-se 25 mcg a cada 6 - 8 semanas até que o efeito desejado seja alcançado. Em pacientes com hipotireoidismo de longa data, particularmente com suspeita de alterações cardiovasculares, a dose inicial deverá ser ainda mais baixa (25mcg/dia).</p>
					`;
				} else if ( tsh >= 7 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = `${Math.ceil( 1.1 * this.form.data.kg )}`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = ``;
				} else if ( tsh > 4.5 && t4 <= 0.7 ) {
					const theDose = Math.ceil( 1.6 * this.form.data.kg );
					this.initialDose = ( theDose > 200 ) ? `200` : `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `Se a dose sugerida for 200mcg, recomenda-se procurar um especialista`;
				}
			}

			if ( this.form.data.condition == 'elder' ) {
				if ( tsh >= 4.5 ) {
					this.initialDose = `50`;
					this.refNum.initialDose = '1,2,3';
					this.resultTxt = `<p><b>Dose Inicial: 50 mcg/dia</b>, aumentando-se 25 mcg a cada 6 - 8 semanas até que o efeito desejado seja alcançado. Em pacientes com hipotireoidismo de longa data, particularmente com suspeita de alterações cardiovasculares, a dose inicial deverá ser ainda mais baixa (25mcg/dia).</p>`;
				}
				if ( tsh < 4.5 ) {
					this.initialDose = `-`;
					this.refNum.initialDose = '';
					this.resultTxt = `<p>Sugestão de não tratamento segundo referência 5.</p>`;
				}
			}

			if ( this.form.data.condition == 'preagnant-1' || this.form.data.condition == 'preagnant-2' || this.form.data.condition == 'preagnant-3' ) {
				if ( tsh > 0 && tsh <= 2.5 ) {
					this.initialDose = `-`;
					this.refNum.initialDose = '1,&nbsp;3,&nbsp;4';
					this.resultTxt = `<p>Sugestão de não tratamento segundo referência 5.</p>`;
				}

				if ( tsh > 2.5 && tsh <= 4 ) {
					if ( this.form.data.anticorpo == 'positivo' ) {
						this.initialDose = `Tratar com dose: 50`;
						this.refNum.initialDose = '1,&nbsp;3,&nbsp;4';
						this.resultTxt = `<p>Qualquer acompanhamento bioquímico na gestação deve ser realizado a cada 4 semanas.</p>`;
					} else {
						this.initialDose = `-`;
						this.refNum.initialDose = '1,&nbsp;3,&nbsp;4';
						this.resultTxt = `<p>Sugestão de não tratamento segundo referência 5.</p>`;
					}
				}
				if ( tsh > 4 && tsh <= 10 ) {
					if ( t4 < 0.7 ) {
						const theDose = Math.ceil( 2 * this.form.data.kg );
						this.initialDose = `${theDose}`;
						this.refNum.initialDose = '1,&nbsp;3,&nbsp;4';
						this.resultTxt = `<p>Qualquer acompanhamento bioquímico na gestação deve ser realizado a cada 4 semanas.</p>`;
					}
					if ( t4 >= 0.7 && t4 <= 1.8 ) {
						const theDose = Math.ceil( 1 * this.form.data.kg );
						this.initialDose = `${theDose}`;
						this.refNum.initialDose = '1,&nbsp;3,&nbsp;4';
						this.resultTxt = `<p>Qualquer acompanhamento bioquímico na gestação deve ser realizado a cada 4 semanas.</p>`;
					}
				}
				if ( tsh > 10 ) {
					const theDose = Math.ceil( 2 * this.form.data.kg );
					this.initialDose = `${theDose}`;
					this.refNum.initialDose = '1,&nbsp;3,&nbsp;4';
					this.resultTxt = `<p>Qualquer acompanhamento bioquímico na gestação deve ser realizado a cada 4 semanas.</p>`;
				}
			}
		}

		if ( this.age == 5 ) {
			if ( this.form.data.condition == 'healthy' ) {
				if ( tsh >= 10 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = `50`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p><strong>Dose inicial:</strong> 50 mcg/dia, verificar parâmetros bioquímicos a cada 6 - 8 semanas para avaliação de possível ajuste de dose.
						</p>
					`;
				}
			}

			if ( this.form.data.condition == 'elder' ) {
				if ( tsh >= 10 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = `25`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p>Titulação até o controle. Revisar exames bioquímicos a cada 6-8 semanas.</p>
					`;
				}
			}
		}

		if ( this.age >= 5 ) {
			if ( this.form.data.condition == 'healthy' ) {
				if ( tsh >= 0.4 && tsh <= 4.5 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '3';
					this.resultTxt = `<p>Sugestão de não tratamento segundo a referência 3.</p>`;
				}
				if ( tsh > 4.5 && t4 <= 0.7 ) {
					this.initialDose = `50`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p><strong>Dose inicial:</strong> 50 mcg/dia, aumentando-se 25 mcg a cada 6 - 8 semanas até que o efeito desejado seja alcançado. Em pacientes com hipotireoidismo de longa data, particularmente com suspeita de alterações cardiovasculares, a dose inicial deverá ser ainda mais baixa (25mcg/dia).</p>
					`;
				}
				if ( tsh > 4.5 && tsh < 10 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p>Acompanhar evolução clínica e os parâmetros bioquímicos do paciente a cada 6 a 12 meses</p>
					`;
				}
			}
			if ( this.form.data.condition == 'elder' ) {
				if ( tsh >= 0.4 && tsh < 4.5 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '3';
					this.resultTxt = `<p>Sugestão de não tratamento segundo a referência 3.</p>`;
				}
				if ( tsh > 4.5 && t4 <= 0.7 ) {
					this.initialDose = `25`;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p><strong>Dose inicial:</strong> 50 mcg/dia, aumentando-se 25 mcg a cada 6 - 8 semanas até que o efeito desejado seja alcançado. Em pacientes com hipotireoidismo de longa data, particularmente com suspeita de alterações cardiovasculares, a dose inicial deverá ser ainda mais baixa (25mcg/dia).</p>
					`;
				}
				if ( tsh > 4.5 && tsh < 10 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p>Acompanhar evolução clínica e os parâmetros bioquímicos do paciente a cada 6 a 12 meses</p>
					`;
				}
			}
		}

		if ( this.age == 6 ) {
			if ( this.form.data.condition == 'healthy' || this.form.data.condition == 'elder' ) {
				if ( tsh >= 10 && t4 >= 0.7 && t4 <= 1.8 ) {
					this.initialDose = ` - `;
					this.refNum.initialDose = '1,&nbsp;3';
					this.resultTxt = `
						<p>Titulação até o controle. Revisar exames bioquímicos a cada 6-8 semanas.</p>
					`;
				}
			}
		}
		// }

		/* if ( this.form.data.calcType == 'followup' ) {
			if ( this.form.data.condition == 'healthy' || this.form.data.condition == 'elder' || this.form.data.condition == 'cancer-nodules-supression' ) {
				this.resultTxt = `<p>Manutenção: recomenda-se 75 a 125 mcg diários sendo que alguns pacientes, com má absorção, podem necessitar de até 200 mcg/dia. A maioria dos pacientes não exige doses superiores a 150 mcg/dia. A falta de resposta ás doses de 200 mcg/dia sugere má absorção, não obediência ao tratamento ou erro diagnóstico.</p>`;

				if ( this.form.data.tsh < 0.4 ) {
					this.followupDose = 'Diminuir a dose';
				}
				if ( this.form.data.tsh >= 0.4 && this.form.data.tsh < 4.5 ) {
					this.followupDose = 'Manter o tratamento';
				}
				if ( this.form.data.tsh >= 4.5 ) {
					this.followupDose = 'Aumentar a dose';
				}
			}
			if ( this.form.data.condition == 'preagnant-1' ) {
				this.resultTxt = `<p>1º trimestre: 0,1 – 2,5 mUI/mL. Segundo a Associação Americana de Tiroide (ATA), no primeiro trimestre o valor superior de normalidade deve ser reduzido em 0,5 mUI/L, o que corresponde a 4,0 mUI/L quando utilizado este método.</p>`;

				if ( this.form.data.tsh < 0.1 ) {
					this.followupDose = 'Diminuir a dose'
				}
				if ( this.form.data.tsh >= 0.1 && this.form.data.tsh <= 2.5 ) {
					this.followupDose = 'Manter o tratamento'
				}
				if ( this.form.data.tsh > 2.5 ) {
					this.followupDose = 'Aumentar a dose'
				}
			}
			if ( this.form.data.condition == 'preagnant-2' ) {
				this.resultTxt = `<p>2º trimestre: 0,2 – 3,5 mUI/mL</p>`;

				if ( this.form.data.tsh < 0.2 ) {
					this.followupDose = 'Diminuir a dose'
				}
				if ( this.form.data.tsh >= 0.2 && this.form.data.tsh <= 3.5 ) {
					this.followupDose = 'Manter o tratamento'
				}
				if ( this.form.data.tsh > 3.5 ) {
					this.followupDose = 'Aumentar a dose'
				}
			}
			if ( this.form.data.condition == 'preagnant-3' ) {
				this.resultTxt = `<p>3º trimestre: 0,3 – 3,5 mUI/mL</p>`;

				if ( this.form.data.tsh < 0.3 ) {
					this.followupDose = 'Diminuir a dose'
				}
				if ( this.form.data.tsh >= 0.3 && this.form.data.tsh <= 3.5 ) {
					this.followupDose = 'Manter o tratamento'
				}
				if ( this.form.data.tsh > 3.5 ) {
					this.followupDose = 'Aumentar a dose'
				}
			}
		} */

		// Text common to all results
		// <p><strong>Dose máxima de segurança: 250mcg</strong> - Consultar especialista acima desta dose.</p>
		this.generalResultTxt = `
			
			<p>As doses administradas de Euthyrox<sup>®</sup> variam de acordo com o grau de hipotireoidismo, a idade do paciente e a tolerabilidade individual. A fim de se adaptar a posologia, é recomendável antes de iniciar o tratamento efetuar as dosagens raioimunológicas do (T3), (T4) e TSH. Os comprimidos devem ser administrados com Líquido, via oral.
			<br/><br/>
			Os comprimidos de Euthyrox<sup>®</sup> devem ser ingeridos com estômago vazio (30 minutos à 1 hora antes ou 2 horas após o café da manhã ou ingestão de alimento), a fim de aumentar sua absorção.</p>

			<h5 class="mt-2">Tabela de Diagnóstico laboratorial do Hipotireodismo Primário</h5>

			<table class="table">
				<tr>
					<th>&nbsp;</th>
					<th>TSH</th>
					<th>T4 Livre</th>
				</tr>
				<tr>
					<td>Normal</td>
					<td>0,4 &ndash; 4,5 mUI/L</td>
					<td>0,7 &ndash; 1,8 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotireodismo Primário</td>
					<td>&gt; 4,5 mUI/L</td>
					<td>&lt; 0,7 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotireodismo Subclínico</td>
					<td>&gt; 4,5 mUI/L</td>
					<td>0,7 &ndash; 1,8 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotireodismo Central</td>
					<td>TSH &gt; 0,4 mUI/L (ou normal)</td>
					<td>&lt; 0,7 ng/dL</td>
				</tr>
			</table>
		`;
	},

	// formulas for Chile
	es_CL () {
		if ( this.form.data.calcType == 'weight' ) {
			if ( this.age == 1 || this.age == 2 ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 2.5 * this.form.data.kg )}`;

				this.resultTxt = '<p>Al decidir la dosis inicial de levotiroxina, el peso del paciente, masa corporal magra, estado de embarazo, etiología de hipotiroidismo, grado de elevación de tirotropina, edad y contexto clínico general, incluida la presencia de enfermedad cardíaca, todos deben ser considerados.Además, el objetivo de tirotropina sérica apropiado para la situación clínica también debe tenerse en cuenta.<sup>3</sup></p>';
			}
			if ( this.age >= 3 ) {
				console.log( this.form.data.condition );
				if ( this.form.data.condition == 'healthy' ) {
					this.initialDose = `25-50`;
					this.refNum.initialDose = '1';
					this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '3';
				}

				if ( this.form.data.condition == 'preagnant' ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
					this.refNum.maintenanceDose = '3';

					this.resultTxt = `
						<h3>TRATAMIENTO DURANTE EL EMBARAZO Y LACTANCIA<sup>1</sup></h3>

						<p>El tratamiento consecuente con hormonas tiroideas es especialmente importante durante el embarazo y la lactancia y, por lo tanto, debe continuarse. Los requerimientos de dosis pueden incluso aumentarse durante el embarazo. Ya que la elevación de la TSH sérica puede ocurrir tan pronto como las 4 semanas de gestación, las mujeres embarazadas que toman levotiroxina deben tener su THS medida durante cada trimestre, para confirmar que los valores de TSH materno se encuentran dentro del intervalo de referencia del embarazo específico del trimestre. </p>

						<p>Un nivel elevado de TSH en suero debe ser corregido por un aumento en la dosis de levotiroxina. Dado que los niveles de TSH posparto son similares a los valores previos a la concepción, la dosis de levotiroxina debe volver a la dosis antes del embarazo inmediatamente después del parto. Se debe obtener un nivel sérico de TSH de 6 a 8 semanas después del parto.</p>

						<h4>El embarazo</h4>
						<p>Las pruebas de diagnóstico de supresión tiroidea deben evitarse durante el embarazo, ya que la aplicación de sustancias radiactivas en mujeres embarazadas está contraindicada. No existe evidencia de teratogenicidad drogo-inducida y/o feto-toxicidad en humanos con
						niveles de dosis terapéuticamente recomendados. Niveles de dosis excesivamente altos de levotiroxina durante la lactancia pueden tener efectos negativos en el desarrollo fetal y postnatal.
						Sin embargo, la levotiroxina no debe administrarse junto con medicamentos contra una hiperfunción tiroidea (tireostáticos) durante el embarazo, ya que una medicación adicional con levotiroxina puede hacer necesaria una mayor dosificación de tireostáticos que pasan
						a través de la placenta e inducen el hipotiroidismo en el bebé.</p>

						<h4>Lactancia</h4>
						<p>La levotiroxina es secretada en la leche materna durante la lactancia pero las concentraciones alcanzadas con niveles de dosis terapéuticamente recomendadas no son suficientes para causar el desarrollo de hipertiroidismo o supresión de secreción TSH en el bebé.</p>

						<p><b>En el caso de embarazo, en pacientes bajo terapia con levotiroxina, debe aumentarse la dosis de sustitución en 30 - 50%, debido a los mayores requerimientos de hormona tiroidea en esta etapa de la vida y los niveles a alcanzar dependerán del trimestre (primer trimestre: TSH < 2.5 mIU/L, segundo y tercer trimestre: TSH < 3.0 mIU/L)<sup>2</sup></b></p>
					`;
				}
				if ( this.form.data.condition == 'alteraciones-cardiovasculares' ) {
					this.initialDose = `25`;
					this.refNum.initialDose = '1';
					this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '3';

					this.resultTxt = `
						<h3>ADULTO MAYOR<sup>1</sup></h3>
						<p>Cuando se comienza con la terapia con LT4, la dosis inicial en sujetos jóvenes es equivalente a la dosis completa. Sin embargo, es prudente elegir una dosis más baja en sujetos mayores y en pacientes con alto riesgo cardiovascular, con el fin de evitar eventos agudos. La dosis baja inicial se ajusta gradualmente con intervalos de 4-6 semanas hasta que los niveles de TSH retornen al rango normal. <sup>1</sup></p>
					`;
				}
				// text applies to the 3 conditions above
				// this.resultTxt = "";

				if ( this.form.data.condition == 'cancer-nodules-supression' ) {
					this.initialDose = `25-50`;
					this.refNum.initialDose = '1';
					this.maintenanceDose = `${Math.ceil( 2.2 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '3';
					this.resultTxt = `
						<h3>ADULTO MAYOR<sup>1</sup></h3>
						<p>Cuando se comienza con la terapia con LT4, la dosis inicial en sujetos jóvenes es equivalente a la dosis completa. Sin embargo, es prudente elegir una dosis más baja en sujetos mayores y en pacientes con alto riesgo cardiovascular, con el fin de evitar eventos agudos. La dosis baja inicial se ajusta gradualmente con intervalos de 4-6 semanas hasta que los niveles de TSH retornen al rango normal. <sup>1</sup></p>
					`;
				}
			}
		} // end if calctype == weight

		if ( this.form.data.calcType == 'tsh' ) {
			if ( this.age == 1 || this.age == 2 ) {
				this.tshDose = ` - `;
				this.refNum.tshDose = '';

				this.resultTxt = '<p>La guía GES no es recomendada en menores de 15. Uso de cálculo de dosis según criterio de la ATA por peso.</p>';
				// remove this text temporarily
				// this.resultTxt = '';
			}
			if ( this.age >= 3 && this.age < 6 ) {
				console.log( this.form.data.condition );
				if ( this.form.data.condition == 'healthy' ) {
					if ( this.form.data.tsh < 4.5 ) {
						this.tshDose = `0`;
						this.refNum.tshDose = null;
						this.resultTxt = `
							<p>No existe sugestión para un nivel de TSH inferior a 4.5.</p>
						`;
					}
					if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
						this.tshDose = `25-50`;
						this.refNum.tshDose = '2';
						this.resultTxt = `
							<h3>Control TSH y T4L en 3 meses</h3>
							<ul>
								<li><b>TSH y T4 libre normales</b> - Control anual por 1-3 años</li>
								<li><b>TSH elevada &lt;10mlU/L, T4 libre normal</b> - Control semestral. Considerar terapia en pacientes de riesgo de progresión o morbilidad asociada***</li> 
								<li><b>TSH elevada &lt;10mlU/L, T4 libre baja</b> - Derivara Endocrinólogo</li>
							</ul>
							<p><small><sup>***</sup> Paceintes de riesgo o morbilidad asociada: Anticuerpos Anti TPO (+), Mujer con deseo de embarazo, Depresión, Trastomos cognitivos no demenciantes, Bocio, Infertilidad, LDL elevado.</small></p></p>
						`;
					}
					if ( this.form.data.tsh >= 10 && this.form.data.tsh < 20 ) {
						this.tshDose = `50-100`;
						this.refNum.tshDose = '2';
						this.resultTxt = "<p>Iniciar Levotiroxina según nivel de TSH. Mantener control a nivel primario.</p>"
					}
					if ( this.form.data.tsh >= 20 ) {
						this.tshDose = `${Math.ceil( 1 * this.form.data.kg )}-${Math.ceil( 1.6 * this.form.data.kg )}`;
						this.refNum.tshDose = '2';
						this.resultTxt = "<p>Iniciar Levotiroxina según nivel de TSH. Mantener control a nivel primario.</p>"
					}
				}
				if ( this.form.data.condition == 'preagnant' ) {
					if ( this.form.data.tsh <= 10 ) {
						this.tshDose = `50-70`;
						this.refNum.tshDose = '1';

						this.resultTxt = "<p>Iniciar Levotiroxina 50-75 ug y derivar a nivel terciario (Endocrinólogo)<sup>(2)</sup></p>"
					}
					if ( this.form.data.tsh >= 10 && this.form.data.tsh < 20 ) {
						this.tshDose = `50-100`;
						this.refNum.tshDose = '2';
					}
					if ( this.form.data.tsh >= 20 ) {
						this.tshDose = `${Math.ceil( 1 * this.form.data.kg )}-${Math.ceil( 1.6 * this.form.data.kg )}`;
						this.refNum.tshDose = '2';
					}
					this.resultTxt += `
						<h3>TRATAMIENTO DURANTE EL EMBARAZO Y LACTANCIA<sup>1</sup></h3>

						<p>El tratamiento consecuente con hormonas tiroideas es especialmente importante durante el embarazo y la lactancia y, por lo tanto, debe continuarse. Los requerimientos de dosis pueden incluso aumentarse durante el embarazo. Ya que la elevación de la TSH sérica puede ocurrir tan pronto como las 4 semanas de gestación, las mujeres embarazadas que toman levotiroxina deben tener su THS medida durante cada trimestre, para confirmar que los valores de TSH materno se encuentran dentro del intervalo de referencia del embarazo específico del trimestre. </p>

						<p>Un nivel elevado de TSH en suero debe ser corregido por un aumento en la dosis de levotiroxina. Dado que los niveles de TSH posparto son similares a los valores previos a la concepción, la dosis de levotiroxina debe volver a la dosis antes del embarazo inmediatamente después del parto. Se debe obtener un nivel sérico de TSH de 6 a 8 semanas después del parto.</p>

						<h4>El embarazo</h4>
						<p>Las pruebas de diagnóstico de supresión tiroidea deben evitarse durante el embarazo, ya que la aplicación de sustancias radiactivas en mujeres embarazadas está contraindicada. No existe evidencia de teratogenicidad drogo-inducida y/o feto-toxicidad en humanos con
						niveles de dosis terapéuticamente recomendados. Niveles de dosis excesivamente altos de levotiroxina durante la lactancia pueden tener efectos negativos en el desarrollo fetal y postnatal.
						Sin embargo, la levotiroxina no debe administrarse junto con medicamentos contra una hiperfunción tiroidea (tireostáticos) durante el embarazo, ya que una medicación adicional con levotiroxina puede hacer necesaria una mayor dosificación de tireostáticos que pasan
						a través de la placenta e inducen el hipotiroidismo en el bebé.</p>

						<h4>Lactancia</h4>
						<p>La levotiroxina es secretada en la leche materna durante la lactancia pero las concentraciones alcanzadas con niveles de dosis terapéuticamente recomendadas no son suficientes para causar el desarrollo de hipertiroidismo o supresión de secreción TSH en el bebé.</p>

						<p><b>En el caso de embarazo, en pacientes bajo terapia con levotiroxina, debe aumentarse la dosis de sustitución en 30 - 50%, debido a los mayores requerimientos de hormona tiroidea en esta etapa de la vida y los niveles a alcanzar dependerán del trimestre (primer trimestre: TSH < 2.5 mIU/L, segundo y tercer trimestre: TSH < 3.0 mIU/L)<sup>2</sup></b></p>
					`;
				}
				if ( this.form.data.condition == 'alteraciones-cardiovasculares' ) {
					this.tshDose = `25-50`;
					this.refNum.tshDose = '2';

					this.resultTxt = "Iniciar Levotiroxina 25-50 ug y derivar a nivel terciario (Endocrinólogo)<sup>(2)</sup>"
					this.resultTxt += `
						<h3>ADULTO MAYOR<sup>1</sup></h3>
						<p>Cuando se comienza con la terapia con LT4, la dosis inicial en sujetos jóvenes es equivalente a la dosis completa. Sin embargo, es prudente elegir una dosis más baja en sujetos mayores y en pacientes con alto riesgo cardiovascular, con el fin de evitar eventos agudos. La dosis baja inicial se ajusta gradualmente con intervalos de 4-6 semanas hasta que los niveles de TSH retornen al rango normal. <sup>1</sup></p>
					`;
				}
			}
			if ( this.age >= 6 ) {
				this.tshDose = `25-50`;
				this.refNum.tshDose = '2';

				this.resultTxt = "Iniciar Levotiroxina 25-50 ug y derivar a nivel terciario (Endocrinólogo)<sup>(2)</sup>"
				this.resultTxt += `
					<h3>ADULTO MAYOR<sup>1</sup></h3>
					<p>Cuando se comienza con la terapia con LT4, la dosis inicial en sujetos jóvenes es equivalente a la dosis completa. Sin embargo, es prudente elegir una dosis más baja en sujetos mayores y en pacientes con alto riesgo cardiovascular, con el fin de evitar eventos agudos. La dosis baja inicial se ajusta gradualmente con intervalos de 4-6 semanas hasta que los niveles de TSH retornen al rango normal. <sup>1</sup></p>
				`;
			}
		}

		// Text common to all results
		this.generalResultTxt = `

		`;
	},

	// formulas for Portugal
	pt_PT () {
		/* PT Value
		1 -> 0-6 months
		2 -> 7-11 months
		3 -> 1-5 years
		4 -> 6-10 years
		5 -> 11-18 years
		6-> > >18 years
		*/
		switch ( this.pathology ) {
			case 'hypothyroidism':
				if ( this.age == 1 ) {
					this.initialDose = `10-15`;
					this.maintenanceDose = `${Math.ceil( 10 * this.form.data.kg )}-${Math.ceil( 15 * this.form.data.kg )}`;
				}
				if ( this.age == 2 ) {
					this.initialDose = `10-15`;
					this.maintenanceDose = `${Math.ceil( 6 * this.form.data.kg )}-${Math.ceil( 8 * this.form.data.kg )}`;
				}
				if ( this.age == 3 ) {
					this.initialDose = `10-15`;
					this.maintenanceDose = `${Math.ceil( 5 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
				}
				if ( this.age == 4 ) {
					this.initialDose = `10-15`;
					this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 5 * this.form.data.kg )}`;
				}
				if ( this.age == 5 ) {
					this.initialDose = `10-15`;
					this.maintenanceDose = `${Math.ceil( 1 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
				}

				if ( this.age == 6 ) {
					if ( this.form.data.condition == 'healthy' ) {
						this.initialDose = `25-50`;
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
					}
					if ( this.form.data.condition == 'preagnant' ) {
						this.initialDose = `10-15`;
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}-${Math.ceil( 1.8 * this.form.data.kg * 1.3 )}`;
					}
					if ( this.form.data.condition == 'elder' ) {
						this.initialDose = `12.5-50`;
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
					}
				}

				break;

			// NOT USED AT THE MOMENT
			case 'hemithyroidectomy' :
			case 'thyroidectomy' :
				this.initialDose = ` - `;
				this.maintenanceDose = `75-200`;
				break;

			case 'hyperthyroidism' :
				this.initialDose = ` - `;
				this.maintenanceDose = `50-100`;
				break;

			case 'cancer' :
				this.initialDose = ` - `;
				this.maintenanceDose = `150-300`;
				break;
		}

		// There is no text for results right now
		this.resultTxt = '<p>Os valores apresentados são apenas informativos, uma vez que existe uma grande variedade na resposta interindividual. A decisão terapêutica deverá ser tomada pelo médico com base nos valores de TSH apresentados pelo doente.</p>'
	},

	// formulas for Central ES
	es_CT () {
		// code...
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>Para los recién nacidos y los bebés con hipotiroidismo congénito, en el cual el reemplazo rápido es importante, la dosis inicial recomendada de 10 a 15 microgramos por kg de peso corporal (BW) por día durante los primeros 3 meses. Posteriormente, la dosis debe ajustarse individualmente de acuerdo a los hallazgos clínicos y de la hormona tiroidea y los valores de TSH.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Los niños pueden recibir la dosis diaria total de Eutirox<sup>®</sup>, como mínimo media hora antes de la primera comida del día. Inmediatamente antes de usarla, triture la tableta y mézclela en un poco de agua y adminístresela al niño con más líquido. Siempre prepare la mezcla en fresco.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Los niños pueden recibir la dosis diaria total de Eutirox<sup>®</sup>, como mínimo media hora antes de la primera comida del día. Inmediatamente antes de usarla, triture la tableta y mézclela en un poco de agua y adminístresela al niño con más líquido. Siempre prepare la mezcla en fresco.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Los niños pueden recibir la dosis diaria total de Eutirox<sup>®</sup>, como mínimo media hora antes de la primera comida del día. Inmediatamente antes de usarla, triture la tableta y mézclela en un poco de agua y adminístresela al niño con más líquido. Siempre prepare la mezcla en fresco.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `55-70`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> solamente aplicable al diagnóstico durante el Embarazo</small></p>'
				this.resultTxt += '<p>El tratamiento con hormonas tiroideas se administra consistentemente durante el embarazo y la lactancia materna en particular. Los requerimientos de dosis pueden inclusive aumentarse durante el embarazo. Debido a que pueden ocurrir elevaciones de la TSH en suero durante las primeras 4 semanas de gestación, las mujeres embarazadas que toman levotiroxina, deberían realizarse controles de TSH durante cada trimestre, con el objetivo de confirmar que los valores de TSH en el suero materno se encuentren dentro de los rangos de referencia. Un rango elevado de TSH en suero debe ser corregido incrementando la dosis de levotiroxina. Debido a que los niveles de TSH post-parto son similares a los valores de pre-concepción, la dosis debe retornar a la administrada previo al embarazo, inmediatamente después del parto. Debe realizarse un control de nivel de TSH en suero de 3 a 6 semanas después del parto.</p><p>El objetivo de TSH con Eutirox<sup>®</sup> en el embarazo varía según el trimestre: durante el primer trimestre debe ser menor de 2,5 mU/L y en el segundo y tercer trimestre menor de 3 mU/L.<sup>(6)</sup></p>'
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p>En pacientes de edad avanzada, en pacientes con cardiopatía coronaria y en pacientes con hipotiroidismo severo o existente por largo tiempo, se requiere especial atención cuando se inicia la terapia con hormonas tiroideas: El tratamiento se inicia a una dosis baja, la cual se incrementa lentamente y a intervalos prolongados con monitoreo frecuente de las hormonas tiroideas. Se puede considerar una dosis de mantenimiento más baja que la requerida para la corrección completa de los niveles de TSH.</p><p>El objetivo de TSH con Eutirox<sup>®</sup> varia con la edad, menores de 60 años, de 1-2,5 mU/L, de 60-70 años 3-4 mU/L y mayores de 70 años de 4-6 mU/L.<sup>(5)</sup></p>'
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>El médico determinará la dosis individual con base en los exámenes clínicos y en las pruebas de laboratorio. Como un número de pacientes muestran concentraciones elevadas de T4 y fT4, la concentración sérica basal de la hormona estimuladora de la tiroides proporciona una base más confiable para el tratamiento subsiguiente. Usted generalmente comienza con una dosis baja, la cual se incrementará cada 2-4 semanas, hasta que logre su dosis individual completa. Durante las semanas iniciales del tratamiento usted tendrá citas para pruebas de laboratorio para ajustar la dosis.</p>
		`;
	},

	// second CC for central
	es_GT () {
		this.es_CT();
	},

	// formulas for Central EN
	en_CT () {
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>For neonates and infants with congenital hyporhyroidism, where rapid replacement is important, the initial recommended dosage is 10 to 15 micrograms per kg body weight (NW) per day for the first 3 months. Thereafter, the dose should be adjusted individually according to the clinical findings and thyroid hormone and TSH values.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `50-75`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> Only applicable to diagnosis during pregnancy</small></p>';
				this.resultTxt += '<p>Treatment with thyroid hormones is given consistently during pregnancy and breastfeeding in particular. Dosage requirements may even increase during pregnancy. Since elevations in serum TSH may occur as early as 4 weeks of gestation, pregnant women taking levothyroxine should have their TSH measured during each trimester, in order to confirm that the maternal serum TSH values lie within the trimester specific pregnancy reference range. An elevated serum TSH level should be corrected by an increase in the dose of levothyroxine. Since postpartum TSH levels are similar to preconception values, the levothyroxine dosage should return to the pre-pregnancy dose immediately after delivery. A serum TSH level should be obtained 6-8 weeks postpartum.</p><p>The goal of TSH with Eutirox<sup>®</sup> in pregnancy varies according to the trimester: during the first trimester it should be less than 2.5 mU / L and in the second and third trimester it should be less than 3 mU / L. <sup>(6)</sup></p>';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p>In elderly patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones: Treatment is initiated at a low dose, which is increased slowly and at lengthy intervals with frequent monitoring of thyroid hormones. A maintenance dose lower than required for complete correction of TSH levels may be considered.</p><p>The TSH target with Eutirox<sup>®</sup> varies with age, under 60 years, from 1-2.5 mU / L, from 60-70 years 3-4 mU / L and over 70 years from 4-6 mU / L<sup>(5)</sup></p>';
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>The doctor will determine your individual dose base on clinical examinations and on laboratory tests. As a number of patients show elevated concentrations of T4 and fT4, basal serum concentration of thyroid stimulating hormone provides a more reliable basis for subsequent treatment.</p>
			<p>You generally start with a low dose, which is increased every 2-4 weeks, until your full individual dose is reached. During the initial weeks of treatment, you will have appointments for laboratory tests in order to adjust the dose.</p>
		`;
	},

	// second CC for central
	en_GT () {
		this.en_CT();
	},

	// formulas for colombia
	es_CO () {
		// code...
		if ( this.age == 1 ) {
			this.initialDose = `${Math.ceil( 10 * this.form.data.kg )}-${Math.ceil( 15 * this.form.data.kg )}`;
			this.refNum.initialDose = '3';
			this.maintenanceDose = `${Math.ceil( 100 * this.bsa )}-${Math.ceil( 150 * this.bsa )}`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '';
		}

		if ( this.age == 2 ) {
			this.initialDose = `${Math.ceil( 10 * this.form.data.kg )}`;
			this.refNum.initialDose = '4';
			this.maintenanceDose = `${Math.ceil( 100 * this.bsa )}-${Math.ceil( 150 * this.bsa )}`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '';
		}

		if ( this.age == 3 ) {
			this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
			this.refNum.initialDose = '2';
			this.maintenanceDose = `100-200`;
			this.refNum.maintenanceDose = '2';
			this.resultTxt = '<p>Los niños pueden recibir la dosis diaria total de Eutirox<sup>®</sup>, como mínimo media hora antes de la primera comida del día. Inmediatamente antes de usarla, triture la tableta y mézclela en un poco de agua y adminístresela al niño con más líquido. Siempre prepare la mezcla en fresco.</p>';
		}
		if ( this.age >= 4 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				if ( this.form.data.tsh < 4.5 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Su paciente no tiene hipotiroidismo.</p>'
				}
				if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Realice una nueva TSH en 8 semanas</p>'
				}
				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = ""
				}
			}
			if ( this.form.data.condition == 'preagnant' ) {
				if ( this.form.data.tsh < 10 ) {
					this.initialDose = `${Math.ceil( 1 * this.form.data.kg * 1.3 )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `${Math.ceil( 100 * 1.3 )}-${Math.ceil( 200 * 1.3 )}`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '';
				}

				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `${Math.ceil( 100 * 1.3 )}-${Math.ceil( 200 * 1.3 )}`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '';
				}

				this.resultTxt += '<p>El tratamiento con hormonas tiroideas se administra de manera ininterrumpida durante el embarazo y la lactancia en particular. Quizá sea necesario aumentar la dosis durante el embarazo.</p><p>Dado que el aumento de la TSH en suero puede ocurrir tan pronto como a las 4 semanas de gestación, las mujeres embarazadas que toman levotiroxina deben medir su TSH en cada trimestre, para confirmar que los valores de TSH séricos maternales están dentro del intervalo de referencia de embarazo específico para el trimestre. Un nivel elevado de TSH sérico debe ser corregido con un aumento en la dosis de levotiroxina. Dado que los valores de TSH postparto son similares a los valores previos a la concepción, la dosis de levotiroxina debe volver a la dosis pre embarazo inmediatamente después del parto. El nivel de TSH sérico debe ser obtenido a las 6-8 semanas post parto.</p><h4>Embarazo:</h4><p>Las pruebas de diagnóstico de supresión de tiroides deben evitarse durante el embarazo, puesto que la aplicación de substancias radioactivas en mujeres embarazadas está contraindicada.</p><p>No existe evidencia de teratogénesis inducida por el fármaco y/o toxicidad fetal en humanos a las dosis terapéuticas recomendadas. Dosis excesivamente elevadas de levotiroxina durante el embarazo pueden tener un efecto negativo en el desarrollo fetal y postnatal.</p><h4>Lactancia</h4><p>La levotiroxina se secreta en la leche materna durante la lactancia, pero las concentraciones alcanzadas a las dosis terapéuticas recomendadas no son suficientes para provocar el desarrollo de hipertiroidismo o supresión de secreción de TSH en el infante.</p>'
			}

			if ( this.form.data.condition == 'infertility' ) {
				if ( this.form.data.tsh < 4.5 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Su paciente no tiene hipotiroidismo, evalúe otras causas de infertilidad.</p>'
				}
				if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
					this.initialDose = `${Math.ceil( 1 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>Nota: Dosis sugerida para TSH &gt; 4.5 &amp; &lt; 10</p>';
				}
				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>Nota: Dosis sugerida para TSH &ge; 10</p>';
				}
			}

			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `${Math.ceil( 1 * this.form.data.kg )}`;
				this.refNum.initialDose = '2';
				this.maintenanceDose = `100-200`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p><b>Adulto Mayor y sospecha de alteraciones cardiovasculares</b>: En pacientes ancianos, en pacientes con enfermedad cardiaca coronaria y en pacientes con hipotiroidismo severo o prolongado, se requiere especial precaución al iniciar la terapia con hormonas de la tiroides: El tratamiento se inicia con una dosis baja, la cual se aumenta lentamente y a intervalos prolongados con monitoreo frecuente de las hormonas de la tiroides.</p>'
			}

			if ( this.form.data.condition == 'bocio' ) {
				if ( this.form.data.tsh < 4.5 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Su paciente no tiene hipotiroidismo, evalúe otras causas de bocio.</p>'
				}
				if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
					this.initialDose = `${Math.ceil( 1 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>La experiencia ha demostrado que una dosis menor es suficiente en pacientes con peso bajo y en pacientes con bocio nodular extenso.</p>';
				}
				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>La experiencia ha demostrado que una dosis menor es suficiente en pacientes con peso bajo y en pacientes con bocio nodular extenso.</p>';
				}
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `${Math.ceil( 2.2 * this.form.data.kg )}`;
				this.refNum.initialDose = '2';
				this.maintenanceDose = `150-300`;
				this.refNum.maintenanceDose = '2';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>Con el fin de tratar a cada paciente según sus necesidades individuales, hay tabletas disponibles con un contenido de levotiroxina sódica de 25 - 200 microgramos. Por lo tanto, los pacientes usualmente sólo tienen que tomar una tableta al día. Se recomienda que se determine la dosis diaria individual con base pruebas de laboratorio y exámenes clínicos. También se puede considerar una dosis de mantenimiento menor que la requerida para la corrección completa de los niveles de TSH. La experiencia ha demostrado que una dosis menor es suficiente en pacientes con peso bajo y en pacientes con bocio nodular extenso. </p>

			<h5 class="mt-2">Tabla de diagnóstico de laboratorio de hipotiroidismo primario</h5>

			<table class="table">
				<tr>
					<th>&nbsp;</th>
					<th>TSH</th>
					<th>T4 Libre</th>
				</tr>
				<tr>
					<td>Normal</td>
					<td>0,4 &ndash; 4,5 mUI/L</td>
					<td>0,7 &ndash; 1,8 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotiroidismo primario</td>
					<td>&gt; 4,5 mUI/L</td>
					<td>&lt; 0,7 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotiroidismo Subclínico</td>
					<td>&gt; 4,5 mUI/L</td>
					<td>0,7 &ndash; 1,8 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotiroidismo Central</td>
					<td>TSH &lt; 0,4 mUI/L (o normal)</td>
					<td>&lt; 0,7 ng/dL</td>
				</tr>
			</table>
		`;
	},

	// formulas for Ecuador
	es_EC () {
		// code...
		if ( this.age == 1 ) {
			this.initialDose = `${Math.ceil( 10 * this.form.data.kg )}-${Math.ceil( 15 * this.form.data.kg )}`;
			this.refNum.initialDose = '3';
			this.maintenanceDose = `${Math.ceil( 100 * this.bsa )}-${Math.ceil( 150 * this.bsa )}`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '';
		}

		if ( this.age == 2 ) {
			this.initialDose = `${Math.ceil( 10 * this.form.data.kg )}`;
			this.refNum.initialDose = '4';
			this.maintenanceDose = `${Math.ceil( 100 * this.bsa )}-${Math.ceil( 150 * this.bsa )}`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '';
		}

		if ( this.age == 3 ) {
			this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
			this.refNum.initialDose = '2';
			this.maintenanceDose = `100-200`;
			this.refNum.maintenanceDose = '2';
			this.resultTxt = '<p>Los niños pueden recibir la dosis diaria total de Eutirox<sup>®</sup>, como mínimo media hora antes de la primera comida del día. Inmediatamente antes de usarla, triture la tableta y mézclela en un poco de agua y adminístresela al niño con más líquido. Siempre prepare la mezcla en fresco.</p>';
		}
		if ( this.age >= 4 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				if ( this.form.data.tsh < 4.5 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Su paciente no tiene hipotiroidismo.</p>'
				}
				if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Realice una nueva TSH en 8 semanas</p>'
				}
				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = ""
				}
			}
			if ( this.form.data.condition == 'preagnant' ) {
				if ( this.form.data.tsh < 10 ) {
					this.initialDose = `${Math.ceil( 1 * this.form.data.kg * 1.3 )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `${Math.ceil( 100 * 1.3 )}-${Math.ceil( 200 * 1.3 )}`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '';
				}

				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `${Math.ceil( 100 * 1.3 )}-${Math.ceil( 200 * 1.3 )}`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '';
				}

				this.resultTxt += '<p>El tratamiento con hormonas tiroideas se administra de manera ininterrumpida durante el embarazo y la lactancia en particular. Quizá sea necesario aumentar la dosis durante el embarazo.</p><p>Dado que el aumento de la TSH en suero puede ocurrir tan pronto como a las 4 semanas de gestación, las mujeres embarazadas que toman levotiroxina deben medir su TSH en cada trimestre, para confirmar que los valores de TSH séricos maternales están dentro del intervalo de referencia de embarazo específico para el trimestre. Un nivel elevado de TSH sérico debe ser corregido con un aumento en la dosis de levotiroxina. Dado que los valores de TSH postparto son similares a los valores previos a la concepción, la dosis de levotiroxina debe volver a la dosis pre embarazo inmediatamente después del parto. El nivel de TSH sérico debe ser obtenido a las 6-8 semanas post parto.</p><h4>Embarazo:</h4><p>Las pruebas de diagnóstico de supresión de tiroides deben evitarse durante el embarazo, puesto que la aplicación de substancias radioactivas en mujeres embarazadas está contraindicada.</p><p>No existe evidencia de teratogénesis inducida por el fármaco y/o toxicidad fetal en humanos a las dosis terapéuticas recomendadas. Dosis excesivamente elevadas de levotiroxina durante el embarazo pueden tener un efecto negativo en el desarrollo fetal y postnatal.</p><h4>Lactancia</h4><p>La levotiroxina se secreta en la leche materna durante la lactancia, pero las concentraciones alcanzadas a las dosis terapéuticas recomendadas no son suficientes para provocar el desarrollo de hipertiroidismo o supresión de secreción de TSH en el infante.</p>'
			}

			if ( this.form.data.condition == 'infertility' ) {
				if ( this.form.data.tsh < 4.5 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Su paciente no tiene hipotiroidismo, evalúe otras causas de infertilidad.</p>'
				}
				if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
					this.initialDose = `${Math.ceil( 1 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>Nota: Dosis sugerida para TSH &gt; 4.5 &amp; &lt; 10</p>';
				}
				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>Nota: Dosis sugerida para TSH &ge; 10</p>';
				}
			}

			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `${Math.ceil( 1 * this.form.data.kg )}`;
				this.refNum.initialDose = '2';
				this.maintenanceDose = `100-200`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p><b>Adulto Mayor y sospecha de alteraciones cardiovasculares</b>: En pacientes ancianos, en pacientes con enfermedad cardiaca coronaria y en pacientes con hipotiroidismo severo o prolongado, se requiere especial precaución al iniciar la terapia con hormonas de la tiroides: El tratamiento se inicia con una dosis baja, la cual se aumenta lentamente y a intervalos prolongados con monitoreo frecuente de las hormonas de la tiroides.</p>'
			}

			if ( this.form.data.condition == 'bocio' ) {
				if ( this.form.data.tsh < 4.5 ) {
					this.initialDose = ` - `;
					this.maintenanceDose = ` - `;
					this.resultTxt = '<p style="font-size:1.5em">Su paciente no tiene hipotiroidismo, evalúe otras causas de bocio.</p>'
				}
				if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
					this.initialDose = `${Math.ceil( 1 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>La experiencia ha demostrado que una dosis menor es suficiente en pacientes con peso bajo y en pacientes con bocio nodular extenso.</p>';
				}
				if ( this.form.data.tsh >= 10 ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.maintenanceDose = `100-200`;
					this.refNum.maintenanceDose = '2';
					this.resultTxt = '<p>La experiencia ha demostrado que una dosis menor es suficiente en pacientes con peso bajo y en pacientes con bocio nodular extenso.</p>';
				}
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `${Math.ceil( 2.2 * this.form.data.kg )}`;
				this.refNum.initialDose = '2';
				this.maintenanceDose = `150-300`;
				this.refNum.maintenanceDose = '2';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>Con el fin de tratar a cada paciente según sus necesidades individuales, hay tabletas disponibles con un contenido de levotiroxina sódica de 25 - 200 microgramos. Por lo tanto, los pacientes usualmente sólo tienen que tomar una tableta al día. Se recomienda que se determine la dosis diaria individual con base pruebas de laboratorio y exámenes clínicos. También se puede considerar una dosis de mantenimiento menor que la requerida para la corrección completa de los niveles de TSH. La experiencia ha demostrado que una dosis menor es suficiente en pacientes con peso bajo y en pacientes con bocio nodular extenso. </p>

			<h5 class="mt-2">Tabla de diagnóstico de laboratorio de hipotiroidismo primario</h5>

			<table class="table">
				<tr>
					<th>&nbsp;</th>
					<th>TSH</th>
					<th>T4 Libre</th>
				</tr>
				<tr>
					<td>Normal</td>
					<td>0,4 &ndash; 4,5 mUI/L</td>
					<td>0,7 &ndash; 1,8 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotiroidismo primario</td>
					<td>&gt; 4,5 mUI/L</td>
					<td>&lt; 0,7 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotiroidismo Subclínico</td>
					<td>&gt; 4,5 mUI/L</td>
					<td>0,7 &ndash; 1,8 ng/dL</td>
				</tr>
				<tr>
					<td>Hipotiroidismo Central</td>
					<td>TSH &lt; 0,4 mUI/L (o normal)</td>
					<td>&lt; 0,7 ng/dL</td>
				</tr>
			</table>
		`;
	},

	// formulas for Peru
	es_PE () {
		if ( this.form.data.calcType == 'initial' ) {
			if ( this.age == 1 ) {
				this.initialDose = `12.5-50`;
				this.refNum.initialDose = '1';
				// this.maintenanceDose = `${Math.ceil( 2.5 * this.form.data.kg )}`;

				this.resultTxt = '<p>En los recién nacidos y infantes con hipotiroidismo congénito, requieren de una rápida sustitución, la terapia inicial recomendada es 10 a 15 mcg/kg de peso corporal, durante los 3 primeros meses. A partir de allí, la dosis debe ser ajustada individualmente de acuerdo a los hallazgos clínicos y a los niveles de la hormona tiroidea y de TSH.</p>';
			}

			if ( this.age == 2 ) {
				this.initialDose = `12.5-50`;
				this.refNum.initialDose = '1';
				this.resultTxt = '<p>-</p>';
			}
			if ( this.age >= 3 ) {
				console.log( this.form.data.condition );
				if ( this.form.data.condition == 'healthy' ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
				}
				if ( this.form.data.condition == 'preagnant' ) {
					this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
					this.refNum.initialDose = '2';

					// text applies to the 3 conditions above
					this.resultTxt = `
						<h4>Embarazo y Lactancia</h4>

						<p>El tratamiento con levotiroxina debe ser llevado a cabo durante el embarazo y en particular durante la lactancia. Puede ser incluso necesario incrementar la dosis durante el embarazo. Ya que el aumento de TSH en suero puede ocurrir a las 4 semanas de gestación, debe controlarse la TSH de las mujeres embarazadas que toman levotiroxina en cada trimestre, con el fin de comprobar que los valores de TSH en suero están dentro de los valores de referencia específicos para cada trimestre del embarazo. Debido a que los niveles de TSH durante el postparto son similares a los valores anteriores al embarazo, la dosis de levotiroxina debe volver a ser la misma que la anterior al embarazo inmediatamente después del parto. Se debe obtener los niveles de TSH en el suero a las 6 - 8 semanas del postparto</p>

						<h4>Embarazo</h4>

						<p>La experiencia ha demostrado que no hay evidencia de teratogenicidad y/o fetotoxicidad en humanos en las dosis terapéuticas recomendadas. Dosis excesivamente altas de levotiroxina durante el embarazo pueden tener efectos negativos en el feto y en el desarrollo postnatal. El tratamiento concomitante del hipertiroidismo con levotiroxina y agentes antitiroideos no está indicado durante el embarazo. Esta combinación podría requerir dosis más elevadas de agentes antitiroideos, para los cuales es conocido que atraviesan la placenta y provocan hipotiroidismo en el niño. El producto no debe ser usado para las pruebas de supresión tiroidea durante el embarazo, ya que el uso de sustancias radioactivas en las mujeres embarazadas está contraindicada.</p>

						<h4>Lactancia</h4>

						<p>La levotiroxina se excreta en la leche materna durante la lactancia, si se administra el nivel de dosis terapéutica recomendado, la concentración resultante es, sin embargo, no son lo suficiente altas como para causar hipertiriodismo o la secreción de TSH en el bebé.</p>
					`;
				}
				if ( this.form.data.condition == 'elder' ) {
					this.initialDose = `12.5-25`;
					this.refNum.initialDose = '1';

					this.resultTxt = `
						<h4>Pacientes mayores y con riesgo cardiovascular</h4>

						<p>En pacientes de edad avanzada, pacientes con enfermedad coronaria y pacientes con hipotiroidismo severo o establecido desde hace tiempo, se requiere especial precaución al inicio de la terapia con hormonas tiroideas. Al iniciar el tratamiento se debe administrar una dosis inicial baja (p.ej. 12,5 mcg/día) que se incrementará de forma lenta y a intervalos prolongados (p.ej. aumentos graduales de 12,5 mcg/día cada dos semanas), con monitorización frecuente de las hormonas tiroideas. Una dosis inferior a la dosis óptima necesaria para conseguir una terapia de sustitución completa, que conlleve a ser insuficiente para la regularización completa de los niveles de TSH, debe ser por tanto considerada.</p>
					`;
				}

				if ( this.form.data.condition == 'cancer-nodules-supression' ) {
					this.initialDose = `${Math.round( 2.2 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
				}

				if ( this.form.data.condition == 'bocio' ) {
					this.initialDose = `${Math.round( 1.6 * this.form.data.kg )}`;
					this.refNum.initialDose = '2';
					this.resultTxt = `<p>La experiencia demuestra que una dosis más baja es suficiente para pacientes de bajo peso y para pacientes con bocio nodular extenso.</p>`;
				}
			}
		}

		if ( this.form.data.calcType == 'followup' ) {
			if ( this.form.data.condition == 'healthy' || this.form.data.condition == 'elder' || this.form.data.condition == 'cancer-nodules-supression' ) {
				// this.resultTxt = `<p>Manutenção: recomenda-se 75 a 125 mcg diários sendo que alguns pacientes, com má absorção, podem necessitar de até 200 mcg/dia. A maioria dos pacientes não exige doses superiores a 150 mcg/dia. A falta de resposta ás doses de 200 mcg/dia sugere má absorção, não obediência ao tratamento ou erro diagnóstico.</p>`;

				if ( this.form.data.tsh < 0.4 ) {
					this.followupDose = 'Disminuir dosis en 25 mcg/dia';
				}
				if ( this.form.data.tsh >= 0.4 && this.form.data.tsh < 4.5 ) {
					this.followupDose = 'Mantener el tratamiento';
				}
				if ( this.form.data.tsh >= 4.5 ) {
					this.followupDose = 'Incrementar dosis en 12.5 a 25 mcg/dia';
				}

				this.refNum.followupDose = '3';
			}
			if ( this.form.data.condition == 'preagnant-1' ) {
				this.resultTxt = `<p>1er trimestre: 0,1 – 2,5 mUI/mL. Según la Asociación America de Tiroides (ATA), en el primer trimestre el valor superior de normalidade debe ser reducido en 0,5 mUI/L, lo que corresponde a 4,0 mUI/L cuando se utiliza este método. </p>`;

				if ( this.form.data.tsh < 0.1 ) {
					this.followupDose = 'Disminuir la dosis'
				}
				if ( this.form.data.tsh >= 0.1 && this.form.data.tsh <= 2.5 ) {
					this.followupDose = 'Mantener el tratamiento'
				}
				if ( this.form.data.tsh > 2.5 ) {
					this.followupDose = 'Incrementar la dosis'
				}

				this.refNum.followupDose = '4';
			}
			if ( this.form.data.condition == 'preagnant-2' ) {
				this.resultTxt = `<p>2do trimestre: 0,2 – 3,5 mUI/mL</p>`;

				if ( this.form.data.tsh < 0.2 ) {
					this.followupDose = 'Disminuir la dosis'
				}
				if ( this.form.data.tsh >= 0.2 && this.form.data.tsh <= 3.5 ) {
					this.followupDose = 'Mantener el tratamiento'
				}
				if ( this.form.data.tsh > 3.5 ) {
					this.followupDose = 'Incrementar la dosis'
				}

				this.refNum.followupDose = '4';
			}
			if ( this.form.data.condition == 'preagnant-3' ) {
				this.resultTxt = `<p>3er trimestre: 0,3 – 3,5 mUI/mL</p>`;

				if ( this.form.data.tsh < 0.3 ) {
					this.followupDose = 'Disminuir la dosis'
				}
				if ( this.form.data.tsh >= 0.3 && this.form.data.tsh <= 3.5 ) {
					this.followupDose = 'Mantener el tratamiento'
				}
				if ( this.form.data.tsh > 3.5 ) {
					this.followupDose = 'Incrementar la dosis'
				}

				this.refNum.followupDose = '4';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>Para tratar a todos los pacientes según sus necesidades individuales, se dispone de levotiroxina sódica en un rango de entre 25 a 200 microgramos. Por ello los pacientes necesitarán, en general, tomar una sola tableta al día. La pauta posológica que se detalla es únicamente orientativa.</p>

			<p>La dosis diaria individual se determinará en base a los análisis clínicos y a las pruebas de laboratorio. Dado que algunos pacientes muestran concentraciones elevadas de T4 y de T4 libre, las concentraciones séricas basales de hormona estimulante del tiroides (TSH) proporcionan un parámetro de mayor fiabilidad para la monitorización del tratamiento. La terapia con hormonas tiroideas se debe iniciar a dosis bajas y se irá incrementando progresivamente cada 2 a 4 semanas hasta conseguir llegar a la dosis de mantenimiento completa.</p>
		`;
	},

	// formulas for Mexico
	es_MX () {
		/* MX Value
		1 -> 0-3 months
		2 -> 3-6 months
		3 -> 6-12 months
		4 -> 1-5 years
		5 -> 6-12 years
		6 -> 13-64 years
		7 -> >= 65 years
		*/
		switch ( this.pathology ) {
			case 'hypothyroidism':

				// text for all ages below 12 years
				this.resultTxtBelow12years = `
					<p>El tratamiento para hipotiroidismo congénito en niños recién nacidos es con levotiroxina oral en ayuno, por las mañanas a dosis inicial de 10 a 15 mcg/Kg/ día.<br/>
					La dosis de levotiroxina debe modificarse a medida que el niño crece y de acuerdo a las siguientes recomendaciones:</p>
					<table class="table">
						<tr>
							<th>EDAD</th>
							<th>mcg/kg/día</th>
							<th>Dosis máxima diaria en mcg</th>
						</tr>
						<tr>
							<td>0 a 3 meses</td>
							<td>10-15</td>
							<td>50</td>
						</tr>
						<tr>
							<td>3 a 6 meses</td>
							<td>7-10</td>
							<td>25-50</td>
						</tr>
						<tr>
							<td>6 a 12 meses</td>
							<td>6-8</td>
							<td>50-75</td>
						</tr>
						<tr>
							<td>1 a 5 años</td>
							<td>4-6</td>
							<td>75-100</td>
						</tr>
						<tr>
							<td>6 a 12 años</td>
							<td>3-5</td>
							<td>100-125</td>
						</tr>
						<tr>
							<td>&gt; 12 años</td>
							<td>3-4</td>
							<td>100-200</td>
						</tr>
					</table>
				`;

				// Result text for Bocio
				this.resultTxtBocio = `
					<p>En el tratamiento de núdulos benignos o bocio multinodular no tóxico, el nivel de supresión de la TSH que se busca es mayor (0.5 o 1.0 mU/L)</p>`
				;

				// Result text for Elder
				this.resultTxtElder = `
					<p>En pacientes de edad avanzada, en pacientes con cardiopatía coronaria y en pacientes con hipotiroidismo severo de larga evolución, se requieren cuidados especiales cuando se inicia el tratamiento con hormonas tiroideas: el tratamiento comienza con una dosis baja, que se va incrementando lentamente y a intervalos prolongados, vigilando a menudo el nivel de las hormonas tiroideas. Una dosis de mantenimiento menor a la requerida habitualmente para una corrección completa de los niveles de TSH puede ser útil en estos pacientes.<br/>
					En pacientes geriátricos la dosis inicial es de 25 mcg una vez al día, incrementándola cada 6 a 8 semanas hasta la obtención de la respuesta deseada (la dosis de mantenimiento es aproximadamente 1 mcg/kg/día).</p>
				`;

				// Result text for CNS
				this.resultTxtCns = `
					<p>La dosis de Eutirox<sup>®</sup> debe individualizarse dependiendo de la enfermedad de base y el tratamiento del paciente. Generalmente la TSH se suprime (concentraciones <0.1mU/L) con dosis de levotiroxina mayores a 2mcg/Kg/día. Sin embargo en pacientes con riesgo de tumores, el nivel de supresión de la TSH debe ser <0.01 mU/L. En terapia de supresión para cáncer de tiroides, las dosis totales diarias van de 150 a 300 mcg/día.</p>
				`;

				// Result text for preagnancy
				this.resultTxtPreagnancy = `
					<p>El tratamiento con hormonas tiroideas se debe administrar de manera ininterrumpida durante el embarazo y la lactancia en particular. Es posible que sea necesario aumentar la dosis durante el embarazo.<br/>
					Dado que el aumento de la TSH en suero puede ocurrir tan pronto como a las 4 semanas de gestación, las mujeres embarazadas que toman levotiroxina deben medir su TSH en cada trimestre, para confirmar que los valores de TSH séricos maternales están dentro del intervalo de referencia de embarazo específico para el trimestre. Un nivel elevado de TSH sérico debe ser corregido con un aumento en la dosis de levotiroxina. Dado que los valores de TSH postparto son similares a los valores previos a la concepción, la dosis de levotiroxina debe volver a la dosis pre embarazo inmediatamente después del parto. El nivel de TSH sérico debe ser obtenido a las 6-8 semanas post parto.
					</p>
					<p><strong>Embarazo:</strong><br/>
					Deben evitarse las pruebas de diagnóstico de supresión de tiroidea durante el embarazo, puesto que la aplicación de substancias radioactivas en mujeres embarazadas está contraindicada.<br/>
					Los niveles de dosis excesivamente altos de levotiroxina durante el embarazo pueden tener un efecto negativo en el desarrollo fetal y posnatal.<br/>
					La terapia de combinación de hipertiroidismo con levotiroxina y agente antitiroideos no está indicada durante el embarazo. Dicha combinación podría requerir dosis mas altas de agentes antitiroideos, los cuales se sabe que pasan a la placenta e inducen hipotiroidismo en el infante.
					</p>
					<p><strong>Lactancia</strong><br/>
					La levotiroxina se secreta en la leche materna durante la lactancia, pero las concentraciones alcanzadas a las dosis terapéuticas recomendadas no son suficientes para provocar el desarrollo de hipertiroidismo o supresión de secreción de TSH en el lactante.</p>
				`;

				if ( this.form.data.calcType == 'initial' ) {
					if ( this.age <= 5 ) {
						if ( this.age == 1 ) {
							this.initialDose = `${Math.ceil( this.form.data.kg * 10 )} - ${Math.ceil( this.form.data.kg * 15 )}`;
							this.refNum.initialDose = '4&#8209;7';
							this.maintenanceDose = ` - `;
						}

						if ( this.age == 2 ) {
							this.initialDose = `25-50 o ${Math.ceil( this.form.data.kg * 8 )}-${Math.ceil( this.form.data.kg * 10 )}`;
							this.refNum.initialDose = '4&#8209;7';
							this.maintenanceDose = ` - `;
						}

						if ( this.age == 3 ) {
							this.initialDose = `50-75 o ${Math.ceil( this.form.data.kg * 6 )}-${Math.ceil( this.form.data.kg * 8 )}`;
							this.refNum.initialDose = '1';
							this.maintenanceDose = ` - `;
						}

						if ( this.age == 4 ) {
							this.initialDose = `75-100 o ${Math.ceil( this.form.data.kg * 5 )}-${Math.ceil( this.form.data.kg * 6 )}`;
							this.refNum.initialDose = '4&#8209;7';
							this.maintenanceDose = ` - `;
						}

						if ( this.age == 5 ) {
							this.initialDose = `100-155 o ${Math.ceil( this.form.data.kg * 4 )}-${Math.ceil( this.form.data.kg * 5 )}`;
							this.refNum.initialDose = '4&#8209;7';
							this.maintenanceDose = ` - `;
						}

						// text for all ages below 12 years
						this.resultTxt = this.resultTxtBelow12years;
					}

					if ( this.age == 6 ) {
						if ( this.form.data.condition == 'healthy' ) {
							if ( this.form.data.tsh >= 10 ) {
								this.initialDose = `${Math.ceil( this.form.data.kg * 1.6 )}`;
								this.refNum.initialDose = '1&#8209;3,&nbsp;10&#8209;11';
								this.resultTxt = ``;
							} else if ( this.form.data.tsh > 4.5 && this.form.data.tsh < 10 ) {
								this.initialDose = ` - `;
								this.refNum.initialDose = '';
								this.resultTxt = `
									<p><strong>Realice una nueva TSH en 8 semanas</strong><sup>(1-3,&nbsp;10-11)</sup></p>
								`;
							} else if ( this.form.data.tsh < 4.5 ) {
								this.initialDose = ` - `;
								this.refNum.initialDose = '';
								this.resultTxt = `
									<p><strong>Su paciente no tiene hipotiroidismo</strong><sup>(1-3,&nbsp;10-11)</sup></p>
								`;
							}
						}

						if ( this.form.data.condition == 'infertility' ) {
							if ( this.form.data.tsh >= 10 ) {
								this.initialDose = `${Math.ceil( this.form.data.kg * 1.6 )}`;
								this.refNum.initialDose = '1&#8209;3,&nbsp;10&#8209;11';
								this.resultTxt = ``;
							} else if ( this.form.data.tsh > 4.5 && this.form.data.tsh < 10 ) {
								this.initialDose = `${Math.ceil( this.form.data.kg )}`;
								this.refNum.initialDose = '1&#8209;3,&nbsp;10&#8209;11';
								this.resultTxt = ``;
							} else if ( this.form.data.tsh < 4.5 ) {
								this.initialDose = ` - `;
								this.refNum.initialDose = '';
								this.resultTxt = `
									<p><strong>Su paciente no tiene hipotiroidismo, evalúe otras causas de infertilidad</strong><sup>(1-3,&nbsp;10-11)</sup></p>
								`;
							}
						}

						if ( this.form.data.condition == 'bocio' ) {
							this.initialDose = ' - ';
							this.refNum.initialDose = '';
							this.resultTxt = `
								<p><strong>Remitir al especialista</strong></p>
								${this.resultTxtBocio}
							`;
						}

						if ( this.form.data.condition == 'elder' ) {
							this.initialDose = `${Math.ceil( this.form.data.kg )}`;
							this.refNum.initialDose = '1&#8209;3,&nbsp;10&#8209;11';
							this.resultTxt = this.resultTxtElder;
						}

						if ( this.form.data.condition == 'cancer-nodules-supression' ) {
							this.initialDose = ' - ';
							this.refNum.initialDose = '';
							this.resultTxt = `
								<p><strong>Remitir al especialista</strong></p>
								${this.resultTxtCns}
							`;
						}

						if ( this.form.data.condition == 'preagnant' ) {
							if ( this.form.data.tsh >= 10 ) {
								this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
								this.refNum.initialDose = '1&#8209;3,&nbsp;10&#8209;11';
							} else if ( this.form.data.tsh >= 4.5 && this.form.data.tsh < 10 ) {
								this.initialDose = `${Math.ceil( this.form.data.kg * 1.3 )}`;
								this.refNum.initialDose = '1&#8209;3,&nbsp;10&#8209;11';
							}
							this.resultTxt = this.resultTxtPreagnancy;
						}
					}

					if ( this.age == 7 ) {
						this.initialDose = `12.5-25`;
						this.refNum.initialDose = '8';
						this.resultTxt = ``;
					}
				}

				if ( this.form.data.calcType == 'followup' ) {
					if ( this.form.data.condition.includes( 'preagnant' ) ) {
						if ( this.form.data.condition == 'preagnant-1' ) {
							if ( this.form.data.tsh >= 2.5 ) {
								this.followupDose = `Incrementar la dosis`;
								this.refNum.followupDose = '9';
							} else if ( this.form.data.tsh > 0.1 && this.form.data.tsh < 2.5 ) {
								this.followupDose = `Mantener el tratamento`;
								this.refNum.followupDose = '9';
							} else if ( this.form.data.tsh <= 0.1 ) {
								this.followupDose = `Disminuir la dosis`;
								this.refNum.followupDose = '9';
							}
							// Text for all cases in preagnancy - 1st quarter
							this.resultTxt = `
								<p>1er trimestre: 0,1 – 2,5 mUI/mL.<br/>
								Según la Asociación America de Tiroides (ATA), En el primer trimestre el valor superior de normalidade debe ser reducido en 0,5 mUI/L, lo que corresponde a 4,0 mUI/L cuando se utiliza este método.</p>
							`;
						}
						if ( this.form.data.condition == 'preagnant-2' ) {
							if ( this.form.data.tsh >= 3.5 ) {
								this.followupDose = `Incrementar la dosis`;
								this.refNum.followupDose = '9';
							} else if ( this.form.data.tsh > 0.2 && this.form.data.tsh < 3.5 ) {
								this.followupDose = `Mantener el tratamento`;
								this.refNum.followupDose = '9';
							} else if ( this.form.data.tsh <= 0.2 ) {
								this.followupDose = `Disminuir la dosis`;
								this.refNum.followupDose = '9';
							}
							// Text for all cases in preagnancy - 2nd quarter
							this.resultTxt = `
								<p>2do trimestre: 0,2 – 3,5 mUI/mL</p>
							`;
						}
						if ( this.form.data.condition == 'preagnant-3' ) {
							if ( this.form.data.tsh >= 3.5 ) {
								this.followupDose = `Incrementar la dosis`;
								this.refNum.followupDose = '9';
							} else if ( this.form.data.tsh > 0.3 && this.form.data.tsh < 3.5 ) {
								this.followupDose = `Mantener el tratamento`;
								this.refNum.followupDose = '9';
							} else if ( this.form.data.tsh <= 0.3 ) {
								this.followupDose = `Disminuir la dosis`;
								this.refNum.followupDose = '9';
							}
							// Text for all cases in preagnancy - 3rd quarter
							this.resultTxt = `
								<p>3er trimestre: 0,3 – 3,5 mUI/mL</p>
							`;
						}

						this.resultTxt += this.resultTxtPreagnancy;
					} else {
						if ( this.form.data.tsh >= 4.5 ) {
							this.followupDose = `Incrementar dosis en 12.5 a 25mcg/dia`;
							this.refNum.followupDose = '8';
							this.resultTxt = ``;
						} else if ( this.form.data.tsh > 0.4 && this.form.data.tsh < 4.5 ) {
							this.followupDose = `Mantener el tratamento`;
							this.refNum.followupDose = '8';
							this.resultTxt = ``;
						} else if ( this.form.data.tsh < 0.4 ) {
							this.followupDose = `Disminuir dosis en 25mcg/dia`;
							this.refNum.followupDose = '8';
							this.resultTxt = ``;
						}

						if ( this.age <= 5 ) {
							this.resultTxt += this.resultTxtBelow12years;
						}

						if ( this.form.data.condition == 'bocio' ) {
							this.resultTxt += this.resultTxtBocio;
						}

						if ( this.form.data.condition == 'elder' ) {
							this.resultTxt += this.resultTxtElder;
						}

						if ( this.form.data.condition == 'cancer-nodules-supression' ) {
							this.resultTxt += this.resultTxtCns;
						}
					}
				}


				break;

			// NOT USED AT THE MOMENT
			case 'hemithyroidectomy' :
			case 'thyroidectomy' :
				this.initialDose = ` - `;
				this.maintenanceDose = `75-200`;
				break;

			case 'hyperthyroidism' :
				this.initialDose = ` - `;
				this.maintenanceDose = `50-100`;
				break;

			case 'cancer' :
				this.initialDose = ` - `;
				this.maintenanceDose = `150-300`;
				break;
		}

		// Text for all results
		this.resultTxt += `
			<p>La dosis se ajustará de acuerdo a los requerimientos y respuesta de cada paciente. Alcanzar una respuesta terapéutica adecuada depende de varios factores como la edad, peso, estado cardiovascular, otras condiciones médicas incluyendo embarazo, medicación concomitante y la patología tiroidea de base.<br/>
			Las recomendaciones de dosificación solo son una guía. Se recomienda la determinación de la dosis individual en base a estudios de laboratorio y exámenes clínicos. Concentraciones en suero basales de hormona estimulante del tiroides provee una base más real para el tratamiento subsiguiente.</p>
		`;
	},

	// formulas for Austria
	de_AT () {
		// this.default();
		/* AT Value
		1 -> 0-3 months
		2 -> 4-11 months
		3 -> 1-3 years
		4 -> 4-10 years
		5 -> 11-16 years
		6 -> >16 years
		*/
		switch ( this.pathology ) {
			case 'hypothyroidism':
				if ( this.age == 1 ) {
					this.initialDose = `12,5-50`;
					this.maintenanceDose = `${Math.ceil( 10 * this.form.data.kg )}-${Math.ceil( 15 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '1';
				}
				if ( this.age == 2 ) {
					this.initialDose = `12,5-50`;
					this.maintenanceDose = `${Math.ceil( 6 * this.form.data.kg )}-${Math.ceil( 10 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '1';
				}
				if ( this.age == 3 ) {
					this.initialDose = `12,5-50`;
					this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '1';
				}
				if ( this.age == 4 ) {
					this.initialDose = `12,5-50`;
					this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 5 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '1';
				}
				if ( this.age == 5 ) {
					this.initialDose = `12,5-50`;
					this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
					this.refNum.maintenanceDose = '1';
				}

				// text applies to the 5 conditions above
				this.resultTxt = `
					<h3>Kinder und Jugendliche</h3>
					<p>Für Neugeborene und Säuglinge mit angeborener Hypothyreose, wo eine rasche Substitution angezeigt ist, ist die empfohlene Anfangsdosis 10-15mcg/kg/KG/Tag in den ersten 3 Monaten.<br/>Dannach sollte die Dosis individuell je nach klinischen Ergebnissen, SD-Hormonwerten und TSH-Werten angepasst werden.</p>
				`;

				if ( this.age == 6 ) {
					if ( this.form.data.condition == 'healthy' ) {
						this.initialDose = `25-50`;
						this.maintenanceDose = `100-200`;

						this.resultTxt = `
							<h3>Dosierungsangaben gelten als Richtlinien</h3>
							<p>Die individuelle Tagesdosis sollte durch labordiagnostische und klinische Untersuchungen ermittelt werden.<br/>Die Bestimmung der basalen TSH-Serumkonzentration ist eine zuverlässige Basis für das weitere therapeutische Vorgehen.<br/>Die Erhaltungsdosen sollen individuell je nach klinischen Ergebnissen, Schilddrüsenhormonwerten und TSH-Werten angepasst werden.<br/>Eine Therapie mit Schilddrüsenhormonen sollte mit niedriger Dosierung begonnen und kontinuierlich alle 2 bis 4 Wochen bis zur vollen Erhaltungsdosis gesteigert werden.</p>
						`;
					}
					if ( this.form.data.condition == 'preagnant' ) {
						this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
						this.refNum.initialDose = '2';
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
						this.refNum.maintenanceDose = '2';

						this.resultTxt = `
							<h3>Fertilität, Schwangerschaft und Stillzeit</h3>
							<p>Eine Behandlung mit T4 ist besonders während der Schwangerschaft und Stillzeit konsequent durchzuführen.<br/>Eine Erhöhung der Dosis kann während der Schwangerschaft erforderlich werden.<br/>Schwangere Frauen, die T4 einnehmen sollten ihre TSH-Werte jedes Trimester bestimmen lassen zur Bestätigung, dass die mütterlichen Serum-TSH-Werte innerhalb des trimester-spezifischen Schwangerschaftsreferenzbereichs liegen.<br/>Ein erhöhter Serum-TSH-Spiegel sollte durch eine Erhöhung der T4-Dosis korrigiert werden</p>
						`;
					}
					if ( this.form.data.condition == 'elder' ) {
						this.initialDose = `12,5-50`;
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
						this.refNum.maintenanceDose = '2';

						this.resultTxt = `
							<h3>Ältere Patienten</h3>
							<p>Bei älteren Patienten, bei Patienten mit KHK und bei Patienten mit schwerer oder lang bestehender SD-Unterfunktion ist eine Behandlung mit SD-Hormonen besondes vorsichtig zu beginnen.<br/>Es sollte zunächst eine niedrige Initialdosis - z.B. 12,5 mcg/Tag gegeben werden, die dann langsam in längeren Intervallen, stufenweise um 12,5 mcg/Tag alle 14 Tage<br/>unter häufiger Kontrolle der Schilddrüsenhormonwerte gesteigert werden sollte.</p>
						`;
					}
				}
				break;
		}
	},

	// formulas for Poland
	pl_PL () {
		/* PL Value
		1 -> 0-6 months
		2 -> 7-11 months
		3 -> 1-5 years
		4 -> 6-10 years
		5 -> 11-18 years
		6 -> 18-64 years
		7 -> >64 years
		*/
		switch ( this.pathology ) {
			case 'hypothyroidism':
				if ( this.age == 1 ) {
					// this.initialDose = `10-15`;
					const { min, max } = ( this.form.data.tsh < 10 ) ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
					this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
					this.maintenanceDose = `${Math.ceil( 10 * this.form.data.kg )}-${Math.ceil( 15 * this.form.data.kg )}`;
				}
				if ( this.age == 2 ) {
					// this.initialDose = `10-15`;
					const { min, max } = ( this.form.data.tsh < 10 ) ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
					this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
					this.maintenanceDose = `${Math.ceil( 6 * this.form.data.kg )}-${Math.ceil( 8 * this.form.data.kg )}`;
				}
				if ( this.age == 3 ) {
					// this.initialDose = `10-15`;
					const { min, max } = ( this.form.data.tsh < 10 ) ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
					this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
					this.maintenanceDose = `${Math.ceil( 5 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
				}
				if ( this.age == 4 ) {
					// this.initialDose = `10-15`;
					const { min, max } = ( this.form.data.tsh < 10 ) ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
					this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
					this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 5 * this.form.data.kg )}`;
				}
				if ( this.age == 5 ) {
					// this.initialDose = `10-15`;
					const { min, max } = ( this.form.data.tsh < 10 ) ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
					this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
					this.maintenanceDose = `${Math.ceil( 1 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
				}

				this.resultTxt = `
					<p>Lekarz musi skorygować dawkę zależnie od indywidualnej odpowiedzi pacjenta</p>
				`;

				if ( this.age == 6 ) {
					if ( this.form.data.condition == 'healthy' ) {
						// this.initialDose = `25-50`;
						/* const { min, max } = ( this.form.data.tsh < 10 ) ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
						this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`; */

						// this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
						const ED = 1.8;

						if ( this.form.data.tsh <= 4.2 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
						} else if ( this.form.data.tsh > 4.2 && this.form.data.tsh <= 10 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.72 * this.form.data.kg ) - ( ED * 0.6 ) )}`;
						} else if ( this.form.data.tsh > 10 && this.form.data.tsh <= 20 ) {
							this.maintenanceDose = `${Math.ceil( ( 1.08 * this.form.data.kg ) - ( ED * 0.4 ) )}`;
						} else if ( this.form.data.tsh > 20 && this.form.data.tsh <= 40 ) {
							this.maintenanceDose = `${Math.ceil( ( 1.26 * this.form.data.kg ) - ( ED * 0.3 ) )}`;
						} else if ( this.form.data.tsh > 40 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
						}

						// the target dose is also the starting dose (jun23 email)
						this.initialDose = this.maintenanceDose;

						this.resultTxt = ``;
					}
					if ( this.form.data.condition == 'preagnant' ) {
						// this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}-${Math.ceil( 1.8 * this.form.data.kg * 1.3 )}`;
						/* const { min, max } = this.form.data.tsh < 10 ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
						this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}-${Math.ceil( 1.8 * this.form.data.kg * 1.3 )}`; */

						// this.initialDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}-${Math.ceil( 1.8 * this.form.data.kg * 1.3 )}`;
						const ED = 1.8;

						if ( this.form.data.tsh <= 4.2 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}-${Math.ceil( 1.8 * this.form.data.kg * 1.2 )}`;
						} else if ( this.form.data.tsh > 4.2 && this.form.data.tsh <= 10 ) {
							this.maintenanceDose = `${Math.ceil( ( 1.2 * this.form.data.kg * 1.3 ) - ( ED * 0.56 ) )}`;
						} else if ( this.form.data.tsh > 10 && this.form.data.tsh <= 20 ) {
							this.maintenanceDose = `${Math.ceil( ( 1.56 * this.form.data.kg * 1.3 ) - ( ED * 0.3 ) )}`;
						} else if ( this.form.data.tsh > 20 && this.form.data.tsh <= 40 ) {
							this.maintenanceDose = `${Math.ceil( ( 1.74 * this.form.data.kg * 1.3 ) - ( ED * 0.2 ) )}`;
						} else if ( this.form.data.tsh > 40 ) {
							this.maintenanceDose = `${Math.ceil( 2.34 * this.form.data.kg * 1.3 )}-${Math.ceil( 1.8 * this.form.data.kg * 1.3 )}`;
						}
						// the target dose is also the starting dose (jun23 email)
						this.initialDose = this.maintenanceDose;

						this.resultTxt = `
							<p>Zalecenie leczenia w konsultacji ze specjalistą z dziedziny endokrynologii</p>
						`;
					}
					if ( this.form.data.condition == 'elder' ) {
						// this.initialDose = `12.5-50`;
						/* const { min, max } = ( this.form.data.tsh < 10 ) ? { min: 1, max: 1.2 } : { min: 1.4, max: 1.6 }
						this.initialDose = `${Math.ceil( min * this.form.data.kg )}-${Math.ceil( max * this.form.data.kg )}`;
						this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`; */

						this.initialDose = `25-50`;
						const ED = 1.8;

						if ( this.form.data.tsh <= 4.2 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
						} else if ( this.form.data.tsh > 4.2 && this.form.data.tsh <= 10 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.54 * this.form.data.kg ) - ( ED * 0.7 ) )}`;
						} else if ( this.form.data.tsh > 10 && this.form.data.tsh <= 20 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.9 * this.form.data.kg ) - ( ED * 0.5 ) )}`;
						} else if ( this.form.data.tsh > 20 && this.form.data.tsh <= 40 ) {
							this.maintenanceDose = `${Math.ceil( ( 1.26 * this.form.data.kg ) - ( ED * 0.3 ) )}`;
						} else if ( this.form.data.tsh > 40 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
						}

						this.resultTxt = ``;
					}
				}
				if ( this.age == 7 ) {
					if ( this.form.data.condition == 'healthy' ) {
						this.initialDose = `25-50`;
						const ED = 1.7;

						if ( this.form.data.tsh <= 4.2 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
						} else if ( this.form.data.tsh > 4.2 && this.form.data.tsh <= 10 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.51 * this.form.data.kg ) - ( ED * 0.7 ) )}`;
						} else if ( this.form.data.tsh > 10 && this.form.data.tsh <= 20 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.85 * this.form.data.kg ) - ( ED * 0.5 ) )}`;
						} else if ( this.form.data.tsh > 20 && this.form.data.tsh <= 40 ) {
							this.maintenanceDose = `${Math.ceil( ( 1.19 * this.form.data.kg ) - ( ED * 0.3 ) )}`;
						} else if ( this.form.data.tsh > 40 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}-${Math.ceil( 1.8 * this.form.data.kg )}`;
						}
					}
					if ( this.form.data.condition == 'elder' ) {
						this.initialDose = `12.5-25`;
						const ED = 1.6;

						if ( this.form.data.tsh <= 4.2 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
						} else if ( this.form.data.tsh > 4.2 && this.form.data.tsh <= 10 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.48 * this.form.data.kg ) - ( ED * 0.7 ) )}`;
						} else if ( this.form.data.tsh > 10 && this.form.data.tsh <= 20 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.8 * this.form.data.kg ) - ( ED * 0.5 ) )}`;
						} else if ( this.form.data.tsh > 20 && this.form.data.tsh <= 40 ) {
							this.maintenanceDose = `${Math.ceil( ( 0.96 * this.form.data.kg ) - ( ED * 0.4 ) )}`;
						} else if ( this.form.data.tsh > 40 ) {
							this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
						}
					}
				}

				break;

			// NOT USED AT THE MOMENT
			case 'hemithyroidectomy' :
			case 'thyroidectomy' :
				this.initialDose = ` - `;
				this.maintenanceDose = `75-200`;
				break;

			case 'hyperthyroidism' :
				this.initialDose = ` - `;
				this.maintenanceDose = `50-100`;
				break;

			case 'cancer' :
				this.initialDose = ` - `;
				this.maintenanceDose = `150-300`;
				break;
		}

		// There is no text for results right now
		this.generalResultTxt = ``;
	},

	// formulas for Saudi Arabia EN
	en_SA () {
		this.form.data.calcType = null;
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>For neonates and infants with congenital hyporhyroidism, where rapid replacement is important, the initial recommended dosage is 10 to 15 micrograms per kg body weight (NW) per day for the first 3 months. Thereafter, the dose should be adjusted individually according to the clinical findings and thyroid hormone and TSH values.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Levothyroxin at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Levothyroxin at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Levothyroxin at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `50-75`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> Only applicable to diagnosis during pregnancy</small></p>';
				this.resultTxt += '<p>Treatment with thyroid hormones is given consistently during pregnancy and breastfeeding in particular. Dosage requirements may even increase during pregnancy. Since elevations in serum TSH may occur as early as 4 weeks of gestation, pregnant women taking levothyroxine should have their TSH measured during each trimester, in order to confirm that the maternal serum TSH values lie within the trimester specific pregnancy reference range. An elevated serum TSH level should be corrected by an increase in the dose of levothyroxine. Since postpartum TSH levels are similar to preconception values, the levothyroxine dosage should return to the pre-pregnancy dose immediately after delivery. A serum TSH level should be obtained 6-8 weeks postpartum.</p><p>The goal of TSH with Levothyroxin in pregnancy varies according to the trimester: during the first trimester it should be less than 2.5 mU / L and in the second and third trimester it should be less than 3 mU / L. <sup>(6)</sup></p>';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = 'Depends on Condition'; // was `${Math.ceil( 1.6 * this.form.data.kg )}`
				this.refNum.maintenanceDose = ''; // was 2

				// this.resultTxt = '<p>In elderly patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones: Treatment is initiated at a low dose, which is increased slowly and at lengthy intervals with frequent monitoring of thyroid hormones. A maintenance dose lower than required for complete correction of TSH levels may be considered.</p><p>The TSH target with Levothyroxin varies with age, under 60 years, from 1-2.5 mU / L, from 60-70 years 3-4 mU / L and over 70 years from 4-6 mU / L<sup>(5)</sup></p>';
				this.resultTxt = '<p>In older patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones, that is, a low initial dose (for example 12.5 microgram day) should be given which should then be increased slowly and at lengthy intervals (e.g.a a gradual increment of 12.5 microgram day fortnightly) with frequent monitoring of thyroid hormones. A dosage, lower than optimal dosage giving complete replacement therapy, consequentially not resulting in a complete correction of TSH level, might therefore need to be considered.<sup>(5)</sup></p>';
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>The doctor will determine your individual dose base on clinical examinations and on laboratory tests. As a number of patients show elevated concentrations of T4 and fT4, basal serum concentration of thyroid stimulating hormone provides a more reliable basis for subsequent treatment.</p>
			<p>You generally start with a low dose, which is increased every 2-4 weeks, until your full individual dose is reached. During the initial weeks of treatment, you will have appointments for laboratory tests in order to adjust the dose.</p>
		`;
	},

	// formulas for Indonesia EN
	en_ID () {
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>For neonates and infants with congenital hyporhyroidism, where rapid replacement is important, the initial recommended dosage is 10 to 15 micrograms per kg body weight (NW) per day for the first 3 months. Thereafter, the dose should be adjusted individually according to the clinical findings and thyroid hormone and TSH values.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `50-75`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> Only applicable to diagnosis during pregnancy</small></p>';
				this.resultTxt += '<p>Treatment with thyroid hormones is given consistently during pregnancy and breastfeeding in particular. Dosage requirements may even increase during pregnancy. Since elevations in serum TSH may occur as early as 4 weeks of gestation, pregnant women taking levothyroxine should have their TSH measured during each trimester, in order to confirm that the maternal serum TSH values lie within the trimester specific pregnancy reference range. An elevated serum TSH level should be corrected by an increase in the dose of levothyroxine. Since postpartum TSH levels are similar to preconception values, the levothyroxine dosage should return to the pre-pregnancy dose immediately after delivery. A serum TSH level should be obtained 6-8 weeks postpartum.</p><p>The goal of TSH with Eutirox<sup>®</sup> in pregnancy varies according to the trimester: during the first trimester it should be less than 2.5 mU / L and in the second and third trimester it should be less than 3 mU / L. <sup>(6)</sup></p>';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p>In elderly patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones: Treatment is initiated at a low dose, which is increased slowly and at lengthy intervals with frequent monitoring of thyroid hormones. A maintenance dose lower than required for complete correction of TSH levels may be considered.</p><p>The TSH target with Eutirox<sup>®</sup> varies with age, under 60 years, from 1-2.5 mU / L, from 60-70 years 3-4 mU / L and over 70 years from 4-6 mU / L<sup>(5)</sup></p>';
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>The doctor will determine your individual dose base on clinical examinations and on laboratory tests. As a number of patients show elevated concentrations of T4 and fT4, basal serum concentration of thyroid stimulating hormone provides a more reliable basis for subsequent treatment.</p>
			<p>You generally start with a low dose, which is increased every 2-4 weeks, until your full individual dose is reached. During the initial weeks of treatment, you will have appointments for laboratory tests in order to adjust the dose.</p>
		`;
	},

	// same formulas as Central EN
	en_PH () {
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>For neonates and infants with congenital hyporhyroidism, where rapid replacement is important, the initial recommended dosage is 10 to 15 micrograms per kg body weight (NW) per day for the first 3 months. Thereafter, the dose should be adjusted individually according to the clinical findings and thyroid hormone and TSH values.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `50-75`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> Only applicable to diagnosis during pregnancy</small></p>';
				this.resultTxt += '<p>Treatment with thyroid hormones is given consistently during pregnancy and breastfeeding in particular. Dosage requirements may even increase during pregnancy. Since elevations in serum TSH may occur as early as 4 weeks of gestation, pregnant women taking levothyroxine should have their TSH measured during each trimester, in order to confirm that the maternal serum TSH values lie within the trimester specific pregnancy reference range. An elevated serum TSH level should be corrected by an increase in the dose of levothyroxine. Since postpartum TSH levels are similar to preconception values, the levothyroxine dosage should return to the pre-pregnancy dose immediately after delivery. A serum TSH level should be obtained 6-8 weeks postpartum.</p><p>The goal of TSH with Eutirox<sup>®</sup> in pregnancy varies according to the trimester: during the first trimester it should be less than 2.5 mU / L and in the second and third trimester it should be less than 3 mU / L. <sup>(6)</sup></p>';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p>In elderly patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones: Treatment is initiated at a low dose, which is increased slowly and at lengthy intervals with frequent monitoring of thyroid hormones. A maintenance dose lower than required for complete correction of TSH levels may be considered.</p><p>The TSH target with Eutirox<sup>®</sup> varies with age, under 60 years, from 1-2.5 mU / L, from 60-70 years 3-4 mU / L and over 70 years from 4-6 mU / L<sup>(5)</sup></p>';
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>The individual dose will be determined on clinical examinations and on laboratory tests. As a number of patients show elevated concentrations of T4 and fT4, basal serum concentration of thyroid stimulating hormone provides a more reliable basis for subsequent treatment.</p>
			<p>Generally start with a low dose, which is increased every 2-4 weeks, until the full individual dose is reached. During the initial weeks of treatment, you will have appointments for laboratory tests in order to adjust the dose.</p>
		`;
	},

	// same formulas as Central EN
	en_ZA () {
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>For neonates and infants with congenital hyporhyroidism, where rapid replacement is important, the initial recommended dosage is 10 to 15 micrograms per kg body weight (NW) per day for the first 3 months. Thereafter, the dose should be adjusted individually according to the clinical findings and thyroid hormone and TSH values.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `50-75`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> Only applicable to diagnosis during pregnancy</small></p>';
				this.resultTxt += '<p>Treatment with thyroid hormones is given consistently during pregnancy and breastfeeding in particular. Dosage requirements may even increase during pregnancy. Since elevations in serum TSH may occur as early as 4 weeks of gestation, pregnant women taking levothyroxine should have their TSH measured during each trimester, in order to confirm that the maternal serum TSH values lie within the trimester specific pregnancy reference range. An elevated serum TSH level should be corrected by an increase in the dose of levothyroxine. Since postpartum TSH levels are similar to preconception values, the levothyroxine dosage should return to the pre-pregnancy dose immediately after delivery. A serum TSH level should be obtained 6-8 weeks postpartum.</p><p>The goal of TSH with Eutirox<sup>®</sup> in pregnancy varies according to the trimester: during the first trimester it should be less than 2.5 mU / L and in the second and third trimester it should be less than 3 mU / L. <sup>(6)</sup></p>';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p>In elderly patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones: Treatment is initiated at a low dose, which is increased slowly and at lengthy intervals with frequent monitoring of thyroid hormones. A maintenance dose lower than required for complete correction of TSH levels may be considered.</p><p>The TSH target with Eutirox<sup>®</sup> varies with age, under 60 years, from 1-2.5 mU / L, from 60-70 years 3-4 mU / L and over 70 years from 4-6 mU / L<sup>(5)</sup></p>';
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>The doctor will determine your individual dose base on clinical examinations and on laboratory tests. As a number of patients show elevated concentrations of T4 and fT4, basal serum concentration of thyroid stimulating hormone provides a more reliable basis for subsequent treatment.</p>
			<p>You generally start with a low dose, which is increased every 2-4 weeks, until your full individual dose is reached. During the initial weeks of treatment, you will have appointments for laboratory tests in order to adjust the dose.</p>
		`;
	},

	// same formulas as Central EN
	en_NG () {
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>For neonates and infants with congenital hyporhyroidism, where rapid replacement is important, the initial recommended dosage is 10 to 15 micrograms per kg body weight (NW) per day for the first 3 months. Thereafter, the dose should be adjusted individually according to the clinical findings and thyroid hormone and TSH values.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `50-75`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> Only applicable to diagnosis during pregnancy</small></p>';
				this.resultTxt += '<p>Treatment with thyroid hormones is given consistently during pregnancy and breastfeeding in particular. Dosage requirements may even increase during pregnancy. Since elevations in serum TSH may occur as early as 4 weeks of gestation, pregnant women taking levothyroxine should have their TSH measured during each trimester, in order to confirm that the maternal serum TSH values lie within the trimester specific pregnancy reference range. An elevated serum TSH level should be corrected by an increase in the dose of levothyroxine. Since postpartum TSH levels are similar to preconception values, the levothyroxine dosage should return to the pre-pregnancy dose immediately after delivery. A serum TSH level should be obtained 6-8 weeks postpartum.</p><p>The goal of TSH with Eutirox<sup>®</sup> in pregnancy varies according to the trimester: during the first trimester it should be less than 2.5 mU / L and in the second and third trimester it should be less than 3 mU / L. <sup>(6)</sup></p>';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p>In elderly patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones: Treatment is initiated at a low dose, which is increased slowly and at lengthy intervals with frequent monitoring of thyroid hormones. A maintenance dose lower than required for complete correction of TSH levels may be considered.</p><p>The TSH target with Eutirox<sup>®</sup> varies with age, under 60 years, from 1-2.5 mU / L, from 60-70 years 3-4 mU / L and over 70 years from 4-6 mU / L<sup>(5)</sup></p>';
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>The doctor will determine your individual dose base on clinical examinations and on laboratory tests. As a number of patients show elevated concentrations of T4 and fT4, basal serum concentration of thyroid stimulating hormone provides a more reliable basis for subsequent treatment.</p>
			<p>You generally start with a low dose, which is increased every 2-4 weeks, until your full individual dose is reached. During the initial weeks of treatment, you will have appointments for laboratory tests in order to adjust the dose.</p>
		`;
	},

	// formulas for Morroco
	fr_MA () {
		/* MA Value
		1 -> 0-12 months
		2 -> 1-5 years
		3 -> 6-12 years
		4 -> 13-17 years
		5 -> 18-64 years
		6-> > >=65 years
		*/
		switch ( this.pathology ) {
			case 'hypothyroidism':
				// infant and newborn
				if ( this.age == 1 ) {
					if ( this.ageMonths == "ageMonths-0_3" ) {
						this.initialDose = `${Math.ceil( 10 * this.form.data.kg )}-${Math.ceil( 15 * this.form.data.kg )}`;
						this.maintenanceDose = null;
						this.resultTxt = '<p>Ces doses sont à ajuster en fonction de la réponse clinique et des résultats des épreuves de laboratoire</p>';
					}
					if ( this.ageMonths == "ageMonths-3_6" ) {
						this.initialDose = `${Math.ceil( 8 * this.form.data.kg )}-${Math.ceil( 10 * this.form.data.kg )}`;
						this.maintenanceDose = null;
						this.resultTxt = '<p>Ces doses sont à ajuster en fonction de la réponse clinique et des résultats des épreuves de laboratoire</p>';
					}
					if ( this.ageMonths == "ageMonths-6_12" ) {
						this.initialDose = `${Math.ceil( 6 * this.form.data.kg )}-${Math.ceil( 8 * this.form.data.kg )}`;
						this.maintenanceDose = null;
						this.resultTxt = '<p>Ces doses sont à ajuster en fonction de la réponse clinique et des résultats des épreuves de laboratoire</p>';
					}
				}
				if ( this.age == 2 ) {
					this.initialDose = `${Math.ceil( 5 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
					this.maintenanceDose = null;
					this.resultTxt = '<p>Ces doses sont à ajuster en fonction de la réponse clinique et des résultats des épreuves de laboratoire</p>';
				}
				if ( this.age == 3 ) {
					this.initialDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 5 * this.form.data.kg )}`;
					this.maintenanceDose = null;
					this.resultTxt = '<p>Ces doses sont à ajuster en fonction de la réponse clinique et des résultats des épreuves de laboratoire</p>';
				}
				if ( this.age == 4 ) {
					this.initialDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
					this.maintenanceDose = null;
					this.resultTxt = '<p>Ces doses sont à ajuster en fonction de la réponse clinique et des résultats des épreuves de laboratoire</p>';
				}
				// acquired condition - substitutes all the above
				if ( this.age <= 4 && this.form.data.condition == "acquired" ) {
					this.initialDose = `12.5 - 50`;
					this.maintenanceDose = null;
					this.resultTxt = '<p>La dose devra être augmentée progressivement toutes les 2 à 4 semaines en fonction des résultats cliniques, des taux des hormones thyroïdiennes et de la TSH  jusqu\'à ce que la dose  permettant la substitution complétée soit atteinte</p>';
				}


				if ( this.age == 5 ) {
					if ( this.form.data.condition == "hypo-1" ) {
						this.initialDose = `${Math.ceil( 1.7 * this.form.data.kg )}`;
						this.maintenanceDose = null;
						this.resultTxt = `
							<!-- img #1 -->
							<h4>Comment surveiller ce patient?</h4>
							<p>Il est recommandé de surveiller le traitement par un dosage de la TSH, l'amélioration des signes cliniques d'hypothyroïdie et l'absence de signes de surdosage en lévothyroxine. Il est recommandé de réaliser un dosage de la TSH:</p>
							<ul>
								<li>6 à 8 semaines après le début du traitement;</li>
								<li>6 à 8 semaines après tout changement de dose au cours du traitement ou en cas de changement de spécialité à base de lévothyroxine;</li>
								<li>en cas de variation importante du poids corporel;</li>
								<li>en cas de persistance et/ou d'aggravation des signes cliniques.</li>
							</ul>

							<!-- img #2 -->
							<h4>Quand considérer que le patient est équilibré?</h4>
							<p>Il est recommandé de considérer un patient en cours de traitement par lévothyroxine comme équilibré lorsqu'avec la même posologie, deux résultats de TSH sont retrouvés dans l'intervalle de référence, à 3 mois d'intervalle.</p>
							<p>L'amélioration des signes cliniques et du ressenti du patient doit être intégrée à la notion d'équilibration.</p>
							<p>La persistance de signes cliniques invite à rechercher un diagnostic différentiel.</p>

							<!-- img #3 -->
							<h4>Quel suivi des patients équilibrés?</h4>
							<p>Une fois le patient bien équilibré par le traitement, il est recommandé de réaliser un suivi annuel par un dosage de la TSH en tenant compte du ressenti du patient. Pour les patients bien équilibrés par le traitement et sans effet indésirable, il est recommande de garder la même spécialité à base de lévothyroxine. Néanmoins, si le médicament à base de lévothyroxine n'est pas ou n'est plus disponible sur le marché et qu'il est nécessaire d'en changer, il est alors recommandé de mettre en œuvre une surveillance clinique et hormonale (TSH) adaptée à la suite de l'introduction de la nouvelle spécialité à base de lévothyroxine.</p>

							<!-- img #4 -->
							<h4>CAT si la TSH reste instable et/ou les signes persistent?</h4>
							<p>En cas de TSH fluctuant souvent en dehors de l'intervalle de référence malgré un traitement substitutif à des doses en adéquation avec le poids corporel (1,6 μg/kg/jour) ou de fortes doses de LT4, il est recommandé de rechercher une cause:</p>
							<ul>
								<li>évaluation de l'observance du traitement;</li>
								<li>des modalités de prise;</li>
								<li>recherche d'interactions médicamenteuses;</li>
								<li>situations affectant le métabolisme et l'absorption de la lévothyroxine11;</li>
								<li>interférences analytiques avec le dosage de la TSH (anticorps interférents, prise de biotine) à évoquer en cas de résultat jugé incohérent par le clinicien.</li>
							</ul>
							<p>Un dosage de la TSH par une autre technique est à discuter avec le biologiste. Il est recommandé d'augmenter les doses de lévothyroxine uniquement après avoir éliminé les causes sous-jacentes à la persistance des signes cliniques ou entraînant des fluctuations de la TSH.</p>
							<p>Si les symptômes cliniques persistent, en cas de difficultés d'équilibration et/ou de discordance entre les signes cliniques et la biologie, il est conseillé de réaliser un dosage de TSH et de la T4L pour rechercher une mauvaise observance ou une malabsorption.</p>
						`;
					}
					if ( this.form.data.condition == "hypo-2" ) {
						this.initialDose = `25-50`;
						this.maintenanceDose = null;
						this.resultTxt = `
							<p><b>25-50 μg/jour et augmenter progressi-vement les doses toutes les 6 à 8 semaines jusqu’à obtenir la substitution adaptée.</b></p>
							<!-- img #5 -->
							<p>Pour les patients de moins de 65 ans présentant une hypothyroïdie fruste, l'initiation d'un traitement est à évaluer selon la symptomatologie clinique et le risque d'évolution vers une hypothyroïdie avérée d'une pathologie thyroïdienne préexistante et dans le cadre d'une décision médicale partagée. Ne pas débuter de traitement avant d'avoir réalisé deux dosages de la TSH entre 6 à 12 semaines d'intervalle.</p>

							<!-- img #6 -->
							<p><b>Si la TSH est supérieure à 10 mUI/L et la T4L dans l'intervalle de référence</b>: l'initiation d'un traitement est recommandée.</p>
							<p><b>Si la TSH est comprise entre 4 et 10 mUI/L</b>: il est recommandé d'envisager un traitement si l'un des critères suivants est retrouvé:</p>
							<ul>
								<li>signes cliniques d'hypothyroïdie;</li>
								<li>anticorps anti-TPO positifs13;</li>
								<li>antécédents cardiovasculaires, notamment cardiopathie ischémique et/ou facteurs de risque cardiovasculaire (dyslipidémie);</li>
								<li>goitre.</li>
							</ul>
							<p>La décision thérapeutique doit être discutée avec le patient, dans le cadre d'une décision médicale partagée, selon le ressenti de la personne et l'intensité des signes cliniques</p>

							<!-- img #7 -->
							<h4>Quel suivi des patients équilibrés?</h4>
							<p>Dans le cas de l'hypothyroïdie fruste, la prise en charge du patient traité rejoint celle de l'hypothyroïdie avérée.</p>
							<p>La surveillance des patients en hypothyroïdie fruste et traités est fondée sur une évaluation clinicobiologique; un dosage de la TSH est recommandé entre 6 à 8 semaines après l'instauration du traitement pour ajuster la dose selon un objectif de TSH dans l'intervalle de référence et le ressenti du patient.</p>
							<p>Si les signes cliniques persistent, il est recommandé de réaliser un dosage de la TSH et, après avoir éliminé les diagnostics différentiels, d'ajuster les doses.</p>
							<p>L'arrêt du traitement peut se discuter en cas de TSH normalisée depuis 3 à 4 mois, sans amélioration clinique.</p>
							<p>Si le traitement est efficace, un dosage annuel de la TSH est recommandé.</p>
						`;
					}
					if ( this.form.data.condition == "elder" ) {
						this.initialDose = `12.5-25`;
						this.maintenanceDose = null;
						this.resultTxt = `
							<p><b>12,5 à 25 ug / jour avec une titration lente sur 4 à 6 semaines en fonction des symptômes et des taux sériques de TSH.</b></p>
							<p>La dose d’initiation de LT4 est de 12,5 à 25 ug / jour avec une titration lente sur 4 à 6 semaines en fonction des symptômes et des taux sériques de TSH.</p>
						`;
					}
					if ( this.form.data.condition == "preg-4" ) {
						this.initialDose = `12.5-25`;
						this.maintenanceDose = null;
						this.resultTxt = `
							<p><b>Augmenter les doses habituelles de 20 % à 30 % afin d’obtenir un taux de TSH compris entre la limite inférieure et 2,5mUI/L.</b></p>
							
							<!-- img #9 -->
							<p>Il existe une particularité en ce qui concerne la surveillance de l'hypothyroïdie clinique connue diagnostiquée avant la grossesse: en effet, les recommandations de l'ATA 2017, précisent qu'il est nécessaire d'effectuer un dosage sérique de la TSH avant la conception et que la dose de T4l doit être ajustée afin d'obtenir un taux de TSH compris entre la limite inférieure et 2,5mUI/L. Un moyen pour y parvenir consiste à administrer deux comprimés supplémentaires par semaine à la posologie quotidienne actuelle de la patiente.</p>

							<!-- img #10 -->
							<p>Lorsqu'un traitement par lévothyroxine est initié chez une personne âgée et afin de limiter le risque iatrogène (angor, ostéoporose, fibrillation atriale), il est recommandé de:</p>
							<ul>
								<li>débuter à une faible posologie (notamment en cas de risque-cardiovasculaire), par exemple-12,5 à 25 μg/jet de proposer une majoration-progressive.</li>
								<li><b>viser une cible thérapeutique de TSH</b> dans les valeurs hautes de l'intervalle de référence adaptées à l'âge en lien avec le ressenti du patient et le risque iatrogène (et <b>dans tous les cas > à 1 mUI/L</b>).</li>
							</ul>
							<p>Dans le cadre d'une hypothyroïdie fruste, lorsqu'un traitement d'épreuve par lévothyroxine est introduit dans le but de réduire des symptômes et que ceux-ci persistent après normalisation de la TSH, il est recommandé d'envisager l'arrêt du traitement dans le cadre d'une décision médicale concertée et partagée.</p>
						`;
					}
					if ( this.form.data.condition == "preg-5" ) {
						if ( this.form.data.tsh < 2.5 ) {
							this.initialDose = `-`;
							this.maintenanceDose = null;
						}
						if ( this.form.data.tsh >= 2.5 && this.form.data.tsh < 10 ) {
							this.initialDose = `25-50`;
							this.maintenanceDose = null;
						}
						if ( this.form.data.tsh >= 10 && this.form.data.tsh <= 20 ) {
							this.initialDose = `50-75`;
							this.maintenanceDose = null;
						}
						if ( this.form.data.tsh > 20 ) {
							this.initialDose = `75-100`;
							this.maintenanceDose = null;
						}

						this.resultTxt = `
							<!-- img #8 -->
							<h4>L'intervalle de référence cible de la TSH</h4>
							<table class="table">
								<thead>
									<tr>
										<th>Trimestre</th>
										<th>L'intervalle de référence cible de la TSH</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Premier</td>
										<td>0,1-2,5 mIU/L</td>
									</tr>
									<tr>
										<td>Deuxième</td>
										<td>0,2-3,0 mIU/L</td>
									</tr>
									<tr>
										<td>Troisième</td>
										<td>0,3-3,0 mIU/L</td>
									</tr>
								</tbody>
							</table>
							 
							<p>En l'absence de traitement et en cas de positivité en anticorps anti-TPO, une surveillance de la TSH est recommandée toutes les 4 à 6 semaines jusqu'à 22 SA puis une fois entre 30 et 34 SA et au moins une fois en post-partum.</p>
						`;
					}
				}

				if ( this.age == 6 ) {
					this.initialDose = `12.5-25`;
					this.maintenanceDose = null;
					this.resultTxt = `<p>Faible posologie 12,5 à 25 µg/j et de proposer une majoration progressive.</p>
						<!-- img #10 -->
							<p>Lorsqu'un traitement par lévothyroxine est initié chez une personne âgée et afin de limiter le risque iatrogène (angor, ostéoporose, fibrillation atriale), il est recommandé de:</p>
							<ul>
								<li>débuter à une faible posologie (notamment en cas de risque-cardiovasculaire), par exemple-12,5 à 25 μg/jet de proposer une majoration-progressive.</li>
								<li><b>viser une cible thérapeutique de TSH </b>dans les valeurs hautes de l'intervalle de référence adaptées à l'âge en lien avec le ressenti du patient et le risque iatrogène (et <b>dans tous les cas > à 1 mUI/L</b>).</li>
							</ul>
							<p>Dans le cadre d'une hypothyroïdie fruste, lorsqu'un traitement d'épreuve par lévothyroxine est introduit dans le but de réduire des symptômes et que ceux-ci persistent après normalisation de la TSH, il est recommandé d'envisager l'arrêt du traitement dans le cadre d'une décision médicale concertée et partagée.</p>
					`;
				}

				break;

			// NOT USED AT THE MOMENT
			case 'hemithyroidectomy' :
			case 'thyroidectomy' :
				this.initialDose = ` - `;
				this.maintenanceDose = `75-200`;
				break;

			case 'hyperthyroidism' :
				this.initialDose = ` - `;
				this.maintenanceDose = `50-100`;
				break;

			case 'cancer' :
				if ( this.form.data.condition == "cancer-substitution" ) {
					this.initialDose = `${Math.ceil( 1 * this.form.data.kg )}`;
					this.maintenanceDose = null;
					this.resultTxt = `
						<p>En cas d’hormonothérapie substitutive, le dosage de la lévothyroxine prescrit correspond à un niveau normal d’hormones thyroïdiennes.</p>
             			<p>Le traitement vise ainsi à remplacer les hormones thyroïdiennes en maintenant le taux de TSH sanguin dans les normes de référence.</p>`;
				}
				if ( this.form.data.condition == "cancer-braking" ) {
					this.initialDose = `${Math.ceil( 2 * this.form.data.kg )}`;
					this.maintenanceDose = null;
					this.resultTxt = `
						<p>En cas d’hormonothérapie frénatrice, le dosage de la lévothyroxine prescrit correspond à un niveau plus élevé que la normale d’hormones thyroïdiennes limitant ainsi la production de TSH dont le taux sanguin sera alors maintenu en dessous des normes de référence.</p>
						<p>L’hormonothérapie frénatrice est indiquée lorsque le risque de récidive est élevé ou que les traitements initiaux n’ont pas réussi à enlever la totalité des cellules cancéreuses.</p>						
					`;
				}

				break;
		}

		// There is no text for results right now
		this.generalResultTxt = '<p>Les valeurs présentées le sont à titre indicatif, car la réponse interindividuelle est très variable. La décision thérapeutique doit être prise par le médecin sur la base des valeurs de TSH présentées par le patient.</p>'
	},

	// same formulas as Central EN
	en_KE () {
		if ( this.age == 1 ) {
			this.initialDose = ` - `;
			// this.refNum.initialDose = '1';
			this.maintenanceDose = `10-15`;
			this.refNum.maintenanceDose = '1';

			this.resultTxt = '<p>For neonates and infants with congenital hyporhyroidism, where rapid replacement is important, the initial recommended dosage is 10 to 15 micrograms per kg body weight (NW) per day for the first 3 months. Thereafter, the dose should be adjusted individually according to the clinical findings and thyroid hormone and TSH values.</p>';
		}

		if ( this.age == 2 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 4 * this.form.data.kg )}-${Math.ceil( 6 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 3 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 3 * this.form.data.kg )}-${Math.ceil( 4 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age == 4 ) {
			this.initialDose = `12.5-25`;
			this.refNum.initialDose = '1';
			this.maintenanceDose = `${Math.ceil( 2 * this.form.data.kg )}-${Math.ceil( 3 * this.form.data.kg )}`;
			this.refNum.maintenanceDose = '4';
			this.resultTxt = '<p>Infants may receive the entire daily dose of Eutirox<sup>®</sup> at least half an hour before the first meal of the day. Immediately before use, crush the tablet and mix it with some water and give it to the child with some more liquid. Always prepare the mixture freshly.</p>';
		}
		if ( this.age >= 5 ) {
			console.log( this.form.data.condition );
			if ( this.form.data.condition == 'healthy' ) {
				this.initialDose = `25-50`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';
			}
			if ( this.form.data.condition == 'preagnant' ) {
				this.initialDose = `50-75`;
				this.refNum.initialDose = '1,**';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg * 1.3 )}`;
				this.refNum.maintenanceDose = '3';

				this.resultTxt = '<p><small><sup>**</sup> Only applicable to diagnosis during pregnancy</small></p>';
				this.resultTxt += '<p>Treatment with thyroid hormones is given consistently during pregnancy and breastfeeding in particular. Dosage requirements may even increase during pregnancy. Since elevations in serum TSH may occur as early as 4 weeks of gestation, pregnant women taking levothyroxine should have their TSH measured during each trimester, in order to confirm that the maternal serum TSH values lie within the trimester specific pregnancy reference range. An elevated serum TSH level should be corrected by an increase in the dose of levothyroxine. Since postpartum TSH levels are similar to preconception values, the levothyroxine dosage should return to the pre-pregnancy dose immediately after delivery. A serum TSH level should be obtained 6-8 weeks postpartum.</p><p>The goal of TSH with Eutirox<sup>®</sup> in pregnancy varies according to the trimester: during the first trimester it should be less than 2.5 mU / L and in the second and third trimester it should be less than 3 mU / L. <sup>(6)</sup></p>';
			}
			if ( this.form.data.condition == 'elder' ) {
				this.initialDose = `25`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `${Math.ceil( 1.6 * this.form.data.kg )}`;
				this.refNum.maintenanceDose = '2';

				this.resultTxt = '<p>In elderly patients, in patients with coronary heart disease, and in patients with severe or long-existing hypothyroidism, special caution is required when initiating therapy with thyroid hormones: Treatment is initiated at a low dose, which is increased slowly and at lengthy intervals with frequent monitoring of thyroid hormones. A maintenance dose lower than required for complete correction of TSH levels may be considered.</p><p>The TSH target with Eutirox<sup>®</sup> varies with age, under 60 years, from 1-2.5 mU / L, from 60-70 years 3-4 mU / L and over 70 years from 4-6 mU / L<sup>(5)</sup></p>';
			}

			if ( this.form.data.condition == 'post-operation' ) {
				this.initialDose = `75`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `200`;
				this.refNum.maintenanceDose = '1';
			}

			if ( this.form.data.condition == 'cancer-nodules-supression' ) {
				this.initialDose = `150`;
				this.refNum.initialDose = '1';
				this.maintenanceDose = `300`;
				this.refNum.maintenanceDose = '1';
			}
		}

		// Text common to all results
		this.generalResultTxt = `
			<p>The doctor will determine your individual dose base on clinical examinations and on laboratory tests. As a number of patients show elevated concentrations of T4 and fT4, basal serum concentration of thyroid stimulating hormone provides a more reliable basis for subsequent treatment.</p>
			<p>You generally start with a low dose, which is increased every 2-4 weeks, until your full individual dose is reached. During the initial weeks of treatment, you will have appointments for laboratory tests in order to adjust the dose.</p>
		`;
	}
};
