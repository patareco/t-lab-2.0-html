/**
 * Localized info about pack sizes plus helper functions
 */

const packCalc = {
	// packs2quant (optional) gives the available quantities for each dosage.
	packs2quant: null,
	// packs is the size of the pills (mcg)
	packs: null,
	// quantities is the number of pills in each pack
	quantities: null,

	labels: {
		packLabelSing: null,
		packLabelPlur: null,
		templateDoseSentence: null,
		templateBoxSentence: null
	},

	pic: false,

	utils: {
		/**
		* @function getTheValue
		*
		* get the value of the combination array
		* @returns a float number
		*/
		getTheValue ( str ) {
			return parseFloat( str.split( '|' )[0] );
		},

		result2json ( str ) {
			const json = {};
			const packs = str.split( '|' )[1].split( '+' );

			packs.forEach( el => {
				el.replace( /(\d+):(\d+)/, ( val, pack, num ) => {
					json[packCalc.quantities[pack]] = ( json[packCalc.quantities[pack]] ) ? parseInt( json[packCalc.quantities[pack]] ) + parseInt( num ) : parseInt( num );
				} )
			} )

			return json;
		}
	},

	/**
	* @function findClosestInPackArray
	*
	* finds the closest value in the packs array,
	* this variable should be assigned by using the locale functions
	* @returns (number) closest value in pack array
	*/
	findClosestInPackArray ( goal ) {
		if ( !this.packs ) return;

		// have to be in crescent order because it will retun the first ocurrence in case
		// the input value is smaller than any of the array elements
		this.packs.sort( ( a, b ) => a - b );

		return this.packs.reduce( function ( prev, curr ) {
			return ( ( Math.abs( curr - goal ) < Math.abs( prev - goal ) ) && ( goal - curr >= 0 ) ? curr : prev );
		} );
	},

	/**
	* @function findClosestInPackArray
	*
	* @param pack - the pack for which you want the picture url.
	* @returns (string) fully formed url for pack img src
	*/
	/* getPicUrl ( pack ) {
		return this.pic.baseUrl + this.pic.baseName + pack + '-' + this.quantities[0] + '.' + this.pic.ext;
	}, */

	getPicUrl ( pack ) {
		if ( this.packs2quant !== null ) {
			return this.pic.baseUrl + this.pic.baseName + pack + '-' + this.packs2quant[pack][0] + '.' + this.pic.ext;
		} else {
			return this.pic.baseUrl + this.pic.baseName + pack + '-' + this.quantities[0] + '.' + this.pic.ext;
		}
	},

	setupLabels () {
		const doseSentenceEl = document.getElementById( 'dose-sentence' );
		this.labels.templateDoseSentence = doseSentenceEl.innerHTML;

		// box sentence
		this.labels.templateBoxSentence = document.getElementById( 'pack-sizes' ).innerHTML;
	},

	/**
	* start of localized functions
	*
	* */
	default () {
		// mg of packs
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150, 175, 200];
		// num of pill in eac pack
		this.quantities = [20, 100];
	},

	pt_BR () {
		/* eslint-disable key-spacing */
		/* this.packs2quant = {
			25:  [50, 30],
			50:  [50, 30],
			75:  [50, 30],
			88:  [50, 30],
			100: [50, 30],
			112: [50],
			125: [50],
			137: [50],
			150: [50],
			175: [50],
			200: [50]
		} */
		/* eslint-enable key-spacing */
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150, 175, 200];
		// this.quantities = [30, 50];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/pt_BR/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	es_CL () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150, 175, 200];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/es_CL/packs/',
			baseName: 'et-',
			ext: 'jpg'
		}
	},

	pt_PT () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150, 175, 200];
		this.quantities = [60];
	},

	es_CT () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 150, 175, 200];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/es_CT/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	// second CC for center
	es_GT () {
		this.es_CT();
	},

	en_CT () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 150, 175, 200];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/en_CT/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	// second CC for center
	en_GT () {
		this.en_CT();
	},

	es_CO () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 150, 175, 200];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/es_CO/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	es_EC () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/es_EC/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	es_PE () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 150, 175];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/es_PE/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	es_MX () {
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150, 175, 200];
		this.quantities = [50];
		this.pic = {
			baseUrl: './img/i18n/es_MX/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	de_AT () {
		this.packs2quant = {
			/* eslint-disable key-spacing */
			25:  [100],
			50:  [100],
			75:  [100],
			88:  [100],
			100: [100],
			112: [50],
			125: [100],
			137: [50],
			150: [100],
			175: [100],
			200: [100]
			/* eslint-enable key-spacing */
		}
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150, 175, 200];
		this.quantities = [50, 100];
		this.pic = {
			baseUrl: './img/i18n/de_AT/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	pl_PL () {
		this.packs2quant = {
			/* eslint-disable key-spacing */
			25:  [50, 100],
			50:  [50, 100],
			75:  [50, 100],
			88:	 [50],
			100: [50, 100],
			112: [50],
			125: [50, 100],
			137: [50],
			150: [50, 100],
			175: [50],
			200: [50]
			/* eslint-enable key-spacing */
		}
		this.packs = [25, 50, 75, 88, 100, 112, 125, 137, 150, 175, 200];
		this.quantities = [50, 100];
		this.pic = {
			baseUrl: './img/i18n/pl_PL/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	en_SA () {
		this.packs = [25, 50, 100, 150];
		this.quantities = [100];
	},

	en_ID () {
		this.packs = [50, 100];
		this.quantities = [25];
		this.pic = {
			baseUrl: './img/i18n/en_ID/packs/',
			baseName: 'et-',
			ext: 'png'
		}
	},

	en_PH () {
		this.packs = [25, 50, 100, 150];
		this.quantities = [100];
		this.pic = {
			baseUrl: './img/i18n/en_PH/packs/',
			baseName: 'et-',
			ext: 'jpg'
		}
	},

	en_ZA () {
		this.packs = [25, 50, 75, 100];
		this.quantities = [30];
		this.pic = {
			baseUrl: './img/i18n/en_ZA/packs/',
			baseName: 'et-',
			ext: 'jpg'
		}
	},

	en_NG () {
		this.packs = [25, 50, 100];
		this.quantities = [100];
		this.pic = {
			baseUrl: './img/i18n/en_NG/packs/',
			baseName: 'et-',
			ext: 'jpg'
		}
	},

	fr_MA () {
		this.packs = [25, 50, 100];
		this.quantities = [100];
		/* this.pic = {
			baseUrl: './img/i18n/en_NG/packs/',
			baseName: 'et-',
			ext: 'jpg'
		} */
	},

	en_KE () {
		this.packs = [25, 50, 100];
		this.quantities = [30];
		this.pic = {
			baseUrl: './img/i18n/en_KE/packs/',
			baseName: 'et-',
			ext: 'jpg'
		}
	}
}
