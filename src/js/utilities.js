/* eslint-disable no-unused-vars */
/**
* general purpose functions
*
*/

/**
* @function scrollToTargetAdjusted
*
* Open native dialog for editing profile
* @returns null
*/
function scrollToTargetAdjusted ( el, offset = document.querySelector( 'header' ).offsetHeight + 5 ) {
	const element = document.querySelector( el );
	const headerOffset = offset;

	const bodyRect = document.body.getBoundingClientRect().top;
	const elementRect = element.getBoundingClientRect().top;
	const elementPosition = elementRect - bodyRect;
	const offsetPosition = elementPosition - offset;
	// console.log( offsetPosition );

	window.scrollTo( {
		top: offsetPosition,
		behavior: "smooth"
	} );
}


const utils = {
	/**
	* @function scrollToTargetAdjusted
	*
	* get url param for country specific formulas
	* @returns language in iso format uderscore separated
	*/
	getLanguage () {
		const queryString = window.location.search;
		const urlParams   = new URLSearchParams( queryString )
		const isDefault   = !urlParams.has( 'country' );
		let country       =  ( isDefault ) ? 'default' : urlParams.get( 'country' ).replace( '-', '_' );

		// check if language calculation exists, if not use default...
		if (
			typeof presCalc[country] == "undefined" ||
			typeof appTranslations[country] == "undefined" ||
			typeof refsTranslation[country] == "undefined" ||
			typeof packCalc[country] == "undefined"
		) {
			console.warn( "Missing keys for language in URL Param, setting default!" );
			console.warn(
				'Files/Object missing:',
				( typeof presCalc[country] == "undefined" ) ? 'presCalc.js / presCalc' : '',
				( typeof appTranslations[country] == "undefined" ) ? 'transInterface.js / appTranslations' : '',
				( typeof refsTranslation[country] == "undefined" ) ? 'transInterface.js / refsTranslation' : '',
				( typeof packCalc[country] == "undefined" ) ? 'packCalc.js / packCalc' : ''
			);

			// set default country
			country = 'default';
		}

		console.log( country, 'country is active' );
		return country;
	},

	/**
	* @function getAllSubsets
	*
	* get the powerset of a given array
	* this is possibly the smallest implementation possible albeit not necessarily efficient
	* really interesting discussions @stackoverflow
	*
	* @url https://stackoverflow.com/questions/42773836/how-to-find-all-subsets-of-a-set-in-javascript
	* @returns an array containing all combinations regardless of order
	*/
	getAllSubsets ( theArray ) {
		return theArray.reduce(
			( subsets, value, i, sArr ) => subsets.concat(
				subsets.map( set => [value, ...set] )
			),
			[[]]
		);
	}
}
