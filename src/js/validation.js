// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
	window.addEventListener('load', function() {
	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.getElementsByClassName('needs-validation');
	// Loop over them and prevent submission
	var validation = Array.prototype.filter.call(forms, function(form) {
		form.addEventListener('submit', function(event) {
			
			event.preventDefault();
			event.stopPropagation();		
			if (form.checkValidity() === true ) {
				formProcess(form, event.target.action);
			}
			
			form.classList.add('was-validated');
		 
		}, false);
		
	});

	}, false);


	//match pass
	var passEl = document.getElementById('password');
	var passConfirmEl = document.getElementById('password-confirm');
	
	if (passEl && passConfirmEl) {

		passConfirmEl.addEventListener('keyup', function(event) {
			
			if (passEl.value != document.getElementById('password-confirm').value) {
				// alert('passwords dont match')
					passConfirmEl.setCustomValidity("Passwords Don't Match");
				} else {
					passConfirmEl.setCustomValidity("");

				}
		}, false);

	}
	
})();