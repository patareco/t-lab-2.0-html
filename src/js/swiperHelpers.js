
// some helpers to provide functions for usage with swiper.

// eslint-disable-next-line no-unused-vars
const swiperHelper = {
	windowWidth: window.innerWidth,
	containerW: document.querySelector( ".container" ).clientWidth,
	bootstrapPad: 15,

	recalculateProps () {
		this.windowWidth = window.innerWidth;
		this.containerW = document.querySelector( ".container" ).clientWidth;
	},

	getPageOffset () {
		this.recalculateProps();
		return ( this.windowWidth - this.containerW ) / 2 + this.bootstrapPad;
	},

	// num slides in swiper is not very well calculated because of the offset so we calculate the true value
	// accounting for the offset

	getNumSlides () {
		this.recalculateProps();
		return this.windowWidth / ( this.containerW - this.bootstrapPad * 2 );
	},

	applySlideWidth ( cls ) {
		if ( !cls ) {
			console.log( 'A class must be passed for the slide element' );
			return
		}

		const calcSlideWidth = this.containerW - this.bootstrapPad * 2

		document.querySelectorAll( cls ).forEach( el => { el.style.width = calcSlideWidth + 'px' } )
	},
	updateSlideWidth ( slideArr ) {
		if ( !slideArr ) {
			console.log( 'The swiper.slides Array must be passed for resizing' );
			return
		}

		this.recalculateProps();
		const calcSlideWidth = this.containerW - this.bootstrapPad * 2

		slideArr.forEach( el => { el.style.width = calcSlideWidth + 'px' } )
	},

	getPaginationLabels ( cls ) {
		if ( !cls ) {
			console.log( 'A class must be passed for the pagination element' );
			return
		}
		const arr = [];

		document.querySelectorAll( cls + ' span' ).forEach( e => arr.push( e.innerHTML ) )

		return arr;
	}
}
