/* eslint-disable no-unused-vars */
/**
* @function addAnalyticsIframe
*
* Adds Google analytics Script tag to head if
* in iFrame mode
*
* @returns null
*/

( function addAnalyticsIframe () {
	if ( navigator.userAgent.indexOf( "TLAB_APP" ) == -1 && location.hostname !== "localhost" ) {
		console.log( "not native app" );

		// change page title for analytics purpose
		const pathName = window.location.pathname;
		const arrPaths = pathName.split( '/' ).filter( ( elm ) => elm );
		const countryPattern = /^[a-zA-Z]{2}-[a-zA-Z]{2}$/;
		const theIndex = arrPaths.length >= 2 ? 1 : 0;

		const notNumber = isNaN( arrPaths[theIndex].split( '-' )[0] );
		const usePattern = arrPaths[theIndex].match( countryPattern ) !== null;

		const isDefault = notNumber && !usePattern;
		const theCountry = isDefault ? 'default' : arrPaths[theIndex];

		document.title = `T-Lab2.0 ${theCountry}`;

		// add analytics script tag
		const script = document.createElement( 'script' );
		script.type = 'text/javascript';
		script.src = 'https://www.googletagmanager.com/gtag/js?id=G-MEZ47ZP4FS';
		document.head.appendChild( script );

		window.dataLayer = window.dataLayer || [];
		function gtag () { dataLayer.push( arguments ); }
		gtag( 'js', new Date() );

		gtag( 'config', 'G-MEZ47ZP4FS' );
	}
} )();
