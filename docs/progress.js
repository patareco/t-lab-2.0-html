/*************************************************
 * PROGRESS STATUS
 **************************************************/

let first = true;
const barSize = 20;
let progress, nHash, bar;

function uploading ( data ) {
	// console.log(data.totalFilesCount); // total file count being transferred
	// console.log(data.transferredFileCount); // number of files transferred
	// console.log(data.filename); // partial path with filename being uploaded
	if ( !first ) {
		clearProgress();
	}
	first = false;

	writeProgress( data );
}

function uploaded ( data ) {
	clearProgress();
	writeProgress( data );
}


function writeProgress ( data ) {
	if ( typeof process.stdout.cursorTo == "function" ) {
		process.stdout.write( "=============================================\n" );
		process.stdout.write( "Uploading to remote server... \n" );
		process.stdout.write( data.filename + "\n" );
		progress = Math.ceil(
			( data.transferredFileCount / data.totalFilesCount ) * 100
		);

		nHash = ( progress / 100 ) * barSize;
		bar = "[";
		for ( let i = 0; i <= nHash; i++ ) {
			bar += "#";
		}
		for ( let i = barSize - nHash; i > 1; i-- ) {
			bar += "-";
		}
		bar += "]";
		// "[##############------]"
		process.stdout.write( bar + " " + progress + "%\n" );
		process.stdout.write( "=============================================" );
	}
}

function clearProgress () {
	if ( typeof process.stdout.cursorTo == "function" ) {
		process.stdout.cursorTo( 0 );
		process.stdout.moveCursor( 0, -4 );
		process.stdout.clearScreenDown();
	}
}

module.exports = {
	writeProgress,
	clearProgress,
	uploading,
	uploaded
}
