const exec = require( 'child_process' ).exec;
const { readFileSync, existsSync, writeFileSync } = require( 'fs' );

const result = function ( command, cb ) {
	exec( command, function ( err, stdout, stderr ) {
		if ( err != null ) {
			return cb( new Error( err ), null );
		} else if ( typeof ( stderr ) != "string" ) {
			return cb( new Error( stderr ), null );
		} else {
			return cb( null, stdout );
		}
	} );
}

const getToday = function () {
	const today = new Date();
	const yyyy = today.getFullYear();
	let mm = today.getMonth() + 1; // Months start at 0!
	let dd = today.getDate();

	if ( dd < 10 ) dd = '0' + dd;
	if ( mm < 10 ) mm = '0' + mm;

	return dd + '/' + mm + '/' + yyyy;
}

function generateCL ( dir = '.' ) {
	result( `sh ${dir}/git_info.sh`, function ( err, response ) {
		if ( !err ) {
			// console.log( response );
			response = response.split( "\n" );
			response.pop();
			latestTag = response.shift();
			currVersion = latestTag.substring( 1 );
			response = response.map( el => el.substring( 8 ) );
			response = '* ' + response.join( '\n * ' );
			console.log( currVersion, response );

			const cLog = readFileSync( `${dir}/changelog.json`, "utf8" );
			const cLogJson = JSON.parse( cLog );

			if ( Object.prototype.hasOwnProperty.call( cLogJson.versions, currVersion ) ) {
				cLogJson.versions[currVersion].date = getToday();
				cLogJson.versions[currVersion].cl = response;
				console.log( cLogJson );
			} else {
				cLogJson.versions = {
					[currVersion]: {
						date: getToday(),
						cl: response
					},
					...cLogJson.versions
				};
				console.log( 'nv', cLogJson );
			}

			writeFileSync( `${dir}/changelog.json`, JSON.stringify( cLogJson, null, 4 ) );
		} else {
			console.log( err );
		}
	} );
}

module.exports = {
	generateCL
}
