function addResultBox ( boxId, resultId, icons, label ) {
	let boxHtml = "";
	let iconsHtml = "";
	icons.forEach( iconName => { iconsHtml += `<div class="${iconName}"></div>` } );

	boxHtml += `
		<div class="result-box">
			<div class="icns">
				${iconsHtml}
			</div>
			<div class="pack-pic" style="display: none;">
				<img src="" alt="eutirox package" />
			</div>
			<p class="result-title" data-lang>${label}</p>
			<p class="result"><span id="${resultId}">0-0</span> μg/<span data-lang>${appTranslations[country].day}</span><sup id="ref-id"></sup></p>
		</div>
	`;

	return boxHtml;
}
const elB = addResultBox( 'test', 'test-2', ['icon-tablet-i'], 'test label' );

// byId( 'result-box-container' ).insertAdjacentElement( '', elB );
presc.addHtmlElm( '#result-box-container', 'afterbegin', 'div', { class: 'col-md-6', id: 'test' }, elB )
