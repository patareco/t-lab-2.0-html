#! /usr/bin/bash
git describe --exact-match --abbrev=1
git log --oneline $(git describe --tags --abbrev=0 @^)..@